<?php

Route::get('/','Frontend\Home\HomeController@index')->name('home');
Route::post('/register/customer','Frontend\Home\HomeController@customerRegister')->name('register');

Route::get('/gioi-thieu', 'Frontend\Home\HomeController@gioithieu')->name('gioithieu');



Route::get('/lien-he','Frontend\LienHe\LienHeController@index')->name('lienhe');

Route::post('/send/info','Frontend\LienHe\LienHeController@sendInfo')->name('post.lienhe');



Route::get('/tin-tuc','Frontend\Post\PostController@index')->name('tintuc');

Route::get('/su-kien','Frontend\Post\PostController@event')->name('sukien');

Route::get('/tin-tuc/{post}_{id}.html','Frontend\Post\PostController@detail')->name('chitiet.tintuc');



Route::get('/dai-ly','Frontend\DaiLi\DailiController@index')->name('daili');

Route::post('/dang-ky-lam-dai-ly','Frontend\DaiLi\DailiController@dangki')->name('daili.dangki');

Route::post('/daily/map','Frontend\DaiLi\DailiController@loadMap')->name('daily.load.map');

Route::get('/bang-mau','Frontend\Color\ColorController@bangmau')->name('bangmau');



Route::get('/cong-trinh-tieu-bieu','Frontend\Color\ColorController@congtrinh')->name('congtrinh');



Route::get('/cam-hung-mau-sac','Frontend\Color\ColorController@camhung')->name('camhung');



Route::get('/cong-thuc-tinh-son','Frontend\CongThuc\CongthucController@congthuc')->name('congthuc');
Route::post('/congthuc','Frontend\CongThuc\CongthucController@caculator')->name('post.congthuc');

Route::get('/tu-van-tu-chuyen-gia', 'Frontend\Tuvan\TuvanController@index')->name('chuyengia');



Route::get('/chitiettuvan-{id}','Frontend\Tuvan\TuvanController@detail')->name('chitiettuvan');



Route::get('/dang-ky-nhan-tu-van','Frontend\Tuvan\TuvanController@dangky')->name('dangky');

Route::post('/dangki/tuvan', 'Frontend\Tuvan\TuvanController@postDangky')->name('dangky.tuvan');





//ROUTE Sáº¢N PHÃ‚M

Route::get('/san-pham', 'Frontend\Product\ProductController@index')->name('sanpham');

Route::get('/{product}_{id}.html','Frontend\Product\ProductController@detail')->name('detail.product');

Route::post('/san-pham/search','Frontend\Product\ProductController@search')->name('sp.search');

Route::get('/danh-muc/{title}_{id}.html','Frontend\Product\ProductController@category')->name('category.product');

Route::get('/video', 'Frontend\Video\VideoController@index')->name('video.list');
Route::get('/bang-gia-son-nesan', 'Frontend\Home\HomeController@priceList')->name('price.list');

Route::get('/bao-hanh', 'Frontend\Guarantee\GuaranteeController@index')->name('guarantee.index');
Route::post('/bao-hanh', 'Frontend\Guarantee\GuaranteeController@add')->name('guarantee.add');
Route::get('/bao-hanh/chinh-sach', 'Frontend\Guarantee\GuaranteeController@policy')->name('guarantee.policy');
Route::get('/bao-hanh/tra-cuu', 'Frontend\Guarantee\GuaranteeController@search')->name('guarantee.search');
Route::get('/bao-hanh/tim-kiem', 'Frontend\Guarantee\GuaranteeController@result')->name('guarantee.result');
Route::post('/bao-hanh/register', 'Frontend\Guarantee\GuaranteeController@register')->name('guarantee.register');

Route::get('/tuyen-dung', 'Frontend\Tuyendung\RecruitController@index')->name('recruit.index');
Route::get('/chinh-sach', 'Frontend\Policy\PolicyController@index')->name('policy.index');

Route::get('/ho-so', 'Frontend\FileController@index')->name('file.index');
Route::get('/ho-so/view', 'Frontend\FileController@view')->name('file.view');







