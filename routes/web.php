<?php
use Illuminate\Support\Facades\Route;

Route::get('/login', 'LoginController@login')->name('login');
Route::get('/logout', function () {
    Auth::logout();
    return redirect(route('login'));
})->name('logout');
Route::post('/post/login', 'LoginController@postLogin')->name('post.login');
Route::post('/ajax/register', 'LoginController@userRegister')->name('user.register');
Route::get('/command/123','LoginController@command');
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'Backend\HomeController@home')->name('admin');
    require __DIR__ . '/Backend/backend.php';
});

//route frontend
require __DIR__ . '/Frontend/frontend.php';
