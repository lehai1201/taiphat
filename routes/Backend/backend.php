<?php

Route::group(array('prefix' => 'user', 'middleware' => 'auth'), function () {
    Route::get('/profile/{id}', 'User\UserController@viewProfile')->name('user.profile');
    Route::post('/edit/{id}', 'User\UserController@editPassword')->name('user.edit.password');
    Route::post('/edit/profile/{id}', 'User\UserController@editProfile')->name('user.edit.profile');
    Route::post('/edit/avatar/{id}', 'User\UserController@editAvatar')->name('user.edit.avatar');
    Route::post('/send/email', 'User\UserController@sendEmail')->name('user.send.email');
});

Route::group(array('prefix' => 'employee', 'middleware' => 'auth'), function () {
    Route::get('/list', 'Backend\Employee\EmployeeController@list')->name('employee.list');
    Route::post('/ajax/edit/user', 'Backend\Employee\EmployeeController@ajaxLoadEditUser')->name('employee.ajax.edit');
    Route::post('/ajax/add/user', 'Backend\Employee\EmployeeController@ajaxAddUser')->name('employee.ajax.add');
    Route::post('/ajax/delete', 'Backend\Employee\EmployeeController@ajaxDeleteUser')->name('employee.ajax.delete');
    Route::post('/ajax/update', 'Backend\Employee\EmployeeController@ajaxUpdateUser')->name('employee.ajax.update');
    Route::post('/ajax/search', 'Backend\Employee\EmployeeController@ajaxSearch')->name('employee.ajax.search');
    Route::get('/export/excel', 'Backend\Employee\EmployeeController@exportExcel')->name('employee.export.excel');
});

Route::group(array('prefix' => 'color', 'middleware' => 'auth'), function () {
    Route::get('/', 'Color\ColorController@index')->name('color.index');
    Route::post('/add', 'Color\ColorController@add')->name('color.add');
    Route::post('/deleted', 'Color\ColorController@deleted')->name('color.deleted');
});

Route::group(array('prefix' => 'category', 'middleware' => 'auth'), function () {
    Route::get('/', 'Backend\Category\CategoryController@index')->name('category.index');
    Route::post('/new/category', 'Backend\Category\CategoryController@newSpecies')->name('category.new.category');
    Route::post('/new/tag', 'Backend\Category\CategoryController@newCategory')->name('category.new.tag');
    Route::post('/deleted/category', 'Backend\Category\CategoryController@deletedCategoryTag')->name('deleted.tag');
    Route::post('/edit/category', 'Backend\Category\CategoryController@editCategory')->name('edit.category');
    Route::post('/post/edit', 'Backend\Category\CategoryController@postEdit')->name('post.edit.category');
    Route::post('/search', 'Backend\Category\CategoryController@search')->name('category.search');
    Route::post('/show/species', 'Backend\Category\CategoryController@showModalSpecies')->name('species.show.modal.edit');
    Route::post('/edit/species', 'Backend\Category\CategoryController@editSpecies')->name('species.edit');
    Route::post('/deleted/species', 'Backend\Category\CategoryController@deleteSpecies')->name('species.deleted');
    Route::post('/add/catalog', 'Backend\Category\CategoryController@addCatalog')->name('category.new.catalog');
    Route::post('/modal/catalog', 'Backend\Category\CategoryController@showModalCatalog')->name('category.show.catalog');
    Route::post('/edit/catalog', 'Backend\Category\CategoryController@editCatalog')->name('catalog.edit');
    Route::post('/delete/catalog', 'Backend\Category\CategoryController@deleteCatalog')->name('catalog.delete');
});

Route::group(array('prefix' => 'contact', 'middleware' => 'auth'), function () {
    Route::get('/', 'Backend\Contact\ContactController@index')->name('list.contact');
    Route::post('/reply', 'Backend\Contact\ContactController@replyContact')->name('reply.contact');
    Route::post('/search', 'Backend\Contact\ContactController@search')->name('search.lienhe');
    Route::post('/send/email/contact', 'Backend\Contact\ContactController@sendEmailContact')->name('send.email');
    Route::get('/tuvan', 'Backend\Contact\ContactController@tuvan')->name('list.tuvan');
    Route::get('/daily', 'Backend\Contact\ContactController@daily')->name('list.daily');

});

Route::group(array('prefix' => 'post', 'middleware' => 'auth'), function () {
    Route::get('/list', 'Backend\Post\PostController@list')->name('post.list');
    Route::get('/edit/{id}', 'Backend\Post\PostController@edit')->name('post.edit');
    Route::post('/edit/{id}', 'Backend\Post\PostController@postEdit')->name('post.submit.edit');
    Route::post('/deleted/post', 'Backend\Post\PostController@deletedPost')->name('deleted.post');
    Route::get('/add', 'Backend\Post\PostController@addPost')->name('post.add');
    Route::post('/post/add', 'Backend\Post\PostController@postAddNews')->name('post.add.news');
    Route::post('/post/check', 'Backend\Post\PostController@checkPost')->name('post.check');
});

Route::group(array('prefix' => 'product', 'middleware' => 'auth'), function () {
    Route::get('/list', 'Backend\Product\ProductController@list')->name('product.list');
    Route::post('/edit', 'Backend\Product\ProductController@edit')->name('product.edit');
    Route::get('/add', 'Backend\Product\ProductController@addProduct')->name('product.add');
    Route::post('/post/add', 'Backend\Product\ProductController@postAddProduct')->name('product.add.post');
    Route::post('/ajax/load', 'Backend\Product\ProductController@ajaxLoadCategory')->name('product.ajax');
    Route::post('/ajax/edit', 'Backend\Product\ProductController@ajaxLoadEditCategory')->name('product.edit.ajax');
    Route::post('/deleted/product', 'Backend\Product\ProductController@deletedProduct')->name('deleted.product');
    Route::post('/search', 'Backend\Product\ProductController@searchProduct')->name('product.search');
    Route::get('/edit-{id}', 'Backend\Product\ProductController@editProduct')->name('product.edit');
    Route::post('/post-edit-{id}', 'Backend\Product\ProductController@postEditProduct')->name('product.post.edit');
});
Route::group(array('prefix' => 'construction', 'middleware' => 'auth'), function () {
    Route::get('/', 'Backend\Construction\ConstructController@index')->name('construction.index');
    Route::post('/add', 'Backend\Construction\ConstructController@add')->name('construct.add');
    Route::post('/delete', 'Backend\Construction\ConstructController@delete')->name('construct.delete');
});

Route::group(array('prefix' => 'slider', 'middleware' => 'auth'), function () {
    Route::post('/add', 'Backend\Slider\SliderController@add')->name('slider.add');
    Route::post('/deleted', 'Backend\Slider\SliderController@deleted')->name('slider.deleted');
});
Route::group(array('prefix' => 'customer', 'middleware' => 'auth'), function () {
    Route::get('/', 'Backend\Customer\CustomerController@index')->name('customer.index');
});

Route::group(array('prefix' => 'daily', 'middleware' => 'auth'), function () {
    Route::get('/', 'Backend\DaiLy\DaiLyController@index')->name('daily.index');
    Route::post('/add', 'Backend\DaiLy\DaiLyController@add')->name('daily.add');
    Route::post('/delete', 'Backend\DaiLy\DaiLyController@delete')->name('daily.delete');
    Route::post('/show/modal', 'Backend\DaiLy\DaiLyController@loadModal')->name('daily.show.modal');
    Route::post('/edit', 'Backend\DaiLy\DaiLyController@edit')->name('daily.edit');
    Route::post('/search', 'Backend\DaiLy\DaiLyController@search')->name('daily.search');
});

Route::group(array('prefix' => 'video', 'middleware' => 'auth'), function () {
    Route::get('/', 'Backend\Video\VideoController@index')->name('video.index');
    Route::get('/add', 'Backend\Video\VideoController@add')->name('video.add');
    Route::post('/add', 'Backend\Video\VideoController@postAdd')->name('video.post.add');
    Route::post('/delete', 'Backend\Video\VideoController@delete')->name('video.delete');
    Route::get('/edit/{id}', 'Backend\Video\VideoController@edit')->name('video.edit');
    Route::post('/edit/{id}', 'Backend\Video\VideoController@postEdit')->name('video.post.edit');
});

Route::group(array('prefix' => 'rate', 'middleware' => 'auth'), function () {
    Route::get('/', 'Backend\Rate\RateController@index')->name('rate.index');
    Route::get('/create', 'Backend\Rate\RateController@add')->name('rate.add');
    Route::post('/create', 'Backend\Rate\RateController@postAdd')->name('rate.post.add');
    Route::get('/edit/{id}', 'Backend\Rate\RateController@edit')->name('rate.edit');
    Route::post('/delete', 'Backend\Rate\RateController@delete')->name('rate.delete');
    Route::post('/edit/{id}', 'Backend\Rate\RateController@postEdit')->name('rate.post.edit');
});

Route::get('/price', 'Backend\PriceController@list')->name('price.index');
Route::get('/price/add', 'Backend\PriceController@add')->name('price.add');
Route::post('/price/add', 'Backend\PriceController@postAdd')->name('price.post.add');
Route::post('/delete', 'Backend\PriceController@delete')->name('price.delete');
Route::get('/edit/{id}', 'Backend\PriceController@edit')->name('price.edit');
Route::post('/edit/{id}', 'Backend\PriceController@postEdit')->name('price.post.edit');

Route::post('meta/delete', 'Backend\Meta\MetaController@delete')->name('meta.delete');
Route::resource('meta', 'Backend\Meta\MetaController');
Route::get('guarantee', 'Backend\GuaranteeController@index')->name('backend.guarantee.index');
Route::post('guarantee/status', 'Backend\GuaranteeController@changeStatus')->name('backend.guarantee.change');
Route::post('guarantee/delete', 'Backend\GuaranteeController@delete')->name('backend.guarantee.delete');
Route::get('guarantee/export', 'Backend\GuaranteeController@export')->name('backend.guarantee.export');

Route::get('/recruit', 'Backend\RecruitController@index')->name('backend.recruit.index');
Route::post('/recruit', 'Backend\RecruitController@add')->name('backend.recruit.post.add');

Route::get('/policy', 'Backend\PolicyController@index')->name('backend.policy.index');
Route::post('/policy', 'Backend\PolicyController@add')->name('backend.policy.post.add');

Route::get('/file', 'Backend\FileController@index')->name('backend.file.index');
Route::post('/file', 'Backend\FileController@add')->name('backend.file.post.add');
