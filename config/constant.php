<?php

if (!defined('IS_SYSADMIN')) {
    define('IS_SYSADMIN', 1);
}
if (!defined('IS_MANAGER')) {
    define('IS_MANAGER', 2);
}
if (!defined('IS_EMPLOYEE')) {
    define('IS_EMPLOYEE', 3);
}


if (!defined('IS_CATEGORY')) {
    define('IS_CATEGORY', 0);
}
if (!defined('IS_TAG')) {
    define('IS_TAG', 1);
}

if (!defined('NO_DELETED')) {
    define('NO_DELETED', 0);
}
if (!defined('IS_DELETED')) {
    define('IS_DELETED', 1);
}

if (!defined('IS_CHECKED')) {
    define('IS_CHECKED', 1);
}


if (!defined('IS_SHOW')) {
    define('IS_SHOW', 0);
}
if (!defined('IS_HIDE')) {
    define('IS_HIDE', 1);
}

//post
if (!defined('IS_POST')) {
    define('IS_POST', 0);
}
if (!defined('IS_EVENT')) {
    define('IS_EVENT', 1);
}
if (!defined('IS_ADVISOTY')) {
    define('IS_ADVISOTY', 2);
}

//contact
if (!defined('IS_CONTACT')) {
    define('IS_CONTACT', 0);
}
if (!defined('IS_COLOR')) {
    define('IS_COLOR', 1);
}
if (!defined('IS_REGISTER_AGENCY')) {
    define('IS_REGISTER_AGENCY', 2);
}

//
if (!defined('IS_NHANTIN')) {
    define('IS_NHANTIN', 0);
}
if (!defined('IS_TUVAN')) {
    define('IS_TUVAN', 2);
}
if (!defined('IS_LIENHE')) {
    define('IS_LIENHE', 1);
}
if (!defined('IS_DAILI')) {
    define('IS_DAILI', 3);
}

if (!defined('IS_NEW')) {
    define('IS_NEW', 0);
}

if (!defined('IS_OLD')) {
    define('IS_OLD', 1);
}

//product
if (!defined('IS_NESSAN')) {
    define('IS_NESSAN', 28);
}
if (!defined('IS_CHAIR')) {
    define('IS_CHAIR', 27);
}
if (!defined('IS_ATINI')) {
    define('IS_ATINI', 22);
}
if (!defined('IS_MALLEND')) {
    define('IS_MALLEND', 21);
}

return [
    'AGENCY' => [
        'NORLAND' => 0,
        'CENTRAL' => 1,
        'SOUTH' => 2,
    ],
    'BRANCH' => [
        0 => 'Miền Bắc',
        1 => 'Miền Trung',
        2 => 'Miền Nam',
    ],
    'META' => [
        0 => 'Header',
        1 => 'Footer'
    ],
    'GUARANTEE_TYPE' => [
        'CONFIRM' => 0,
        'ACCEPT' => 1,
        'CANCEL' => 2,
    ],
    'UNIT_TEXT' => [
        1 => 'Thùng',
        2 => 'Lon',
        3 => 'Lit',
        4 => 'Bao',
    ],
    'UNIT' => [
        'BIN' => 1,
        'BOX' => 2,
        'LIT' => 3,
        'BAG' => 4,
    ]
];
