@extends('layouts.layout')
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin')}}">Trang chủ</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('rate.index')}}">Đánh giá</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <p>Cập nhật đánh giá</p>
                    </li>
                </ul>
                <div class="page-content-inner">
                    @if(Session::has('errors'))
                        @foreach(Session::get('errors') as $error)
                            <div class="alert-danger"
                                 style="padding: 10px;margin-bottom: 10px">{{ $error }}</div>
                        @endforeach
                    @endif
                    @if(Session::has('success'))
                        @foreach(Session::get('success') as $success)
                            <div class="alert-success"
                                 style="padding: 10px;margin-bottom: 10px">{{ $success }}</div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">Cập nhật</div>
                                </div>
                                <div class="portlet-body">
                                    <form action="{{ route('rate.post.edit', $rate->id) }}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <br>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Avatar</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="avatar" onChange="loadImg(event, this)"
                                                       value="{{ old('avatar') }}">
                                                <br>
                                                <img style="width: 200px;margin-top: 10px"
                                                     id="img_show" src="/images/rate/{{ $rate->avatar }}" alt="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Nhận xét</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="comment" class="form-control" maxlength="128"
                                                       placeholder="Nhận xét" value="{{ $rate->comment }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Tên</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="name" class="form-control" maxlength="128"
                                                       placeholder="Tên" value="{{ $rate->name }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Địa chỉ</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="address" class="form-control" maxlength="128"
                                                       placeholder="Địa chỉ" value="{{ $rate->address }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-sm-2 col-form-label">Sắp xếp</label>
                                            <div class="col-sm-10">
                                                <select name="sort" class="form-control">
                                                    @for($i=1;$i<=20;$i++)
                                                        <option {{ $rate->sort == $i ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2"> Lưu</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_footer_script')
    <script>
        function loadImg(event, e) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];
            if ($(e).val() == '') {
                $("#img_show").attr('src', '');
            } else {
                if ($.inArray($(e).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    $("#img_show").attr('src', '');
                    alert('Phải chọn ảnh kiểu ảnh và có đuôi: JPEG,JPG,PNG');
                    $(e).val('');
                } else {
                    var path = URL.createObjectURL(event.target.files[0]);
                    $("#img_show").attr('src', path);
                }
            }
        }
    </script>
@endsection
