@extends('layouts.layout')
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin')}}">Trang chủ</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('guarantee.index')}}">Bảo hành</a>
                    </li>
                </ul>


                <div class="page-content-inner">
                    @if(Session::has('errors'))
                        <div class="alert-danger"
                             style="padding: 10px;margin-bottom: 10px">{{ Session::get('errors') }}</div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert-success"
                             style="padding: 10px;margin-bottom: 10px">{{ Session::get('success') }}</div>
                    @endif
                        <form method="get" action="{{ route('backend.guarantee.index') }}" class="form-search">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="start_date" class="form-control datepicker" value="{{ request()->get('start_date') }}" placeholder="Từ ngày">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="end_date" class="form-control datepicker" value="{{ request()->get('end_date') }}" placeholder="Đến ngày">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="phone" class="form-control" value="{{ request()->get('phone') }}" placeholder="Số điện thoại">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <a href="{{ route('backend.guarantee.index') }}" class="btn btn-success">Làm mới</a>
                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </form>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fas fa-star"></i> Bảo hành
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body">
                                                <table class="table table-bordered" id="table-guarantee">
                                                    <thead>
                                                    <tr>
                                                        <th onclick="checkedAll(this)" class="first-th"><input type="checkbox" id="check_all" class="call-checkbox"/></th>
                                                        <th class="text-center" style="width: 15%">Tên khách hàng</th>
                                                        <th class="text-center" style="width: 12%">Số điện thoại</th>
                                                        <th class="text-center">Tên sản phẩm</th>
                                                        <th class="text-center">Mã sản phẩm</th>
                                                        <th class="text-center">Đại lý</th>
                                                        <th class="text-center">Đơn vị</th>
                                                        <th class="text-center" style="width: 5%">Số lượng</th>
                                                        <th class="text-center" scope="col">Trạng thái</th>
                                                        <th class="text-center" scope="col">Ngày đăng ký</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="data-guarantee">
                                                    @if($dataGuarantee->isNotEmpty())
                                                        @foreach($dataGuarantee as $data)
                                                            <tr onclick="checkedData(this)">
                                                                <th style="padding: 13px"><input type="checkbox" class="call-checkbox" value="{{ $data->id }}" /></th>
                                                                <td class="text-left">{{ $data->client_name }}</td>
                                                                <td class="text-left">(+84) {{ $data->phone }}</td>
                                                                <td class="text-left">{{ $data->product_name }}</td>
                                                                <td class="text-left">{{ $data->code }}</td>
                                                                <td>{{ $data->agency_name }}</td>
                                                                <td>
                                                                    @if((int)$data->unit == 1)
                                                                        <div>Thùng</div>
                                                                    @elseif((int)$data->unit == 2)
                                                                        <div>Lon</div>
                                                                    @elseif((int)$data->unit == 3)
                                                                        <div>Lít</div>
                                                                    @else
                                                                        <div>Bao</div>
                                                                    @endif
                                                                </td>
                                                                <td>{{ $data->number }}</td>
                                                                <td>
                                                                    @if((int)$data->type == 0)
                                                                        <div class="confirm">Chờ xác nhận</div>
                                                                    @elseif((int)$data->type == 1)
                                                                        <div class="accept">  Đã xác nhận</div>
                                                                    @else
                                                                        <div class="cancel">Đã hủy</div>
                                                                    @endif
                                                                </td>
                                                                <td>{{ date('d-m-Y', strtotime($data->created_at)) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-primary" onclick="changeStatus(1, this)">Xác nhận đơn bảo hành</button>
                                            <button type="button" class="btn btn-danger"  onclick="changeStatus(2, this)">Hủy đơn bảo hành</button>
                                            <button type="button" class="btn btn-success" onclick="exportData(this)">Export</button>
                                            <button type="button" class="btn btn-warning"  onclick="deleteData(this)">Xóa đơn bảo hành</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('begin_page_level_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
             $('#table-guarantee').DataTable({
                 "aoColumnDefs": [
                     { "bSortable": false, "aTargets": [ 0 ] },
                     { "bSearchable": false, "aTargets": [ 0] }
                 ]
             });
            $(".datepicker").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                clearBtn: true,
                todayBtn: true
            })

            $("#check_all").on('click', function (e) {
                e.stopPropagation();
                let checked = $("#check_all:checked");
                let data = $(".data-guarantee").find('input[type="checkbox"]');

                if (checked.length > 0) {
                    data.each(function () {
                        $(this).prop('checked', true)
                    });
                } else {
                    data.each(function () {
                        $(this).prop('checked', false)
                    });
                }
            })
        });



        function checkedData(e) {
            if($(e.target).is(':checkbox')) return;
            var check = $(e).find(':checkbox');
            check.prop('checked',!check.is(':checked'));
        }

        function changeStatus(type, e) {
            var table = $('#table-guarantee').DataTable();
            var checked = table.$('input[type="checkbox"]:checked');

            if (checked.length == 0) {
                swal('Vui lòng chọn đơn bảo hành !', '', 'error');
            } else {
                $(e).attr('disabled', 'true');
                var list = [];
                checked.each(function () {
                    list.push($(this).val());
                });

                if (list.length > 0) {
                    $.ajax({
                        type: 'POST',
                        url: '{{route('backend.guarantee.change')}}',
                        data: {
                            ids: list,
                            type: type
                        },
                        dataType: 'json',
                        success(response) {
                            if (response.success) {
                                swal('Cập nhật thành công', '', 'success');
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1500)
                            } else {
                                $(e).attr('disabled', 'false');
                                swal('Có lỗi xảy ra', '', 'error');
                            }
                        }
                    });
                }
            }
        }

        function exportData(e) {
            var table = $('#table-guarantee').DataTable();
            var checked = table.$('input[type="checkbox"]:checked');

            if (checked.length == 0) {
                swal('Vui lòng chọn đơn bảo hành !', '', 'error');
            } else {
                $(e).attr('disabled', 'true');
                var list = [];
                checked.each(function () {
                    list.push($(this).val());
                });

                if (list.length > 0) {
                    var params = Object.assign({}, list);
                    var url = "{{ URL::to(route('backend.guarantee.export'))}}?" + $.param(params);
                    window.location = url;
                }
                $(e).prop('disabled', false);
            }
        }

        function deleteData(e) {
            var table = $('#table-guarantee').DataTable();
            var checked = table.$('input[type="checkbox"]:checked');

            if (checked.length == 0) {
                swal('Vui lòng chọn đơn bảo hành !', '', 'error');
            } else {
                $(e).attr('disabled', 'true');
                var list = [];
                checked.each(function () {
                    list.push($(this).val());
                });
                if(confirm('Bạn có chắc chắn muốn xóa?')){
                    if (list.length > 0) {
                        $.ajax({
                            type: 'POST',
                            url: '{{route('backend.guarantee.delete')}}',
                            data: {
                                ids: list,
                            },
                            dataType: 'json',
                            success(response) {
                                if (response.success) {
                                    swal('Xóa thành công', '', 'success');
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 1500)
                                } else {
                                    $(e).attr('disabled', 'false');
                                    swal('Có lỗi xảy ra', '', 'error');
                                }
                            }
                        });
                    }
                }
            }
        }


        function checkedAll(e) {
            if ($(e.target).is(':checkbox')) return;
            var check = $(e).find(':checkbox');
            check.prop('checked', !check.is(':checked'));

            selectAll();
        }

        function selectAll() {
            let checked = $("#check_all:checked");
            let data = $(".data-guarantee").find('input[type="checkbox"]');

            if (checked.length > 0) {
                data.each(function () {
                    $(this).prop('checked', true)
                });
            } else {
                data.each(function () {
                    $(this).prop('checked', false)
                });
            }
        }
    </script>
@endsection

