@extends('layouts.layout')
@section('page_head_include')
    <link rel="stylesheet" href="/css/reponsive/reponsive_reponsive.css">
@endsection
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin')}}">Trang chủ</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('video.index')}}">Video</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <p>Cập nhật video</p>
                    </li>
                </ul>

                <div class="page-content-inner">
                    @if(Session::has('errors'))
                        @foreach(Session::get('errors') as $error)
                            <div class="alert-danger"
                                 style="padding: 10px;margin-bottom: 10px">{{ $error }}</div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">Cập nhật</div>
                                </div>
                                <div class="portlet-body">
                                    <form action="{{ route('video.post.edit', $video->id) }}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <br>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Video</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" maxlength="128" name="url" value="{{ $video->url }}" placeholder="https://www.youtube.com/watch?v=QOOWzrBYn3k">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Tiêu đề</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="title" class="form-control" maxlength="128"
                                                       placeholder="Tiêu đề" value="{{ $video->title }}">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2"> Lưu</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_footer_script')
    <script>
        function loadVideo(event, e) {
            var fileExtension = ['mp4', 'mov', 'avi', 'ogg', 'flv', 'wmv', '3gp'];
            if ($(e).val() != '') {
                if ($.inArray($(e).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert('Phải chọn định dạng video');
                    $(e).val('');
                }
            }

            $("#preview-video").hide();
        }
    </script>
@endsection
