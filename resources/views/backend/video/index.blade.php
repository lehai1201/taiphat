@extends('layouts.layout')
@section('page_head_include')
    <link rel="stylesheet" href="/css/reponsive/reponsive_reponsive.css">
@endsection
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin')}}">Trang chủ</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('video.index')}}">Video</a>
                    </li>
                </ul>

                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fas fa-video"></i> Danh sách video
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body">
                                                <div class="table-toolbar">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="btn-group">
                                                                <a class="btn sbold green"
                                                                   href="{{ route('video.add') }}"><i
                                                                        class="fas fa-plus"></i> Thêm mới</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div id="data_list">
                                                        <div>Tổng số video: <b>{{count($listVideo)}}</b></div>
                                                        <table
                                                            class="table table-striped table-bordered table-hover no-footer reponsive_table"
                                                            role="grid"
                                                            aria-describedby="sample_1_info">
                                                            <thead class="bg-blue bg-font-grey-gallery">
                                                            <tr role="row">
                                                                <th tabindex="0"
                                                                    aria-controls="sample_1" rowspan="1" colspan="1">
                                                                    Video
                                                                </th>
                                                                <th tabindex="0"
                                                                    aria-controls="sample_1" rowspan="1" colspan="1">
                                                                    Tiêu đề
                                                                </th>
                                                                <th>Ngày thêm
                                                                </th>
                                                                <th>Thao tác</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if($listVideo->isEmpty())
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <h5 class="font-red">Danh sách trống</h5>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                @foreach($listVideo as $key => $video)
                                                                    <tr>
                                                                        <td style="width: 30%;">
                                                                            <iframe frameborder="0" src="{{ $video->genUrl() }}"></iframe>
                                                                        </td>
                                                                        <td>{{$video->title}}</td>
                                                                        <td style="width: 20%;">{{ date('d/m/Y', strtotime($video->created_at)) }}</td>
                                                                        <td style="width: 20%;"><a class="font-green"
                                                                                                   href="{{ route('video.edit', $video->id) }}">Sửa</a>&nbsp;<a
                                                                                class="font-red" href="javascript:void(0)"
                                                                                onclick="showModalDeleted('{{$video->id}}')">Xóa</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                        <div class="pagination">{!! $listVideo->links() !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete_video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red"><h5 class="modal-title bg-font-dark bold" id="exampleModalLabel"><i
                            class="icon-trash"></i> XÓA VIDEO</h5></div>
                <div class="modal-body">
                    <form id="form_delete_product_tag">
                        <div class="form-group">
                            <div class="form-group font-red-flamingo">Bạn có chắc chắn muốn xóa video này?</div>
                        </div>
                        <input type="hidden" id="video_id" name="video_id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" onclick="deleteVideo()"
                            class="btn btn-outline red">Xóa
                    </button>
                    <button type="button" class="btn btn-outline purple" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('begin_page_level_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });

        function showModalDeleted(video_id) {
            $('#video_id').val(video_id);
            $('#delete_video').modal('show');
        }

        function deleteVideo() {
            var id = $('#video_id').val();
            $.ajax({
                type: 'POST',
                url: '{{route('video.delete')}}',
                data: {
                    id: id
                },
                success: function (result) {
                    $('#delete_video').modal('hide');
                    swal(result, '', 'success');
                    setTimeout(function () {
                        swal.close();
                        window.location.reload();
                    }, 1500);
                }
            });
        }
    </script>
@endsection
