@extends('layouts.layout')
@section('page_content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{route('admin')}}">Trang chủ</a>
                                    <i class="fas fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Danh sách bảng màu</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fas fa-gift"></i> Màu sắc
                                    </div>
                                    <div class="actions">
                                        <a href="#add_color" data-toggle="modal" class="btn btn-sm font-red bg-white">
                                            <i class="fas fa-plus font-red"></i> Thêm mới </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    @if(!is_null($data_color))
                                        <div class="row">
                                            @foreach($data_color as $color)
                                                <div class="col-md-2 col-sm-2 col-xs-6">
                                                    <a class="color-demo" onclick="showModalDelete({{$color->id}})" style="text-decoration: none;">
                                                        <div class="color-view bg-font-dark bold uppercase"
                                                             style="background-color: {{$color->color_code}} !important;">{{$color->name}}
                                                        </div>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div  style="text-align: center">{{$data_color->links()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL THÊM MỚI MÀU SẮC--}}
    <div class="modal fade" id="add_color" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h4 class="modal-title bg-font-dark bold" id="exampleModalLabel">Thêm mới màu sắc</h4>
                </div>
                <div class="modal-body">
                    <form id="form_add_color">
                        <div class="form-group">
                            <label>Tên màu</label>
                            <div class="input-icon">
                                <i class="fas fa-font"></i>
                                <input autofocus class="form-control placeholder-no-fix" type="text" id=""
                                       placeholder=""
                                       name="name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Mã màu hexa</label>
                            <div class="input-icon">
                                <i class="fas fa-paint-brush"></i>
                                <input autofocus class="form-control placeholder-no-fix" type="text" id="input_color"
                                       placeholder="#234ABC"
                                       name="color_code"/>
                                <div style="margin: 10px">
                                    <a href="#" id="check_color" class="font-blue">Xem trước</a>
                                </div>
                                <div id="demo_color" class="hide bold font-white text-center" style="padding: 20px;width: 200px;"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_add_color"
                            class="btn btn-outline blue">Thêm
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{--MODAL XÓA MÀU SẮC--}}
    <div class="modal fade" id="delete_color" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red">
                    <h4 class="modal-title bg-font-dark bold" id="exampleModalLabel">Xóa màu</h4>
                </div>
                <div class="modal-body">
                    <form id="form_add_color">
                        <div class="form-group">
                            <div>Nếu xóa sẽ không thể khôi phục được, Có chắc chắn xóa màu này?</div>
                            <input type="hidden" id="color_id" value="">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline blue" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_delete_color" onclick="deleteColor()"
                            class="btn btn-outline red">Xóa
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('page_footer_script')
    <script type="text/javascript">
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });
        $("#button_add_color").on('click',function (e) {
            e.preventDefault();
            $("#button_add_color").attr('disabled',true);
            var formColor=$("#form_add_color").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('color.add')}}',
                data: formColor,
                success: function (result) {
                    $("#add_color").modal('hide');
                    if(result=='success'){
                        swal('Thêm thành công','','success');
                        setTimeout(function(){
                          window.location.reload();
                        },1500);
                    }else{
                        $("#button_add_color").attr('disabled',false);
                        swal(result,'','error');
                        setTimeout(function(){
                            swal.close();
                            $("#add_color").modal('show');
                        },1500);
                    }
                }
            });
        })

        $("#check_color").on('click',function(e){
            e.preventDefault();
            $("#demo_color").addClass('hide');
            var color=$("#input_color").val();
            if(color){
                $("#demo_color").removeClass('hide');
                $("#demo_color").css('background-color',color);
                $("#demo_color").html(color);
            }else{
                toastr.error('Phải nhập mã code');
            }
        });

        function showModalDelete(id){
            $("#color_id").val(id);
            $("#delete_color").modal('show');
        }
        function deleteColor(){
            $("#button_delete_color").attr('disabled',true);
            var id=$("#color_id").val();
            $.ajax({
                type: 'POST',
                url: '{{route('color.deleted')}}',
                data:{
                    id: id
                },
                success: function (result) {
                    $("#delete_color").modal('hide');
                    if(result=='success'){
                        swal('Đã xóa thành công','','success');
                        setTimeout(function () {
                            window.location.reload();
                        },1500);
                    }
                }
            });
        }
    </script>
@stop
