<div class="page-header-menu">
    <div class="container-fluid">
        <div class="hor-menu menu-backend">
            <ul class="nav navbar-nav backend-menu">
                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown active">
                    <a href="{{route('admin')}}"> <i class="fas fa-home"></i> Trang chủ
                        <span class="arrow"></span>
                    </a>
                </li>
                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"><i class="fas fa-paper-plane"></i> Sản phẩm
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li aria-haspopup="true" class=" ">
                            <a href="{{route('product.list')}}" class="nav-link  "> Danh sách sản phẩm</a>
                        </li>
                        @cannot('Employees')
                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('category.index')}}" class="nav-link  "> Quản lí danh mục </a>
                            </li>
                        @endcan
                    </ul>
                </li>
                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"><i class="icon-speech"></i> Liên hệ
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li aria-haspopup="true" class=" ">
                            <a href="{{route('list.contact')}}" class="nav-link  ">Liên hệ</a>
                        </li>
                        <li aria-haspopup="true" class=" ">
                            <a href="{{route('list.tuvan')}}" class="nav-link  ">Tư vấn</a>
                        </li>
                        <li aria-haspopup="true" class=" ">
                            <a href="{{route('list.daily')}}" class="nav-link  ">Đăng ký làm đại lý</a>
                        </li>
                    </ul>
                </li>
                @can('SYS')
                    <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                        <a href="{{route('employee.list')}}"><i class="icon-user"></i> Danh sách nhân viên
                            <span class="arrow"></span>
                        </a>
                    </li>
                @endcan
                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                    <a href="{{route('post.list')}}">
                        <i class="icon-docs"></i> Bài viết
                        <span class="arrow"></span>
                    </a>
                </li>
                <li aria-haspopup="true" class=" ">
                    <a href="{{route('color.index')}}" class="nav-link "><i class="fas fa-paint-brush"></i> Màu sắc</a>
                </li>
                <li aria-haspopup="true" class=" ">
                    <a href="{{route('construction.index')}}" class="nav-link "><i class="fas fa-building-o"></i> Công
                        trình tiêu biểu</a>
                </li>
                <li aria-haspopup="true" class=" ">
                    <a href="{{route('daily.index')}}" class="nav-link "><i class="fas fa-store-alt"></i> Đại lý</a>
                </li>
                <li aria-haspopup="true" class=" ">
                    <a href="{{route('rate.index')}}" class="nav-link "><i class="fas fa-star"></i> Đánh giá</a>
                </li>
                <li aria-haspopup="true" class=" ">
                    <a href="{{route('video.index')}}" class="nav-link "><i class="fas fa-video"></i> Video</a>
                </li>
                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"><i class="fas fa-arrow-down"></i> Khác
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li aria-haspopup="true" class=" ">
                            <a href="https://dashboard.tawk.to/login"  target="_blank" class="nav-link "><i class="fas fa-comment-dots"></i> Chat</a>
                        </li>
                        @can('SYS')
                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('price.index')}}" class="nav-link  "><i class="fas fa-dollar-sign"></i> Quản lí bảng giá</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                                <a href="{{ route('meta.index') }}" class="nav-link "><i class="fas fa-tag"></i> Quản lý thẻ Meta</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                                <a href="{{ route('backend.guarantee.index') }}" class="nav-link "><i class="fas fa-tag"></i> Quản lý đơn bảo hành</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                                <a href="{{ route('backend.recruit.index') }}" class="nav-link "><i class="fas fa-users"></i> Tuyển dụng</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                                <a href="{{ route('backend.policy.index') }}" class="nav-link "><i class="fas fa-users"></i> Chính sách</a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                                <a href="{{ route('backend.file.index') }}" class="nav-link "><i class="fas fa-file" aria-hidden="true"></i> Hồ sơ</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
