<div class="page-header-top">
    <div class="container">
        <div class="page-logo">
            <a href="{{route('admin')}}">
                <img src="{{asset('images/tp.png')}}" alt="logo" class="logo-default">
            </a>
        </div>
        <a href="javascript:;" class="menu-toggler"></a>
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-default">{{$sldaily+$sllienhe+$sltuvan+$slnhantin}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        @if($sldaily+$sllienhe+$sltuvan+$slnhantin>0)
                            <li class="external">
                                <h3>Bạn có
                                    <strong>{{$sldaily+$sllienhe+$sltuvan+$slnhantin}}</strong> thông báo mới</h3>
                            </li>
                        @endif
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                @if($sldaily+$sllienhe+$sltuvan+$slnhantin>0)
                                    @if($sldaily>0)
                                        <li>
                                            <a href="{{route('list.daily')}}">
                                                <span class="time">News</span>
                                                <span class="details">
                                            <span class="label label-sm label-icon label-success">
                                                <i class="fas fa-building-o"></i>
                                            </span>{{$sldaily}} Đơn đăng ký làm đại lý
                                        </span>
                                            </a>
                                        </li>
                                    @endif
                                    @if($sllienhe>0)
                                        <li>
                                            <a href="{{route('list.contact')}}">
                                                <span class="time">News</span>
                                                <span class="details">
                                            <span class="label label-sm label-icon label-danger">
                                                <i class="far fa-address-book"></i>
                                            </span>{{$sllienhe}} Liên hệ
                                        </span>
                                            </a>
                                        </li>
                                    @endif
                                    @if($sltuvan>0)
                                        <li>
                                            <a href="{{route('list.tuvan')}}">
                                                <span class="time">News</span>
                                                <span class="details">
                                            <span class="label label-sm label-icon label-warning">
                                                <i class="fas fa-comment"></i>
                                            </span>
                                            {{$sltuvan}} Tư vấn </span>
                                            </a>
                                        </li>
                                    @endif
                                    @if($slnhantin>0)
                                        <li>
                                            <a href="javascript:;">
                                        <span class="details">
                                            <span class="time">News</span>
                                            <span class="label label-sm label-icon label-info">
                                                <i class="fas fa-bullhorn"></i>
                                            </span> {{$slnhantin}} Đăng ký nhận tin tức </span>
                                            </a>
                                        </li>
                                    @endif
                                @else
                                    <li>
                                        <a href="javascript:;">
                                        <span class="details">
                                            <span class="label label-sm label-icon label-info">
                                                <i class="fas fa-bullhorn"></i>
                                            </span> Không có thông báo nào</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown dropdown dropdown-extended dropdown-notification dropdown-dark role">
                    <a href="javascript:;" class="dropdown-toggle font-red" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true" aria-expanded="false">
                        <i class="icon-tag font-red" style="font-size: 14px;"></i> {{$data_user_login->role->name}}
                    </a>
                </li>
                <li class="droddown dropdown-separator"><span class="separator"></span></li>
                <li class="dropdown dropdown-user dropdown-dark">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <img class="img-circle" src="{{$data_user_login->getAvatarAtributte()}}">
                        <span class="username username-hide-mobile">{{$data_user_login->name}}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li><a href="{{route('user.profile',$data_user_login->id)}}"><i class="icon-user"></i> Thông tin
                            </a></li>
                        <li class="divider"></li>
                        <li><a href="{{route('logout')}}"><i class="icon-logout"></i> Đăng xuất </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
