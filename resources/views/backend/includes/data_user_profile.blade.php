<div class="portlet light ">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
        </div>
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab" aria-expanded="false">Thông tin</a>
            </li>
            <li class="">
                <a href="#tab_1_2" data-toggle="tab" aria-expanded="false">Đổi avatar</a>
            </li>
            <li class="">
                <a href="#tab_1_3" data-toggle="tab" aria-expanded="true">Đổi mật khẩu</a>
            </li>
        </ul>
    </div>
    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}
            <button type="button" class="close" data-close="alert" aria-hidden="true"></button>
        </div>
    @endif

    @if(Session::has('error'))
        <div class="alert alert-danger">{{Session::get('error')}}
            <button type="button" class="close" data-close="alert"
                    aria-hidden="true"></button>
        </div>
    @endif
    <div class="portlet-body">
        <div class="tab-content">
            <!-- PERSONAL INFO TAB -->
            <div class="tab-pane active" id="tab_1_1">
                <form id="form_edit_profile">
                    {{ csrf_field()  }}
                    <label class="control-label">Name</label>
                    <div class="form-group">
                        <input type="text" name="name"
                               value="{{$data_user_profile->name}}"
                               class="form-control"></div>
                    <label class="control-label">Địa chỉ</label>
                    <div class="form-group">
                        <input type="text" name="address"
                               value="{{$data_user_profile->address}}"
                               class="form-control"></div>
                    <label class="control-label">Số điện thoại</label>
                    <div class="form-group">
                        <input type="text" name="phone"
                               value="{{$data_user_profile->phone}}"
                               class="form-control"></div>
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="text" disabled
                               value="{{$data_user_profile->email}}"
                               class="form-control"></div>
                    <div class="form-group">
                        <label class="control-label">Chức vụ</label>
                        <input type="text" disabled
                               value="{{$data_user_profile->role->name}}"
                               class="form-control"></div>
                    <div class="margiv-top-10">
                        <button onclick="editProfile()" class="btn btn-outline red">
                            Thay đổi
                        </button>
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="tab_1_2">
                <form method="post" action="{{route('user.edit.avatar',$data_user_profile->id)}}" enctype="multipart/form-data">
                    {{ csrf_field()  }}
                    <div class="form-group">
                        <div class="fileinput fileinput-new"
                             data-provides="fileinput">
                            <div class="fileinput-new thumbnail"
                                 style="width: 200px; height: 150px;">
                                <img src="{{$data_user_profile->getAvatarAtributte()}}"
                                     alt=""></div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                <span class="btn btn-outline purple btn-file">
                                    <span class="fileinput-new">Chọn ảnh </span>
                                    <span class="fileinput-exists">Chọn ảnh khác </span>
                                    <input type="file" name="avatar">
                                </span>
                                <a href="javascript:;" class="btn btn-outline dark fileinput-exists" data-dismiss="fileinput"> Xóa </a>
                            </div>
                        </div>
                        <div class="clearfix margin-top-10"><span>Phải chọn kiểu JPG, PNG</span></div>
                    </div>
                    <div class="margin-top-10"><button type="submit" class="btn btn-outline red">Thay đổi</button></div>
                </form>
            </div>
            <div class="tab-pane" id="tab_1_3">
                <form id="form_edit_password">
                    {{ csrf_field() }}
                    <label class="control-label">Mật khẩu cũ</label>
                    <div class="form-group">
                        <input type="password" name="old_password" class="form-control"></div>
                    <label class="control-label">Mật khẩu mới</label>
                    <div class="form-group">
                        <input type="password" id="newpassword" name="password" class="form-control"></div>
                    <label class="control-label">Nhập lại mật khẩu mới</label>
                    <div class="form-group">
                        <input type="password" name="password_confirmation" class="form-control"></div>
                    <div class="margin-top-10">
                        <button onclick="editPassword()" class="btn btn-outline purple">Đổi mật khẩu</button>
                        <button type="reset" class="btn btn-outline dark">Reset</button>
                    </div>
                </form>
            </div>
            <!-- END PRIVACY SETTINGS TAB -->
        </div>
    </div>
</div>