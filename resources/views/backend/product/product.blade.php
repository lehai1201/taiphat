@extends('layouts.layout')
@section('page_head_include')
    <link rel="stylesheet" href="/css/reponsive/reponsive_reponsive.css">
@endsection
@section('page_content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <div class="portlet box grey-cascade">
                        <div class="portlet-title">
                            <div class="caption"><i class="fas fa-globe"></i>Danh sách Sản Phẩm</div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a class="btn green" href="{{route('product.add')}}">
                                                Thêm mới <i class="fas fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(Session::has('success'))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert-success" style="padding: 15px;margin-bottom: 10px">{{Session::get('success')}}</div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" id="search_product" placeholder="Tìm kiếm theo tên">
                                </div>
                            </div>
                           <div id="table_product">
                               @include('backend.product.table_product')
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--MODAL DELETE--}}
    <div class="modal fade" id="delete_product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red"><h5 class="modal-title bg-font-dark bold" id="exampleModalLabel"><i
                                class="icon-trash"></i> XÓA DANH MỤC</h5></div>
                <div class="modal-body">
                    <form id="form_delete_product_tag">
                        <div class="form-group">
                            <div class="form-group font-red-flamingo">Bạn có chắc chắn muốn xóa sản phẩm này?</div>
                        </div>
                        <input type="hidden" id="product_id" name="product_id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="button_delete_product" onclick="deleteProduct()"
                            class="btn btn-outline red">Xóa
                    </button>
                    <button type="button" class="btn btn-outline purple" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('begin_page_level_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });

        //function search
        $("#search_product").on('keypress',function (e) {
            if(e.which==13){
                var search=$("#search_product").val();
               $.ajax({
                   type: 'POST',
                   url: '{{route('product.search')}}',
                   data: {
                       search: search
                   },
                   success: function (data) {
                       $("#table_product").html(data);
                   }
               });
            }
        });

        //function apgination ajax
        $(document).on('click','.pagination a',function(e){
            e.preventDefault();
            var page=$(this).attr('href').split('page=')['1'];
            fetch_data(page);
        })
        function fetch_data(page){
            $.ajax({
                type: 'POST',
                url: '{{route('product.search')}}?page='+page,
                success: function(result){
                    $('#table_product').html(result);
                }
            });
        }
        function showModalDeleted(product_id) {
            $('#product_id').val(product_id);
            $('#delete_product').modal('show');
        }

        function deleteProduct() {
            // $('#button_delete_product').attr('disabled',true);
            var id = $('#product_id').val();
            $.ajax({
                type: 'POST',
                url: '{{route('deleted.product')}}',
                data: {
                    id: id
                },
                success: function (result) {
                    $('#delete_product').modal('hide');
                    swal(result, '', 'success');
                    setTimeout(function () {
                        swal.close();
                        window.location.reload();
                    },1500);
                }
            });
        }
    </script>
@stop
