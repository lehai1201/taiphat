<div class="row">
    <div class="col-md-12"><b>Tổng số sản phẩm: {{count($data_all_product)}}</b></div>
</div>
<table class="table table-striped table-bordered table-hover reponsive_table" id="sample_1">
    <thead>
    <tr>
        <th>Ảnh</th>
        <th>Tên</th>
        <th>Dòng sơn</th>
        <th>Danh mục</th>
        <th>Mã sản phẩm</th>
        <th>Khối lượng (Kg)</th>
        <th>Giá (VNĐ)</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if($data_all_product->isEmpty())
        <tr>
            <td colspan="8" class="font-red">Danh sách trống</td>
        </tr>
    @else
        @foreach($data_all_product as $product)
            <tr>
                <td data-title="Ảnh:"><img width="100" src="/images/product/{{$product->images}}" alt=""></td>
                <td data-title="Tên:">{{$product->title}}</td>
                <td data-title="Dòng sơn:">{{$product->category->name}}</td>
                <td data-title="Danh mục:">{{$product->codeprice->description}}</td>
                <td data-title="Mã code:">{{$product->codeprice->code}}</td>
                <td data-title="Khối lượng:">{{$product->codeprice->kl}}</td>
                <td data-title="Giá:">{{number_format($product->codeprice->price)}}</td>
                <td>
                    <a href="{{route('product.edit',$product->id)}}"
                       class="btn btn-circle btn-outline btn-icon-only green">
                        <i class="fas fa-edit"></i>
                    </a>&nbsp;
                    <a href="javascript:;"
                       onclick="showModalDeleted('{{$product->id}}')"
                       class="btn btn-circle btn-outline btn-icon-only red-thunderbird ">
                        <i class="fas fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div>{{$data_all_product->links()}}</div>
