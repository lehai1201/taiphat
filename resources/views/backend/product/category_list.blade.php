<div class="col-md-12"><br>
    <label >Danh mục</label>
        <select id="category_list_select" name="category"
                class="form-control input-medium"
                style="width:100% !important;">
            @if($data_category->isEmpty())
                <option>Danh sách trống</option>
            @else
                @foreach($data_category as $category)
                    <option @if(!empty($product)) @if($product->code_price_id==$category->id) selected @endif @endif value="{{$category->id}}">{{$category->info()}}</option>
                @endforeach
            @endif
        </select>
</div>