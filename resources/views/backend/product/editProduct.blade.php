@extends('layouts.layout')
@section('page_content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="fas fa-edit"></i>Sửa sản phẩm</div>
                        </div>
                        <div class="portlet-body">
                            @if(Session::has('errors'))
                                <div class="alert-danger"
                                     style="padding: 10px;margin-bottom: 10px">{{Session::get('errors')}}</div>
                            @endif
                                <input type="hidden" id="id_product" value="{{$id}}">
                            <form  class="form-horizontal" method="POST"
                                  action="{{route('product.post.edit',$product->id)}}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="col-md-12">
                                                <input autofocus class="form-control placeholder-no-fix" type="text"
                                                       placeholder="Tên sản phẩm" name="title" value="{{$product->title}}"/>
                                            </div>
                                            <div class="col-md-12"><br>
                                                <label>Mô tả sản phẩm</label>
                                                <textarea style="resize: none" id="description"
                                                          name="description" rows="3"
                                                          placeholder="Mô tả">{!! $product->description !!}</textarea>
                                            </div>
                                            <div class="col-md-12"><br>
                                                <label>Nội dung </label>
                                                <textarea style="resize: none" id="content"
                                                          name="content" rows="3"
                                                          placeholder="Nội dung sản phẩm">{!! $product->content !!}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-12">
                                                <label>Loại sơn</label>
                                                <select class="form-control" name="species" id="species"
                                                        style="width:100% !important;">
                                                    @foreach($data_species as $species)
                                                        <option @if($product->category_id==$species->id) selected @endif value="{{$species->id}}">{{$species->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div id="category_list"></div>
                                            <div class="col-md-12"><br>
                                                <label for="button_img">Chọn ảnh</label>
                                                <div>
                                                    <input id="button_img" name="images" onchange="loadImg(event,this)"
                                                           type="file" value="Chọn ảnh">
                                                </div>
                                            </div>
                                            <div class="col-md-12"><img style="width: 200px;margin-top: 10px"
                                                                        id="img_show" src="/images/product/{{$product->images}}" alt=""></div>
                                        </div>
                                    </div>
                                    <br></div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center">
                                        <button class="btn btn-outline red" type="submit"><i class="fas fa-edit"></i> Sửa </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page_footer_script')
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('content');
            CKEDITOR.replace('description');
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
            id = $("#species :selected").val();
             ajaxLoadCategory(id);
        })
        $("#species").on('change', function () {
            var species_id = $("#species").val();
            ajaxLoadCategory(species_id);
        });
        function ajaxLoadCategory(id_category) {
            var id_product=$("#id_product").val();
            $.ajax({
                type: 'POST',
                url: '{{route('product.edit.ajax')}}',
                data: {
                    id: id_product,
                    id_category: id_category
                },
                success: function (data) {
                    $("#category_list").html(data);
                    $("#category_list_select").select2();
                }
            });
        };
        function loadImg(event, e) {
            console.log(event);
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];
            if ($(e).val() == '') {
                $("#img_show").attr('src', '');
            } else {
                if ($.inArray($(e).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    $("#img_show").attr('src', '');
                    alert('Phải chọn ảnh kiểu ảnh và có đuôi: JPEG,JPG,PNG');
                    $(e).val('');
                } else {
                    var path = URL.createObjectURL(event.target.files[0]);
                    $("#img_show").attr('src', path);
                }
            }
        }
    </script>
@stop
