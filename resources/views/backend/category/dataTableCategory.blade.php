<div class="font-dark bold" style="padding: 4px">Tổng số mã code: {{count($data_categories)}}</div>
<table class="table table-striped table-hover table-bordered reponsive_table">
    <thead class="bg-green-haze font-white">
    <tr>
        <th> Tên danh mục</th>
        <th>Mã code</th>
        <th>Loại sơn</th>
        <th>Đơn vị</th>
        <th>Khối lượng</th>
        <th>Giá</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if($data_categories->isEmpty())
        <tr>
            <td colspan="7">Danh sách trống</td>
        </tr>
    @else
        @foreach($data_categories as $category)
            <tr>
                <td data-title="Tên:">{{$category->description}}</td>
                <td data-title="Code:">{{$category->code}}</td>
                <td data-title="Loại sơn:">{{$category->category->name}}</td>
                <td data-title="Đơn vị:">{{$category->unit}}</td>
                <td data-title="Khối lượng:">{{$category->kl}} Kg</td>
                <td data-title="Giá:">{{number_format($category->price)}}</td>
                <td><a class="font-red" onclick="showModalDeleteCategory({{$category->id}})">Xóa</a>&nbsp;<a
                            onclick="showModalEdit({{$category->id}})">Sửa</a>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div style="clear: both">{{$data_categories->links()}}</div>

