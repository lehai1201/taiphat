@extends('layouts.layout')
@section('page_head_include')
    <link rel="stylesheet" href="/css/reponsive/reponsive_reponsive.css">
@endsection
@section('page_head_include')
    <style>
        td, th {
            text-align: center;
        }
    </style>
@stop
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="page-breadcrumb breadcrumb">
                            <li><a href="{{route('admin')}}">Trang chủ</a><i class="fas fa-circle"></i></li>
                            <li><a href="{{route('category.index')}}">Danh mục sản phẩm</a></li>
                        </ul>
                    </div>
                </div>
                {{--TABLE--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-share font-blue"></i>
                                    <span class="caption-subject font-blue bold uppercase">Danh mục</span>
                                </div>
                                <div class="actions">
                                    <div class="btn-group">
                                        <a class="btn green btn-circle btn-sm" href="javascript:;"
                                           data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Thao
                                            tác <i class="fas fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a class="font-green" href="#new_category" data-toggle="modal"><i
                                                        class="fas fa-tag font-green"></i> Thêm mã code sản phẩm</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a class="font-red" href="#new_catalog" data-toggle="modal"><i
                                                            class="icon-share font-red"></i> Thêm danh mục</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a class="font-yellow" href="#new_species" data-toggle="modal"><i
                                                            class="fas fa-plus font-yellow"></i> Thêm dòng sơn</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#overview_3" data-toggle="tab">Mã code sản phẩm</a>
                                        </li>
                                        <li>
                                            <a href="#overview_2" data-toggle="tab">Danh mục</a>
                                        </li>
                                        <li>
                                            <a href="#overview_1" data-toggle="tab">Dòng sơn</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="overview_1">
                                            <div class="table-responsive">
                                                <div class="font-dark bold" style="padding: 4px">Tổng số loại
                                                    sơn: {{count($data_species)}}</div>
                                                <table class="table  table-hover table-bordered">
                                                    <thead class="bg-blue center font-white">
                                                    <tr>
                                                        <th> Tên danh mục</th>
                                                        <th> Số lượng sản phẩm</th>
                                                        <th> Ngày tạo</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($data_species->isEmpty())
                                                        <tr>
                                                            <td colspan="4" class="font-red text-center">Danh sách trống</td>
                                                        </tr>
                                                    @else
                                                        @foreach($data_species as $species)
                                                            <tr>
                                                                <td>{{$species->name}}
                                                                </td>
                                                                <td>{{count($species->products)}}</td>
                                                                <td>{{$species->getTimeCreated()}}</td>
                                                                <td>
                                                                    @if(!in_array($species->id,$id_dont_delete))
                                                                        <a class="font-red" onclick="showModalDeleteSpecies('{{$species->id}}')">Xóa</a>
                                                                    @endif
                                                                    &nbsp;<a
                                                                            onclick="showModalEditSpecies('{{$species->id}}')">Sửa</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="overview_2">
                                            <div class="table-responsive">
                                                <div class="table-responsive">
                                                    <div class="font-dark bold" style="padding: 4px">Tổng số loại
                                                        sơn: {{count($dataCatalog)}}</div>
                                                    <table class="table  table-hover table-bordered">
                                                        <thead class="bg-blue center font-white">
                                                        <tr>
                                                            <th> Tên danh mục</th>
                                                            <th> Ngày tạo</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if($dataCatalog->isEmpty())
                                                            <tr>
                                                                <td colspan="4" class="font-red text-center">Danh sách trống</td>
                                                            </tr>
                                                        @else
                                                            @foreach($dataCatalog as $catalog)
                                                                <tr>
                                                                    <td>{{$catalog->name}}
                                                                    </td>
                                                                    <td>{{$catalog->getTimeCreated()}}</td>
                                                                    <td>
                                                                        &nbsp;<a onclick="showModalEditCatalog('{{$catalog->id}}')">Sửa</a>
                                                                        <a class="font-red" onclick="showModalDeleteCatalog('{{$catalog->id}}')">Xóa</a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane active" id="overview_3">
                                            <div class="table-responsive">
                                                <div class="row" style="margin-bottom: 10px">
                                                    <div class="col-md-4"><input id="search_input_category" type="text"
                                                                                 class="form-control"
                                                                                 placeholder="Tìm mã code sản phẩm"></div>
                                                </div>
                                                <div id="data_table_category">
                                                    @include('backend.category.dataTableCategory')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL add danh mục--}}
    <div class="modal fade" id="new_catalog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">THÊM MỚI DANH MỤC</h5>
                </div>
                <div class="modal-body">
                    <form id="form_add_catalog">
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Tên danh mục</label>
                            <div class="input-icon">
                                <i class="fas fa-font"></i>
                                <input autofocus class="form-control placeholder-no-fix" type="text"
                                       placeholder="Tên danh mục"
                                       name="name"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_add_catalog" class="btn btn-outline blue">Thêm</button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL edit danh mục--}}
    <div class="modal fade" id="edit_catalog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">SỬA DANH MỤC</h5>
                </div>
                <div class="modal-body">
                    <form id="form_edit_catalog">
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Tên danh mục</label>
                            <div class="input-icon">
                                <i class="fas fa-font"></i>
                                <input autofocus class="form-control placeholder-no-fix" id="catalog_name" type="text"
                                       placeholder="Tên danh mục"
                                       name="name"/>
                                <input type="hidden" id="id_catalog" value="" name="id">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_edit_catalog" onclick="editCatalog()"
                            class="btn btn-outline blue">Cập nhật
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL DELETE catalog--}}
    <div class="modal fade" id="delete_catalog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel"><i class="icon-trash"></i> XÓA DANH
                        MỤC</h5>
                </div>
                <div class="modal-body">
                    <form id="form_delete_catalog_tag">
                        <div class="form-group">
                            <div class="form-group font-red-flamingo">Bạn có chắc chắn muốn xóa?</div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <span> Xóa danh mục,tag màu sẽ ảnh hưởng đến các sản phẩm </span>
                                <div> Bạn cần cân nhắc thật kỹ trước khi thực hiện xóa Danh mục. Vì các lý do sau:</div>
                                <div>
                                    1. Danh mục sẽ mất trên hệ thống, các sản phẩm thuộc danh mục sẽ bị xóa.
                                </div>
                                <div>
                                    2. Dữ liệu bị xóa khó khôi phục được.
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="catalog_deleted_id" name="catalog_id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline purple" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_delete_catalog" onclick="deleteCatalog()"
                            class="btn btn-outline red">Xóa
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL add Dòng sơn--}}
    <div class="modal fade" id="new_species" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">THÊM MỚI LOẠI SƠN</h5>
                </div>
                <div class="modal-body">
                    <form id="form_add_category">
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Tên loại sơn</label>
                            <div class="input-icon">
                                <i class="fas fa-font"></i>
                                <input autofocus class="form-control placeholder-no-fix" type="text"
                                       placeholder="Tên dòng sơn"
                                       name="name"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_add_category" class="btn btn-outline blue">Thêm</button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL ADD code price--}}
    <div class="modal fade" id="new_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">THÊM MỚI DANH MỤC</h5>
                </div>
                <div class="modal-body">
                    <form id="form_add_tag">
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Tên danh mục</label>
                            <div class="input-icon">
                                <i class="fas fa-font"></i>
                                <input autofocus class="form-control placeholder-no-fix" type="text"
                                       placeholder="Tên danh mục"
                                       name="name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <select name="species" class="form-control">
                                        @if($data_species->isEmpty())
                                            <option value="">Danh sách trống</option>
                                        @else
                                            @foreach($data_species as $species)
                                                <option value="{{$species->id}}">{{$species->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <select name="catalog_id" class="form-control">
                                        @if($dataCatalog->isEmpty())
                                            <option value="">Danh sách trống</option>
                                        @else
                                            @foreach($dataCatalog as $catalog)
                                                <option value="{{$catalog->id}}">{{$catalog->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="kl" placeholder="Khối lượng">
                                    <br>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="unit" placeholder="Đơn vị">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="code" placeholder="Code">
                                    <br>
                                </div>
                                <div class="col-md-6">
                                    <input type="number" name="price" class="form-control" min="0" placeholder="Giá">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_add_tag" class="btn btn-outline blue">Thêm</button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL DELETE--}}
    <div class="modal fade" id="delete_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel"><i class="icon-trash"></i> XÓA DANH
                        MỤC</h5>
                </div>
                <div class="modal-body">
                    <form id="form_delete_category_tag">
                        <div class="form-group">
                            <div class="form-group font-red-flamingo">Bạn có chắc chắn muốn xóa?</div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <span> Xóa danh mục,tag màu sẽ ảnh hưởng đến các sản phẩm </span>
                                <div> Bạn cần cân nhắc thật kỹ trước khi thực hiện xóa Danh mục. Vì các lý do sau:</div>
                                <div>
                                    1. Danh mục sẽ mất trên hệ thống, các sản phẩm thuộc danh mục sẽ bị xóa.
                                </div>
                                <div>
                                    2. Dữ liệu bị xóa khó khôi phục được.
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="category_id" name="category_id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline purple" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_delete_category" onclick="deleteCategoryTag()"
                            class="btn btn-outline red">Xóa
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL EDIT--}}
    <div class="modal fade" id="edit_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-green">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">SỬA DANH MỤC</h5>
                </div>
                <div class="modal-body">
                    <div id="data_modal_edit">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_edit_category" class="btn btn-outline blue">Cập nhật</button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL EDIT Dòng sơn--}}
    <div class="modal fade" id="edit_species" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">SỬA LOẠI SƠN</h5>
                </div>
                <div class="modal-body">
                    <form id="form_edit_category">
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Tên loại sơn</label>
                            <div class="input-icon">
                                <i class="fas fa-font"></i>
                                <input autofocus class="form-control placeholder-no-fix" id="species_name" type="text"
                                       placeholder="Tên dòng sơn"
                                       name="name"/>
                                <input type="hidden" id="id_species" value="" name="id">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_edit_category" onclick="editSpecies()"
                            class="btn btn-outline blue">Cập nhật
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL DELETE DÒNG SƠN--}}
    <div class="modal fade" id="delete_species" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel"><i class="icon-trash"></i> XÓA DÒNG SƠN</h5>
                </div>
                <div class="modal-body">
                    <form id="form_delete_species">
                        <div class="form-group">
                            <div class="form-group font-red-flamingo">Bạn có chắc chắn muốn xóa?</div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <span> Xóa dòng sơn sẽ ảnh hưởng đến các sản phẩm </span>
                                <div> Bạn cần cân nhắc thật kỹ trước khi thực hiện xóa Danh mục. Vì các lý do sau:</div>
                                <div>
                                    1. Dòng sơn sẽ mất trên hệ thống, các sản phẩm thuộc danh mục sẽ bị xóa.
                                </div>
                                <div>
                                    2. Dữ liệu bị xóa khó khôi phục được.
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="species_deleted_id" name="id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline purple" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_delete_species" onclick="deleteSpecies()"
                            class="btn btn-outline red">Xóa
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('begin_page_level_js')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });

        $("#search_input_category").on('keypress', function (e) {
            var search = $("#search_input_category").val();
            if (e.which == 13) {
                $.ajax({
                    type: 'POST',
                    url: '{{route('category.search')}}',
                    data: {
                        search: search
                    },
                    success: function (data) {
                        $("#data_table_category").html(data);
                    }
                });
            }
        });
        //paging ajax
        $(document).on('click', '.pagination a', function (e) {
            e.preventDefault();
            var page = $(this).attr('href').split('page=')['1'];
            fetch_data(page);
        })

        function fetch_data(page) {
            $.ajax({
                type: 'POST',
                url: '{{route('category.search')}}?page=' + page,
                success: function (result) {
                    $('#data_table_category').html(result);
                }
            });
        }

        $('#button_add_category').on('click', function (event) {
            event.stopPropagation();
            $("#button_add_category").attr('disabled', true);
            var data_all = $('#form_add_category').serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('category.new.category')}}',
                data: data_all,
                success: function (result) {
                    $('#new_species').modal('hide');
                    if (result == 'success') {
                        $('#button_add_category').prop('disabled', true);
                        swal('Thành công', 'Đã thêm thành công', 'success');
                        setTimeout(function () {
                            window.location.reload();
                            $("li").removeClass('active');
                            $("#category").addClass('active');
                        }, 1500);
                    } else {
                        $("#button_add_category").attr('disabled', false);
                        swal('Lỗi', result, 'error');
                        setTimeout(function () {
                            swal.close();
                            $('#new_species').modal('show');
                        }, 1500)
                    }
                }
            });
        })

        $('#button_add_catalog').on('click', function (event) {
            event.stopPropagation();
            $("#button_add_catalog").attr('disabled', true);
            $('#new_catalog').modal('hide');
            var data_all = $('#form_add_catalog').serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('category.new.catalog')}}',
                data: data_all,
                success: function (result) {
                    $('#new_species').modal('hide');
                    if (result == 'success') {
                        $('#button_add_catalog').prop('disabled', true);
                        swal('Thành công', 'Đã thêm thành công', 'success');
                        setTimeout(function () {
                            window.location.reload();
                            $("li").removeClass('active');
                            $("#category").addClass('active');
                        }, 1500);
                    } else {
                        $("#button_add_catalog").attr('disabled', false);
                        swal('Lỗi', result, 'error');
                        setTimeout(function () {
                            swal.close();
                            $('#new_catalog').modal('show');
                        }, 1500)
                    }
                }
            });
        })

        $('#button_add_tag').on('click', function () {
            var data_all = $('#form_add_tag').serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('category.new.tag')}}',
                data: data_all,
                success: function (result) {
                    $('#new_category').modal('hide');
                    if (result == 'success') {
                        $('#button_add_tag').prop('disabled', true);
                        swal('Thành công', 'Đã thêm thành công', 'success');
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    } else {
                        swal('Lỗi', result, 'error');
                        setTimeout(function () {
                            swal.close();
                            $('#new_category').modal('show');
                        }, 1500)
                    }
                }
            });
        })

        $("#button_edit_category").on('click', function () {
            $("#button_edit_category").attr('disabled', true);
            var data_all = $("#form_edit_category_product").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('post.edit.category')}}',
                data: data_all,
                success: function (data) {
                    $("#edit_category").modal('hide');
                    $("#button_edit_category").attr('disabled', false);
                    if (data == 'success') {
                        swal('Đã sửa thành công', '', 'success');
                        setTimeout(function () {
                            swal.close();
                            window.location.reload();
                        }, 1500);
                    } else {
                        swal(data, '', 'error');
                    }
                }
            });
        });

        function showModalDeleteCategory(category_id) {
            $('#category_id').val(category_id);
            $('#delete_category').modal('show');
        }

        function deleteCategoryTag() {
            $('#button_delete_category').attr('disabled', true);
            var data_all = $('#form_delete_category_tag').serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('deleted.tag')}}',
                data: data_all,
                success: function (result) {
                    if (result == 'success') {
                        $('#delete_category').modal('hide');
                        swal('Đã xóa thành công', '', 'success');
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    }
                }
            });
        }

        function showModalEdit(id) {
            $.ajax({
                type: 'POST',
                url: '{{route('edit.category')}}',
                data: {
                    id: id
                },
                success: function (data) {
                    $("#data_modal_edit").html(data);
                    $("#id_category_hidden").val(id);
                    $("#edit_category").modal('show');
                }
            });
        }

        function showModalEditSpecies(id) {
            $.ajax({
                type: 'POST',
                url: '{{route('species.show.modal.edit')}}',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function (data) {
                    $("#species_name").val(data['name']);
                    $("#id_species").val(data['id']);
                    $("#edit_species").modal('show');
                }
            });
        }

        function showModalEditCatalog(id) {
            $.ajax({
                type: 'POST',
                url: '{{route('category.show.catalog')}}',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function (data) {
                    $("#catalog_name").val(data['name']);
                    $("#id_catalog").val(data['id']);
                    $("#edit_catalog").modal('show');
                }
            });
        }

        function editCatalog() {
            $("#button_edit_catalog").attr('disabled', true);
            var data_all = $("#form_edit_catalog").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('catalog.edit')}}',
                data: data_all,
                success: function (data) {
                    $("#edit_catalog").modal('hide');
                    if (data == 'success') {
                        swal('Cập nhật thành công', '', 'success');
                        setTimeout(function () {
                            swal.close();
                            window.location.reload();
                        }, 1500)
                    } else {
                        $("#button_edit_catalog").attr('disabled', false);
                        swal(data, '', 'error');
                        setTimeout(function () {
                            swal.close();
                            $("#edit_catalog").modal('show');
                        }, 1500)
                    }
                }
            });
        }

        function editSpecies() {
            $("#button_edit_category").attr('disabled', true);
            var data_all = $("#form_edit_category").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('species.edit')}}',
                data: data_all,
                success: function (data) {
                    $("#edit_species").modal('hide');
                    if (data == 'success') {
                        swal('Cập nhật thành công', '', 'success');
                        setTimeout(function () {
                            swal.close();
                            window.location.reload();
                        }, 1500)
                    } else {
                        $("#button_edit_category").attr('disabled', false);
                        swal(data, '', 'error');
                        setTimeout(function () {
                            swal.close();
                            $("#edit_species").modal('show');
                        }, 1500)
                    }
                }
            });
        }

        function showModalDeleteSpecies(id){
            $("#species_deleted_id").val(id);
            $('#delete_species').modal('show');
        }

        function showModalDeleteCatalog(id){
            $("#catalog_deleted_id").val(id);
            $('#delete_catalog').modal('show');
        }

        function deleteSpecies(){
            $("#button_delete_species").attr('disabled',true);
            var id=$("#species_deleted_id").val();
            $.ajax({
                type: 'POST',
                url: '{{route('species.deleted')}}',
                data:{
                    id: id
                },
                success: function (data) {
                    $('#delete_species').modal('hide');
                    if(data=='success'){
                        swal('Đã xóa thành công','','success');
                        setTimeout(function () {
                            window.location.reload();
                        },1500)
                    }
                }
            });
        }

        function deleteCatalog(){
            $("#button_delete_catalog").attr('disabled',true);
            var id=$("#catalog_deleted_id").val();
            $.ajax({
                type: 'POST',
                url: '{{route('catalog.delete')}}',
                data:{
                    id: id
                },
                success: function (data) {
                    $('#delete_catalog').modal('hide');
                    if(data=='success'){
                        swal('Đã xóa thành công','','success');
                        setTimeout(function () {
                            window.location.reload();
                        },1500)
                    }
                }
            });
        }
    </script>
@stop
