<form id="form_edit_category_product">
    <input type="hidden" name="id" id="id_category_hidden" value="">
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Tên danh mục</label>
        <div class="input-icon">
            <i class="fas fa-font"></i>
            <input autofocus class="form-control placeholder-no-fix" type="text"
                   placeholder="Tên danh mục"
                   name="name" value="{{$category->description}}" />
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <select name="species" class="form-control">
                    @if($data_species->isEmpty())
                        <option value="">Danh sách trống</option>
                    @else
                        @foreach($data_species as $species)
                            <option @if($category->category_id==$species->id) selected @endif value="{{$species->id}}">{{$species->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <select name="catalog_id" class="form-control">
                    @if($dataCatalog->isEmpty())
                        <option value="">Danh sách trống</option>
                    @else
                        @foreach($dataCatalog as $catalog)
                            <option @if($category->catalog_id==$catalog->id) selected @endif value="{{$catalog->id}}">{{$catalog->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <input type="number" class="form-control" name="kl" placeholder="Khối lượng" value="{{$category->kl}}">
                <br>
            </div>
            <div class="col-md-6">
                <input type="text" class="form-control" name="unit" placeholder="Đơn vị" value="{{$category->unit}}">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="form-control" name="code" placeholder="Code" value="{{$category->code}}">
                <br>
            </div>
            <div class="col-md-6">
                <input type="number" name="price" class="form-control" min="0" placeholder="Giá" value="{{$category->price}}">
            </div>
        </div>
    </div>
</form>
