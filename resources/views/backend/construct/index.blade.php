@extends('layouts.layout')
@section('page_level_plugin')
    <link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/css/reponsive/reponsive_reponsive.css">
    <style>
        .swal-icon--success__ring {
            border-radius: 50% !important;
        }
    </style>
@stop
@section('page_content')
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="{{route('admin')}}">Trang chủ</a>
                                <i class="fas fa-circle"></i>
                            </li>
                            <li>
                                <a href="{{route('construction.index')}}">Công trình tiêu biểu</a>
                            </li>
                        </ul>
                    </div>
                </div>
                {{--Nội dung--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-share font-blue"></i>
                                    <span class="caption-subject font-blue bold uppercase">Danh mục</span>
                                </div>
                                <div class="actions">
                                    <div class="btn-group">
                                        <a class="btn green btn-circle btn-sm" href="javascript:;"
                                           data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Thao
                                            tác <i class="fas fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a class="font-red" href="#new_construct" data-toggle="modal"><i
                                                            class="fas fa-gopuram font-red"></i> Thêm mới công trình
                                                    tiêu biểu</a>
                                            </li>
                                            <li class="divider"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#overview_1" data-toggle="tab">Công trình tiêu biểu</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="overview_1">
                                            <div class="table-responsive">
                                                <div class="font-dark bold" style="padding: 4px">Tổng số công
                                                    trình: {{count($data_all_construct)}}
                                                </div>
                                                @if(count($data_all_construct)<=5)
                                                    <div class="font-red"><h4>PHẢI CÓ ÍT NHẤT 5 CÔNG TRÌNH</h4></div>
                                                @endif
                                                <table class="table  table-hover table-bordered reponsive_table">
                                                    <thead class="bg-blue center font-white">
                                                    <tr>
                                                        <th> Ảnh</th>
                                                        <th> Tiêu đề</th>
                                                        <th> Ngày tạo</th>
                                                        <th>Thao tác</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($data_all_construct->isEmpty())
                                                        <tr>
                                                            <td colspan="4" class="font-red" style="text-align: center">
                                                                Danh sách trống
                                                            </td>
                                                        </tr>
                                                    @else
                                                        @foreach($data_all_construct as $construct)
                                                            <tr>
                                                                <td data-title="Ảnh:"><a class="media_construct" rel="gallery1"
                                                                       title="{{$construct->title}}"
                                                                       href="/images/congtrinh/{{$construct->images}}">
                                                                        <img style="width: 200px"
                                                                             src="/images/congtrinh/{{$construct->images}}">
                                                                    </a>
                                                                </td>
                                                                <td data-title="Tiêu đề: ">{{$construct->title}}</td>
                                                                <td data-title="Ngày tạo:">{{$construct->getTimeCreated()}}</td>
                                                                <td>
                                                                    @if(count($data_all_construct)>5)
                                                                        <a onclick="deleteConstruct({{$construct->id}})"
                                                                           class="font-red">Xóa</a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL Thêm công trình--}}
    <div class="modal fade" id="new_construct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">THÊM MỚI CÔNG TRÌNH TIÊU BIỂU</h5>
                </div>
                <div class="modal-body">
                    <form id="form_add_construct" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Tên công trình</label>
                            <div class="input-icon">
                                <i class="fas fa-font"></i>
                                <input autofocus class="form-control placeholder-no-fix" type="text"
                                       placeholder="Tiêu đề"
                                       name="title"/>
                                <div style="margin-top: 10px" class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                         style="width: 300px; height: 250px;"></div>
                                    <div>
                                        <span class="btn red btn-outline btn-file"><span
                                                    class="fileinput-new"> Chọn ảnh </span>
                                            <span class="fileinput-exists"> Thay đổi </span>
                                            <input type="file" name="images_construct">
                                        </span>
                                        <a href="javascript:;" class="btn red fileinput-exists"
                                           data-dismiss="fileinput"> Xóa </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline dark" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_add_construct" onclick="addConstruct()"
                            class="btn btn-outline blue">Thêm
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL XÓA CÔNG TRÌNH--}}
    <div class="modal fade" id="delete_construct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red">
                    <h3 class="modal-title bg-font-dark bold" id="exampleModalLabel">XÁC NHẬN XÓA</h3>
                </div>
                <div class="modal-body">
                    <h4 class="font-red">Bạn có chắc chắn muốn xóa công trình này?</h4>
                    <input type="hidden" name="id" id="id_construct" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline purple" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_delete_construct"
                            class="btn btn-outline red">Xóa
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('begin_page_level_js')
    <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script type="text/javascript" src="/fancybox/lib/jquery.mousewheel.pack.js"></script>
    <script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>

@stop
@section('page_footer_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
            $(".media_construct").fancybox({
                prevEffect: 'none',
                nextEffect: 'none',
            });
            $(".media_slider").fancybox({
                prevEffect: 'none',
                nextEffect: 'none',
            });
        });

        function addConstruct() {
            var form = $('#form_add_construct')[0];
            var formData = new FormData(form);
            $.ajax({
                type: 'POST',
                url: '{{route('construct.add')}}',
                data: formData,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('#button_add_construct').attr('disabled', true);
                    $('#new_construct').modal('hide');
                    if (result == 'success') {
                        swal('Thành công', '', 'success');
                        setTimeout(function () {
                            swal.close();
                            window.location.reload();
                        }, 1000);
                    } else {
                        $('#button_add_construct').attr('disabled', false);
                        swal(result, '', 'error');
                        setTimeout(function () {
                            swal.close();
                            $("#new_construct").modal('show');
                        }, 1500);
                    }
                }
            });
        }

        function deleteConstruct(id) {
            $("#id_construct").val(id);
            $("#delete_construct").modal('show');
        }

        $("#button_delete_construct").on("click", function (e) {
            e.preventDefault();
            $("#button_delete_construct").attr('disabled', true);
            var id = $("#id_construct").val();
            $.ajax({
                type: 'POST',
                url: '{{route('construct.delete')}}',
                data: {
                    id: id
                },
                success: function (result) {
                    $("#delete_construct").modal('hide');
                    swal(result, '', 'success');
                    setTimeout(function () {
                        swal.close();
                        window.location.reload();
                    }, 1500)
                }
            });
        })
    </script>
@stop
