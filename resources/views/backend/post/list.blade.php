@extends('layouts.layout')
@section('page_head_include')
    <link rel="stylesheet" href="/css/reponsive/reponsive_reponsive.css">
@endsection
@section('page_head_include')
    <style>
        .select2-container {
            border: 1px solid #dedede;
            border-radius: 5px;
            line-height: 30px;
        }
    </style>
@endsection
@section('page_content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <div class="portlet box grey-cascade">
                        <div class="portlet-title">
                            <div class="caption"><i class="fas fa-globe"></i>Danh sách bài viết</div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a class="btn green" href="{{route('post.add')}}">
                                                Thêm mới <i class="fas fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                    @if(!$postCheck->isEmpty())
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <select class="form-control select2-single " name="postcheck"
                                                        id="listPost">
                                                    @foreach($postCheck as $check)
                                                        <option value="{{$check->id}}">{{$check->getTitlePost()}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-primary" id="button_check_post"
                                                    onclick="checkPost()">Ghim lên trang chủ <i class="fas fa-tags"></i>
                                            </button>
                                        </div>
                                    @endif
                                </div>
                                @if(Session::has('message'))
                                    <div class="alert-success" style="padding: 5px;">{{Session::get('message')}}</div>
                                @endif
                            </div>
                            <table class="table table-striped table-bordered table-hover reponsive_table" id="sample_1">
                                <thead>
                                <tr>
                                    <th>Ảnh đại diện</th>
                                    <th>Tiêu đề</th>
                                    <td>Loại</td>
                                    <th>Mô tả</th>
                                    <th>Ngày tạo</th>
                                    <th>Người tạo</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(!$posts->isEmpty())
                                    @foreach($posts as $post)
                                        <tr class="odd gradeX">
                                            <td data-title="Ảnh:"><img src="/images/post/{{$post->images}}" style="width: 200px" alt="">
                                            </td>
                                            <td data-title="Tiêu đề:">
                                                <p style="width: 150px;white-space: nowrap; text-overflow: ellipsis;overflow: hidden;">{{$post->title}}</p>
                                            </td>
                                            <td data-title="Loại:">
                                                @if($post->type==IS_POST) Tin tức @endif
                                                @if($post->type==IS_EVENT) Sự kiện @endif
                                                @if($post->type==IS_ADVISOTY) Tư vấn của chuyên gia @endif
                                            </td>
                                            <td data-title="Mô tả:">
                                                <p style="width: 150px;white-space: nowrap; text-overflow: ellipsis;overflow: hidden;">{{$post->description}}</p>
                                            </td>
                                            <td data-title="Ngày tạo:">{{$post->getTimeCreated()}}</td>
                                            <td data-title="Người tạo:">{{$post->user->name}}</td>
                                            <td>
                                                <a class="font-blue" href="{{route('post.edit',$post->id)}}">Sửa</a>&nbsp;
                                                @if($post->id==$post_selected->post_id)
                                                    <p class="font-yellow">Đang được ghim</p>
                                                @else
                                                    <a class="font-red" onclick="loadModalDelete({{$post->id}})">Xóa</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>
                            </table>
                            <div>{{$posts->links()}}</div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    {{--MODAL DELETE--}}
    <div class="modal fade" id="delete_post" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel"><i class="icon-trash"></i> XÓA BÀI
                        VIẾT</h5>
                </div>
                <div class="modal-body">
                    <form id="form_delete_category_tag">
                        <div class="form-group">
                            <div class="form-group font-red-flamingo">Bạn có chắc chắn muốn xóa bài viết này?</div>
                        </div>
                        <input type="hidden" id="post_id" name="post_id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="button_delete_post"
                            class="btn btn-outline red">Xóa
                    </button>
                    <button type="button" class="btn btn-outline dark" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('begin_page_level_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
            $("#listPost").select2();
        });

        function checkPost() {
            var id = $("#listPost").val();
            $.ajax({
                type: 'POST',
                url: '{{route('post.check')}}',
                data: {
                    id: id
                },
                success: function (data) {
                    if (data == 'success') {
                        toastr.success('Đã ghim thành công');
                    }
                }
            });
        }

        function loadModalDelete(id) {
            $("#post_id").val(id);
            $("#delete_post").modal('show');
        }

        $("#button_delete_post").on('click', function () {
            $("#button_delete_post").attr('disabled', true);
            var id = $("#post_id").val();
            $.ajax({
                type: 'POST',
                url: '{{route('deleted.post')}}',
                data: {
                    id: id
                },
                success: function () {
                    $("#delete_post").modal('hide');
                    swal('Đã xóa thành công', '', 'success');
                    setTimeout(function () {
                        swal.close();
                        window.location.reload();
                    }, 1500);
                }
            });
        });
    </script>
@stop
