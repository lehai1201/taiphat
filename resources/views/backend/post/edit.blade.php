
@extends('layouts.layout')
@section('page_content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="fas fa-plus"></i>Sửa</div>
                        </div>
                        <div class="portlet-body">
                            @if(Session::has('errors'))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="padding: 10px;margin-bottom: 10px" class="alert-danger">{{Session::get('errors')}}</div>
                                    </div>
                                </div>
                            @endif
                            <form id="form_add_post" class="form-horizontal" method="POST"
                                  action="{{route('post.submit.edit',$post->id)}}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="col-md-12">
                                                <label>Tiêu đề</label>
                                                <input class="form-control placeholder-no-fix" type="text"
                                                       placeholder="Tiêu đề" name="title" value="{{$post->title}}"/>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Mô tả</label>
                                                <input class="form-control placeholder-no-fix" type="text"
                                                       placeholder="Mô tả" name="description" value="{{$post->description}}" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-12">
                                                <label>Loại bài viết</label>
                                                <select class="form-control" name="type" id="type"
                                                        style="width:100% !important;">
                                                    <option value="{{IS_POST}}" @if($post->type==IS_POST) selected @endif>Tin tức</option>
                                                    <option value="{{IS_EVENT}}" @if($post->type==IS_EVENT) selected @endif>Sự kiện</option>
                                                    <option value="{{IS_ADVISOTY}}" @if($post->type==IS_ADVISOTY) selected @endif>Tư vấn của chuyên gia</option>
                                                </select>
                                            </div>
                                            <div id="category_list"></div>
                                            <div class="col-md-12"><br>
                                                <label for="button_img">Ảnh đại diện</label>
                                                <div>
                                                    <input name="images" onchange="loadAvatar(event,this)"
                                                           type="file" value="Chọn ảnh">
                                                </div>
                                            </div>
                                            <div class="col-md-12"><img style="width: 200px;margin-top: 10px"
                                                                        id="img_show" src="/images/post/{{$post->images}}" alt=""></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><br>
                                        <label>Nội dung</label>
                                        <textarea style="resize: none" id="description_post"
                                                  name="content" rows="10"
                                                  placeholder="content">{!! $post->content !!}</textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center">
                                        <button class="btn btn-outline blue" type="submit"><i class="fas fa-edit"></i> Cập nhật
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page_footer_script')
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var editor = CKEDITOR.replace('description_post', {
                uiColor: '#CCEAEE',
                height: 400,
                filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
                filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
                filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
                filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
                filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
                filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
            });
            CKFinder.setupCKEditor(editor);
        });

        function loadAvatar(event, e) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];
            if ($(e).val() == '') {
                $("#img_show").attr('src', '');
            } else {
                if ($.inArray($(e).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    $("#img_show").attr('src', '');
                    alert('Phải chọn ảnh kiểu ảnh và có đuôi: JPEG,JPG,PNG');
                    $(e).val('');
                } else {
                    var path = URL.createObjectURL(event.target.files[0]);
                    $("#img_show").attr('src', path);
                }
            }
        }
    </script>
@endsection

