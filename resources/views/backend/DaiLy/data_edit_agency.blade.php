<form id="form_edit_agency">
    <div class="row">
        <input type="hidden" name="id" value="{{$agency->id}}">
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-icon">
                    <i class="fas fa-font"></i>
                    <input class="form-control placeholder-no-fix" type="text"
                           placeholder="Họ và tên"
                           name="name" value="{{$agency->name}}"/>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Số điện thoại</label>
                <div class="input-icon">
                    <i class="fas fa-phone-square"></i>
                    <input class="form-control placeholder-no-fix" type="text"
                           placeholder="Số điện thoại"
                           name="phone" value="{{$agency->phone}}"/></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Địa chỉ</label>
                <div class="input-icon">
                    <i class="fas fa-check"></i>
                    <input class="form-control placeholder-no-fix" type="text"
                           placeholder="Địa chỉ"
                           name="address" value="{{$agency->address}}"/>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Khu vực</label>
                <select name="area" class="form-control">
                    @foreach(config('constant.BRANCH') as $key =>$value)
                        <option {{ $agency->area == $key ? 'selected' : '' }} value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <label>Code Map:</label>
            <textarea style="resize: none" class="form-control" name="map" id="" cols="30"
                      rows="10">{{$agency->code_map}}</textarea>
        </div>
    </div>
</form>
