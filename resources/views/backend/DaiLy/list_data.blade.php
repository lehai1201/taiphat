<div>Tổng số đại lý: <b>{{count($data_agencies)}}</b></div>
<table class="table table-striped table-bordered table-hover no-footer reponsive_table"
       role="grid"
       aria-describedby="sample_1_info">
    <thead class="bg-blue bg-font-grey-gallery">
    <tr role="row">
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1">Tên đại lý
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"> Địa chỉ
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"> Số điện thoại
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"> Chi nhánh
        </th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if($data_agencies->isEmpty())
        <tr>
            <td colspan="7">
                <h5 class="font-red">Danh sách trống</h5>
            </td>
        </tr>
    @else
        @foreach($data_agencies as $agency)
            <tr>
                <td data-title="Tên đại lý:">{{$agency->name}}</td>
                <td data-title="Địa chỉ:">{{$agency->address}}</td>
                <td data-title="Số điện thoại:">{{$agency->phone}}</td>
                <td data-title="Chi nhánh:">{{ $agency->getAreaAtributte() }}</td>
                <td><a class="font-green" href="#" onclick="showModalEdit('{{$agency->id}}')">Sửa</a>&nbsp;<a class="font-red" href="#" onclick="showModalDelete('{{$agency->id}}')">Xóa</a></td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{--<div class="pagination">{!! $data_user_all->links() !!}</div>--}}
