@extends('layouts.layout')
@section('page_level_plugin')
    <link rel="stylesheet" href="/css/reponsive/reponsive_reponsive.css">
@endsection
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin')}}">Trang chủ</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('daily.index')}}">Đại lý</a>
                    </li>
                </ul>
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fas fa-store-alt"></i> Danh sách đại lý
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body">
                                                <div class="table-toolbar">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="btn-group">
                                                                <button data-toggle="modal"
                                                                        data-target="#modal_add_agency"
                                                                        class="btn sbold green"> Thêm mới
                                                                    <i class="fas fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row" style="padding: 0px;">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input id="search_agency"
                                                                       style="border-radius: 4px !important;"
                                                                       type="text" class="form-control"
                                                                       placeholder="Tìm theo tên..">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="data_list">
                                                        @include('backend.DaiLy.list_data')
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--MODAL ADD--}}
    <div class="modal fade" id="modal_add_agency" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">ĐĂNG KÝ</h5>
                </div>
                <div class="modal-body">
                    <form id="form_add_agency">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{--<label class="control-label visible-ie8 visible-ie9">Họ và tên</label>--}}
                                    <div class="input-icon">
                                        <i class="fas fa-font"></i>
                                        <input class="form-control placeholder-no-fix" type="text"
                                               placeholder="Họ và tên"
                                               name="name"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label visible-ie8 visible-ie9">Số điện thoại</label>
                                    <div class="input-icon">
                                        <i class="fas fa-phone-square"></i>
                                        <input class="form-control placeholder-no-fix" type="text"
                                               placeholder="Số điện thoại"
                                               name="phone"/></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label visible-ie8 visible-ie9">Địa chỉ</label>
                                    <div class="input-icon">
                                        <i class="fas fa-check"></i>
                                        <input class="form-control placeholder-no-fix" type="text"
                                               placeholder="Địa chỉ"
                                               name="address"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label visible-ie8 visible-ie9">Chi nhánh</label>
                                    <select name="area" class="form-control">
                                        @foreach(config('constant.BRANCH') as $key =>$value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Code Map:</label>
                                <textarea style="resize: none" class="form-control" name="map" id="" cols="30"
                                          rows="10"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="button_add_agency" class="btn btn-outline blue">Thêm</button>
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>
    {{--MODAL DELETE--}}
    <div class="modal fade" id="modal_delete_agency" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red">
                    <h4 class="modal-title bg-font-dark bold" id="exampleModalLabel">Xóa đại lý</h4>
                </div>
                <div class="modal-body">
                    <form id="form_add_color">
                        <div class="form-group">
                            <div>Nếu xóa sẽ không thể khôi phục được, Có chắc chắn đại lý này?</div>
                            <input type="hidden" id="agency_id" value="">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline blue" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_delete_agency" onclick="deleteAgency()"
                            class="btn btn-outline red">Xóa
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL EDIT--}}
    <div class="modal fade" id="modal_edit_agency" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h4 class="modal-title bg-font-dark bold" id="exampleModalLabel">Sửa đại lý</h4>
                </div>
                <div class="modal-body">
                    <div id="data_edit_agency">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline dark" data-dismiss="modal">Hủy</button>
                    <button type="submit" id="button_edit_agency"
                            class="btn btn-outline blue" onclick="editAgency()">Thực hiện
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('begin_page_level_js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });
        $("#button_add_agency").on('click', function () {
            $("#button_add_agency").attr('disabled', true);
            var data_all = $("#form_add_agency").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('daily.add')}}',
                data: data_all,
                success: function (data) {
                    $("#modal_add_agency").modal('hide');
                    if (data == 'success') {
                        swal('Thêm đại lí thành công', '', 'success');
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500)
                    } else {
                        $("#button_add_agency").attr('disabled', false);
                        swal(data, '', 'error');
                        setTimeout(function () {
                            swal.close();
                            $("#modal_add_agency").modal('show');
                        }, 1500)
                    }
                }
            });
        });

        function showModalDelete(id) {
            $("#agency_id").val(id);
            $("#modal_delete_agency").modal('show');
        }

        function deleteAgency() {
            var id = $("#agency_id").val();
            $.ajax({
                type: 'POST',
                url: '{{route('daily.delete')}}',
                data: {
                    id: id
                },
                success: function () {
                    $("#modal_delete_agency").modal('hide');
                    swal('Đã xóa thành công', '', 'success');
                    setTimeout(function () {
                        window.location.reload();
                    }, 1500)
                }
            });
        }

        function showModalEdit(id) {
            $("#modal_edit_agency").modal('show');
            $.ajax({
                type: 'POST',
                url: '{{route('daily.show.modal')}}',
                data: {
                    id: id
                },
                success: function (data) {
                    $("#data_edit_agency").html(data);
                    $("#modal_edit_agency").modal('show');
                }
            });
        }

        function editAgency() {
            $("#button_edit_agency").attr('disabled', true);
            data_all = $("#form_edit_agency").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('daily.edit')}}',
                data: data_all,
                success: function (data) {
                    $("#modal_edit_agency").modal('hide');
                    if (data == 'success') {
                        swal('Đã sửa thành công', '', 'success');
                        setTimeout(function () {
                            swal.close();
                            window.location.reload();
                        }, 1500);
                    } else {
                        $("#button_edit_agency").attr('disabled', false);
                        swal(data, '', 'error');
                        setTimeout(function () {
                            $("#modal_edit_agency").modal('show');
                        }, 1500)
                    }
                }
            });
        }

        $("#search_agency").on('keypress', function (e) {
            if (e.which == 13) {
                var search = $("#search_agency").val();
                $.ajax({
                    type: 'POST',
                    url: '{{route('daily.search')}}',
                    data: {
                        search: search
                    },
                    success: function (data) {
                        $("#data_list").html(data);
                    }
                });
            }
        })
    </script>
@endsection
