@extends('layouts.layout')
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin')}}">Trang chủ</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('meta.index')}}">Meta</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <p>Thêm mới</p>
                    </li>
                </ul>
                <div class="page-content-inner">
                    @if(Session::has('errors'))
                        @foreach(Session::get('errors') as $error)
                            <div class="alert-danger"
                                 style="padding: 10px;margin-bottom: 10px">{{ $error }}</div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">Thêm mới</div>
                                </div>
                                <div class="portlet-body">
                                    <form action="{{ route('meta.store') }}" method="POST">
                                        @csrf
                                        <br>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Thẻ meta</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="tag" maxlength="200"  value="{{ old('tag') }}" placeholder="<meta name='description' content='nesan'>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-sm-2 col-form-label">Vị trí</label>
                                            <div class="col-sm-10">

                                                @foreach(config('constant.META') as $key => $value)
                                                    <input class="form-check-input" type="radio"
                                                           name="type" id="inlineRadio-{{ $key }}" {{ $key == 0 ? 'checked' : '' }}
                                                           value="{{ $key }}">
                                                    <label class="form-check-label" for="inlineRadio-{{ $key }}">{{ $value }}</label>&nbsp;&nbsp;
                                                @endforeach

                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2"> Lưu</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
