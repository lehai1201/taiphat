@extends('layouts.layout')
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin')}}">Trang chủ</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('meta.index')}}">Meta</a>
                    </li>
                </ul>


                <div class="page-content-inner">
                    @if(Session::has('errors'))
                        <div class="alert-danger"
                             style="padding: 10px;margin-bottom: 10px">{{ Session::get('errors') }}</div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert-success"
                             style="padding: 10px;margin-bottom: 10px">{{ Session::get('success') }}</div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fas fa-star"></i> Meta
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body">
                                                <div class="table-toolbar">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="btn-group">
                                                                <a class="btn sbold green"
                                                                   href="{{ route('meta.create') }}"><i class="fas fa-plus"></i> Thêm mới</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div id="data_list">
                                                        <table
                                                            class="table table-striped table-bordered table-hover no-footer reponsive_table"
                                                            role="grid"
                                                            aria-describedby="sample_1_info">
                                                            <thead class="bg-blue bg-font-grey-gallery">
                                                            <tr role="row">
                                                                <th tabindex="0"
                                                                    aria-controls="sample_1" rowspan="1" colspan="1">
                                                                    Nội dung
                                                                </th>
                                                                <th>Thể loại
                                                                </th>
                                                                <th>Ngày thêm
                                                                </th>
                                                                <th>Thao tác</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if($listMeta->isEmpty())
                                                                <tr>
                                                                    <td colspan="4" class="text-center">
                                                                        <h5 class="font-red">Danh sách trống</h5>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                @foreach($listMeta as $meta)
                                                                    <tr>
                                                                        <td style="width: 20%;">
                                                                            {{ $meta->tag }}
                                                                        </td>
                                                                        <td style="width: 20%;">{{ $meta->getType() }}</td>
                                                                        <td style="width: 20%;">{{ date('d/m/Y', strtotime($meta->created_at)) }}</td>
                                                                        <td style="width: 10%;"><a class="font-green"
                                                                                                   href="{{ route('meta.edit', $meta->id) }}">Sửa</a>&nbsp;<a
                                                                                class="font-red"
                                                                                href="javascript:void(0)"
                                                                                onclick="showModalDeleted('{{$meta->id}}')">Xóa</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                        <div class="pagination">{!! $listMeta->links() !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete_price" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-red"><h5 class="modal-title bg-font-dark bold" id="exampleModalLabel"><i
                            class="icon-trash"></i> XÓA META</h5></div>
                <div class="modal-body">
                    <form id="form_delete_product_tag">
                        <div class="form-group">
                            <div class="form-group font-red-flamingo">Bạn có chắc chắn muốn xóa thẻ này?</div>
                        </div>
                        <input type="hidden" id="price_id" name="price_id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" onclick="deleteRate()"
                            class="btn btn-outline red">Xóa
                    </button>
                    <button type="button" class="btn btn-outline purple" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('begin_page_level_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });

        function showModalDeleted(price_id) {
            $('#price_id').val(price_id);
            $('#delete_price').modal('show');
        }

        function deleteRate() {
            var id = $('#price_id').val();
            $.ajax({
                type: 'POST',
                url: '{{route('meta.delete')}}',
                data: {
                    id: id
                },
                success: function (result) {
                    $('#delete_price').modal('hide');
                    swal(result, '', 'success');
                    setTimeout(function () {
                        swal.close();
                        window.location.reload();
                    }, 1500);
                }
            });
        }
    </script>
@endsection

