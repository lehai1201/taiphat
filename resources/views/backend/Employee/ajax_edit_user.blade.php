<form action="" id="data_edit_user">
    {{csrf_field()}}
    <div class="form-body">
        <input type="hidden" name="id" value="{{$data_user->id}}">
        <div class="form-group">
            <label>Tên</label>
            <div class="input-group">
                <span class="input-group-addon input-circle-left"><i class="fas fa-user"></i>
                </span>
                <input type="text" class="form-control" name="name" value="{{$data_user->name}}"></div>
        </div>
        <div class="form-group">
            <label>Quyền</label>
            <div class="input-group">
                <span class="input-group-addon input-circle-left"><i class="fas fa-user"></i>
                </span>
                <input disabled type="text" class="form-control" value="{{$data_user->role->name}}"></div>
        </div>
        <div class="form-group">
            <label>Địa chỉ</label>
            <div class="input-group">
                <span class="input-group-addon input-circle-left"><i class="fas fa-user"></i>
                </span>
                <input type="text" class="form-control" name="address" value="{{$data_user->address}}"></div>
        </div>
        <div class="form-group">
            <label>Số điện thoại</label>
            <div class="input-group">
                <span class="input-group-addon input-circle-left"><i class="fas fa-user"></i>
                </span>
                <input type="text" class="form-control" name="phone" value="{{$data_user->phone}}"></div>
        </div>
    </div>
    <div class="form-group">
        <label data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
            <input name="is_change_password" value="{{IS_CHECKED}}" type="checkbox"/> Gán mật khẩu
        </label>
    </div>
    <div id="collapseOne" aria-expanded="false" class="collapse">
        <div class="form-group">
            <label>Mật khẩu mới</label>
            <div class="input-group col-md-12">
                            <span class="input-group-addon input-circle-left">
                                <i class="fas fa-key"></i>
                            </span>
                <input type="text" name="password" class="form-control"
                       placeholder="Mật khẩu mới"></div>
        </div>
    </div>
    </div>
</form>
