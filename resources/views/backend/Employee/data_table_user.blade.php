<div>Tổng số nhân viên: <b>{{count($data_user_all)}}</b></div>
<table class="table table-striped table-bordered table-hover no-footer reponsive_table"
       role="grid"
       aria-describedby="sample_1_info">
    <thead class="bg-blue bg-font-grey-gallery">
    <tr role="row">
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"
            style="width: 161px;"> Tên
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"
            style="width: 161px;"> Email
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"
            style="width: 161px;"> Địa chỉ
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"
            style="width: 161px;"> Tên đăng nhập
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"
            style="width: 161px;"> Điện thoại
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"
            style="width: 161px;"> Quyền
        </th>
        <th tabindex="0"
            aria-controls="sample_1" rowspan="1" colspan="1"
            style="width: 161px;">
        </th>
    </tr>
    </thead>
    <tbody>
    @if($data_user_all->isEmpty())
        <tr>
            <td colspan="7">
                <h5 class="font-red">Danh sách trống</h5>
            </td>
        </tr>
    @else
        @foreach($data_user_all as $data_user)
            <tr class="gradeX odd" role="row">
                <td data-title="Tên">{{$data_user->name}}</td>
                <td data-title="Email">
                    <a href="mailto:{{$data_user->email}}">{{$data_user->email}}</a>
                </td>
                <td data-title="Địa chỉ">
                    <span class="label label-sm label-info">{{$data_user->address}}</span>
                </td>
                <td class="center" data-title="Tên đăng nhập">{{$data_user->username}}</td>
                <td data-title="Số điện thoại">{{$data_user->phone}} </td>
                <td data-title="Chức vụ">{{$data_user->role->name}}</td>
                <td>
                    <div class="btn-group">
                        <a class="font-blue"
                           onclick="editUser({{$data_user->id}})">
                            <i class="icon-docs font-blue"></i>
                            Sửa</a>
                        @if($data_user->role_id!=IS_SYSADMIN)
                            <a class="font-red-flamingo button_delete_user"
                               data-id="{{$data_user->id}}"
                               data-toggle="modal"
                               href="#delete">
                                <i class="icon-trash font-red-flamingo"></i>
                                Xóa</a>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div class="pagination">{!! $data_user_all->links() !!}</div>