@extends('layouts.layout')
@section('page_head_include')
    <link rel="stylesheet" href="/css/reponsive/reponsive_reponsive.css">
@endsection
@section('page_content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin')}}">Trang chủ</a>
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Quản lí nhân viên</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-user"></i> Danh sách nhân viên
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body">
                                                <div class="table-toolbar">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="btn-group">
                                                                <button data-toggle="modal"
                                                                        data-target="#modal_add_user"
                                                                        class="btn sbold green"> Thêm mới
                                                                    <i class="fas fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="btn-group pull-right">
                                                                <button class="btn green  btn-outline dropdown-toggle"
                                                                        data-toggle="dropdown">Công cụ
                                                                    <i class="fas fa-angle-down"></i>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <li>
                                                                        <a href="{{route('employee.export.excel')}}">
                                                                            <i class="fas fa-file-excel-o"></i>
                                                                            Export to Excel </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="padding: 0px;">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <input id="search_user"
                                                                   style="border-radius: 4px !important;"
                                                                   type="text" class="form-control"
                                                                   placeholder="Tìm theo tên..">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="data_user" class="">
                                                    @include('backend.Employee.data_table_user')
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Portlet PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END PAGE CONTENT BODY -->
    <!-- END CONTENT BODY -->

    <!-- MODAL EDIT -->
    <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel" style="color: white;">Cập nhật</h5>
                </div>
                <div class="modal-body">
                    <div id="data_modal"></div>
                </div>
                <div class="modal-footer">
                    <button id="button_update_user" type="button" class="btn btn-outline blue">Cập nhật</button>
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL EDIT -->
    {{-- MODAL ADD USER --}}
    <div class="modal fade" id="modal_add_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header  bg-blue">
                    <h5 class="modal-title bg-font-dark bold" id="exampleModalLabel">ĐĂNG KÝ</h5>
                </div>
                <div class="modal-body">
                    <form id="form_add_user">
                        <p> Thông tin cá nhân: </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label visible-ie8 visible-ie9">Họ và tên</label>
                                    <div class="input-icon">
                                        <i class="fas fa-font"></i>
                                        <input class="form-control placeholder-no-fix" type="text"
                                               placeholder="Họ và tên"
                                               name="myname"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                                    <div class="input-icon">
                                        <i class="fas fa-envelope"></i>
                                        <input class="form-control placeholder-no-fix" type="text" placeholder="Email"
                                               name="email"/></div>
                                </div>
                            </div>
                            {{--tab2--}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label visible-ie8 visible-ie9">Địa chỉ</label>
                                    <div class="input-icon">
                                        <i class="fas fa-check"></i>
                                        <input class="form-control placeholder-no-fix" type="text" placeholder="Địa chỉ"
                                               name="address"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select name="role_id" class="select2-multiple form-control select2 multiple">
                                        <option value="{{IS_EMPLOYEE}}">Nhân viên</option>
                                        <option value="{{IS_MANAGER}}"> Quản lí</option>
                                        <option value="{{IS_SYSADMIN}}"><i class="fas fa-check"></i> Sysadmin</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <p>Thông tin tài khoản: </p>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Tên đăng nhập</label>
                            <div class="input-icon">
                                <i class="fas fa-user"></i>
                                <input class="form-control placeholder-no-fix" type="text" autocomplete="off"
                                       placeholder="Tên đăng nhập"
                                       name="username"/></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Số điện thoại</label>
                            <div class="input-icon">
                                <i class="fas fa-phone"></i>
                                <input class="form-control placeholder-no-fix" type="number" autocomplete="off"
                                       placeholder="Số điện thoại"
                                       name="phone"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Mật khẩu</label>
                            <div class="input-icon">
                                <i class="fas fa-lock"></i>
                                <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                                       id="register_password"
                                       placeholder="Mật khẩu" name="password"/></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Nhập lại mật khẩu</label>
                            <div class="controls">
                                <div class="input-icon">
                                    <i class="fas fa-check"></i>
                                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                                           placeholder="Nhập lại mật khẩu" name="password_confirmation"/></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="button_add_user" class="btn btn-outline blue">Thêm</button>
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>
    {{-- END MODAL ADD USER --}}
    {{--MODAL DELETED USER--}}
    <div class="modal fade" id="delete">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header bg-red">
                    <div id="load_delete"></div>
                    <input id="calendar_id" type="hidden" value="">
                    <h4 class="modal-title" id="exampleModalLabel" style="color: white">Xác nhận xóa tài khoản
                    </h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" value="">
                    <h4>Bạn có chắc chắn xóa tài khoản này?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="deleteUser()" class="btn btn-danger">Đồng ý</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>
    {{--END MODAL DELETED USER--}}
@stop
@section('begin_page_level_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
            $("#button_add_user").on("click", function () {
                var data = $("#form_add_user").serializeArray();

                $.ajax({
                    type: 'POST',
                    url: '{{route('employee.ajax.add')}}',
                    data: data,
                    success: function (result) {
                        if (result == 'success') {
                            $("#modal_add_user").modal('hide');
                            swal('Thành công', 'Đã tạo tài khoản thành công', 'success');
                            setTimeout(function () {
                                swal.close();
                                window.location.reload();
                            }, 1500);
                        } else {
                            $("#modal_add_user").modal('hide');
                            swal('Lỗi', result, 'error');
                            setTimeout(function () {
                                swal.close();
                                $("#modal_add_user").modal('show');
                            }, 1500);
                        }

                    }
                });
            });
            $(".button_delete_user").on('click', function () {
                var id = $(this).data('id');
                $(".modal-body #id").val(id);
            });
            $("#button_update_user").on('click', function () {
                var data_all = $("#data_edit_user").serialize();
                $.ajax({
                    type: 'POST',
                    url: '{{route('employee.ajax.update')}}',
                    data: data_all,
                    success: function (result) {
                        if (result == 'success') {
                            $('#modal_edit').modal('hide');
                            swal('Thành công', 'Cập nhật thành công', 'success');
                            setTimeout(function () {
                                swal.close();
                                window.location.reload();
                            }, 1500);
                        } else {
                            $('#modal_edit').modal('hide');
                            swal('Lỗi', result, 'error');
                        }
                    }
                });
            });
            $('#search_user').on('keypress', function (e) {
                if (e.which == 13) {
                    var search = $("#search_user").val();
                    $.ajax({
                        type: 'POST',
                        url: '{{route('employee.ajax.search')}}',
                        data: {
                            search: search
                        },
                        success: function (result) {
                            $('#data_user').html(result);
                        }
                    });
                }
            });
        });

        //paging ajax
        $(document).on('click', '.pagination a', function (e) {
            e.preventDefault();
            var page = $(this).attr('href').split('page=')['1'];
            fetch_data(page);
        })

        function fetch_data(page) {
            $.ajax({
                type: 'POST',
                url: '{{route('employee.ajax.search')}}?page=' + page,
                success: function (result) {
                    $('#data_user').html(result);
                }
            });
        }
    </script>
    <script>
        function editUser(id) {
            var data_edit_user = $("#data_edit_user").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('employee.ajax.edit')}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: id,
                    data: data_edit_user
                },
                success: function (result) {
                    $('#data_modal').html(result);
                    $("#modal_edit").modal('show');
                }
            });
        }

        function deleteUser() {
            var id = $("#id").val();
            $.ajax({
                type: 'POST',
                url: '{{route('employee.ajax.delete')}}',
                data: {
                    id: id
                },
                success: function (result) {
                    if (result == 'success') {
                        $("#delete").modal('hide');
                        swal('Thành công', 'Bạn đã xóa thành công', 'success');
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    }
                }
            });
        }
    </script>
@stop
