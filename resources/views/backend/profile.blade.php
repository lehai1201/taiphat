@extends('layouts.layout')
@section('page_head_include')
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
    <style>
        label.error {
            color: indianred !important;
        }
    </style>
@stop
@section('page_content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route('admin')}}">Trang chủ</a>
                            <i class="fas fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Tài khoản</a>
                            <i class="fas fa-circle"></i>
                        </li>
                        <li>
                            <span>{{$data_user_profile->name}}</span>
                        </li>
                    </ul>
                    <div class="page-content-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet box red-sunglo">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-user"></i> Thông tin tài khoản
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="profile-sidebar">
                                                    <div class="portlet light profile-sidebar-portlet ">
                                                        <div class="profile-userpic">
                                                            <img src="{{$data_user_profile->getAvatarAtributte()}}"
                                                                 class="img-responsive" alt=""></div>
                                                        <div class="profile-usertitle">
                                                            <div class="profile-usertitle-name">{{$data_user_profile->name}}</div>
                                                            <div class="profile-usertitle-job">{{$data_user_profile->role->name}}</div>
                                                        </div>
                                                        {{--<div class="profile-userbuttons">--}}
                                                            {{--<button type="button" class="btn btn-circle green btn-sm">Follow</button>--}}
                                                            {{--<button type="button" class="btn btn-circle red btn-sm">Message</button>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                                <div class="profile-content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="data_profile">
                                                                @include('backend.includes.data_user_profile')
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page_footer_script')
    <script src="/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/jquery-validate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script>
        $.validator.setDefaults({
            highlight: function (element) {
                $(element).closest('.form-group ').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            success: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            }
        });

        function editProfile() {
            $("#form_edit_profile").validate({
                rules: {
                    name: 'required',
                    phone: 'required',
                    address: 'required'
                },
                messages: {
                    name: 'Tên không được để trống',
                    phone: 'Số điện thoại không được để trống',
                    address: 'Địa chỉ không được để trống'
                },
                submitHandler: function () {
                    var data_all = $("#form_edit_profile").serialize();
                    $.ajax({
                        type: 'POST',
                        url: '{{route('user.edit.profile',$data_user_login->id)}}',
                        dataType: 'html',
                        data: data_all,
                        success: function (result) {
                            swal("Thành công!", "Cập nhật thành công !", "success");
                            $("#data_profile").html(result);
                        }
                    });
                }
            });
        }

        function editPassword() {
            $("#form_edit_password").validate({
                rules: {
                    old_password: 'required',
                    password: {
                        minlength: 4,
                        required: true,
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: "#newpassword",
                    }
                },
                messages: {
                    old_password: 'Bắt buộc phải nhập',
                    password: {
                        minlength: 'Mật khẩu mới phải có ít nhất 4 kí tự',
                        required: 'Bắt buộc phải nhập',
                        confirmed: 'Mật khẩu mới không trùng'
                    },
                    password_confirmation: {
                        required: 'Bắt buộc phải nhập',
                        equalTo: 'Mật khẩu mới không trùng'
                    }
                },
                submitHandler: function () {
                    var data_all = $("#form_edit_password").serialize();
                    $.ajax({
                        type: 'POST',
                        url: '{{route('user.edit.password',$data_user_profile->id)}}',
                        data: data_all,
                        success: function (result) {
                            if (result == 'error') {
                                swal("Lỗi!", "Mật khẩu cũ không đúng ", "error");
                            } else {
                                swal("Thành công!", "Cập nhật thành công ", "success");
                                $("#data_profile").html(result);
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop
