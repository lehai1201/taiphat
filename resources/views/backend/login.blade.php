<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8"/>
    <title>Tài Phát - Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Preview page of Metronic Admin Theme #3 for " name="description"/>
    <meta content="" name="author"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/pages/css/login-4.min.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico"/>
    <style>
        label.error {
            color: indianred !important;
        }

        h3 {
            color: #0E1112 !important;
        }

        h4 {
            color: #0E1112 !important;
        }

        p {
            color: #0E1112 !important;
        }
    </style>
</head>
<body class=" login">
<div class="content" style="background-color: #a8b8cd;
        box-shadow: 5px 5px #888888;margin-top: 100px;">
    <div style="text-align: center"><a href="{{route('login')}}"><img src="images/tp.png" alt=""/></a></div>
    <form class="login-form" id="login-form" method="post" action="{{route('post.login')}}">
        {{ csrf_field() }}
        <h3 class="form-title">Đăng nhập</h3>
        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="input-icon"><i class="fas fa-user"></i><input class="form-control placeholder-no-fix" type="text"
                                                                     autocomplete="off" placeholder="Username"
                                                                     name="username" autofocus/></div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon"><i class="fas fa-lock"></i><input class="form-control placeholder-no-fix"
                                                                     type="password" autocomplete="off"
                                                                     placeholder="Password"
                                                                     name="password"/></div>
        </div>
        <div class="form-actions">
            <label class="rememberme mt-checkbox mt-checkbox-outline font-dark"><input type="checkbox" name="remember"
                                                                                       value="1"/>Ghi nhớ<span></span>
            </label>
            <button type="submit" class="btn green pull-right" id="button_user_login">Đăng nhập</button>
        </div>
    </form>
    <form class="forget-form" method="post">
        <h3>Forget Password ?</h3>
        <p> Enter your e-mail address below to reset your password. </p>
        <div class="form-group">
            <div class="input-icon"><i class="fas fa-envelope"></i><input class="form-control placeholder-no-fix"
                                                                         type="text" autocomplete="off"
                                                                         placeholder="Email" name="email"/></div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn red btn-outline">Back</button>
            <button type="submit" class="btn green pull-right"> Submit</button>
        </div>
    </form>
    <form id="register-form" class="register-form">
        {{ csrf_field() }}
        <h3>Đăng ký</h3>
        <p> Thông tin của bạn: </p>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Full Name</label>
            <div class="input-icon">
                <i class="fas fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Full Name" name="fullname"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <div class="input-icon">
                <i class="fas fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email"/></div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Address</label>
            <div class="input-icon">
                <i class="fas fa-check"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Address" name="address"/>
            </div>
        </div>
        <p>Thông tin tài khoản: </p>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="input-icon">
                <i class="fas fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="username"
                       name="username"/></div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fas fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password"
                       placeholder="Password" name="password"/></div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
            <div class="controls">
                <div class="input-icon">
                    <i class="fas fa-check"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                           placeholder="Re-type Your Password" name="password_confirmation"/></div>
            </div>
        </div>
        <div class="form-group">
            <label class="mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="agree"/> Tôi đồng ý với
                <a href="javascript:;">chính sách </a> &
                <a href="javascript:;">Điều khoản </a>
                <span></span>
            </label>
            <div id="register_tnc_error"></div>
        </div>
        <div class="form-actions">
            <button id="register-back-btn" type="button" class="btn red btn-outline"> Quay lai</button>
            <button id="register-submit-btn" class="btn green pull-right">Đăng ký</button>
        </div>
    </form>
</div>
<div class="copyright"> <?php echo date('Y'); ?> &copy; CuZa Dev Team</div>
<!--[if lt IE 9]>
<script src="/assets/global/plugins/respond.min.js"></script>
<script src="/assets/global/plugins/excanvas.min.js"></script>
<script src="/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="/assets/pages/scripts/login-4.min.js" type="text/javascript"></script>
{{--Jquery Validate--}}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="/jquery-validate/dist/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $.validator.setDefaults({
        highlight: function (element) {
            $(element).closest('.form-group ').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        }
    });
    $("#register-form").validate({
        rules: {
            fullname: "required",
            email: {
                required: true,
                email: true,
            },
            username: "required",
            address: "required",
            password: {
                required: true,
                minlength: 4,
            },
            password_confirmation: {
                required: true,
                equalTo: "#register_password",
            },
            agree: {
                required: true,
            }
        },
        messages: {
            fullname: "Tên bắt buộc phải nhập",
            email: {
                required: "Email không được để trống",
                email: "Phải nhập định dang email",
            },
            username: "Tên đăng nhập không được để trống",
            address: "Địa chỉ bắt buộc phải nhập",
            password: {
                required: "Mật khẩu bắt buộc phải nhập",
                minlength: "Mật khẩu phải có ít nhất 4 kí tự",
            },
            password_confirm: {
                required: "Bắt buộc phải nhập",
                equalTo: "Mật khẩu không trùng",
            },
            agree: {
                required: "Bạn phải chấp nhập với điều khoản",
            }
        },
        submitHandler: function (result) {
            $("#register-submit-btn").prop("disabled", true);
            var data_all = $("#register-form").serialize();
            $.ajax({
                url: "{{route('user.register')}}",
                type: "POST",
                data: data_all,
                success: function (result) {
                    if (result == 'success') {
                        swal("Thành công!", "Tự động chuyển hướng đăng nhập!", "success");
                    } else {
                        $("#register-submit-btn").prop("disabled", false);
                        swal("Lỗi!", result, "error");
                    }
                }
            });
        }
    });
    $("#login-form").validate({
        rules: {
            username: "required",
            password: "required",
        },
        messages: {
            username: "Tên đăng nhập không được để trống",
            password: "Mật khẩu không được để trống",
        }
    });
    $("#userRegister").on('click', function () {
        $(".login-form").hide();
        $(".register-form").toggle();
    });
    $("#forget-password").on('click', function () {
        $(".login-form").hide();
        $(".forget-form").toggle();
    });
    $("#user-register").on("click", function () {
        $(".login-form").hide();
        $(".register-form").toggle();

    });
    $("#back-btn").on("click", function () {
        $(".forget-form").hide();
        $(".login-form").toggle();
    });
    $("#register-back-btn").on("click", function () {
        $(".register-form").hide();
        $(".login-form").toggle();
    });

</script>
</body>
</html>
