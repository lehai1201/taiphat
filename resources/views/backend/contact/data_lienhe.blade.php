<div class="row">
    @if($data_contact->isEmpty())
        <div class="col-md-12"><h3 class="font-red">Danh sách trống</h3></div>
    @else
        @foreach($data_contact as $contact)
            <div class="col-md-4 ">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-user"></i> {{$contact->name}}
                            @if($contact->is_new==IS_NEW)
                                <img style="width: 75px" src="/images/news.png" alt="">
                            @endif
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="expand"> </a>
                            <a onclick="sendEmail('{{$contact->id}}')"
                               class="font-white"> Trả lời</a>
                        </div>
                    </div>
                    <div class="portlet-body portlet-collapsed">
                        <div class="row static-info">
                            <div class="col-md-5 name"> Tên khách hàng:</div>
                            <div class="col-md-7 value"> {{$contact->name}}</div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name"> Email:</div>
                            <div class="col-md-7 value"
                                 style="word-break: break-word;"> {{$contact->email}}</div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name"> Ghi chú:</div>
                            <div class="col-md-7 value"
                                 style="word-break: break-word;"> {{$contact->note}}</div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name"> Số điện thoại:</div>
                            <div class="col-md-7 value"> {{$contact->phone}}</div>
                        </div>
                        @if(empty($contact->is_reply))
                            <div class="row static-info">
                                <div class="col-md-5 name">Phản hồi:</div>
                                <div class="col-md-7 value">Chưa</div>
                            </div>
                        @else
                            <div class="row static-info">
                                <div class="col-md-5 name">Phản hồi gần nhất:</div>
                                <div class="col-md-7 value">{{$contact->is_reply}}</div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
<div style="text-align: center">{{$data_contact->links()}}</div>