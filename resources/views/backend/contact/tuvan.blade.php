@extends('layouts.layout')
@section('page_head_include')
@stop
@section('page_content')
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="{{route('admin')}}">Trang chủ</a>
                                <i class="fas fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Tư vấn</a>
                            </li>
                        </ul>
                    </div>
                </div>
                {{--NỘI DUNG--}}
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
                        <div class="dashboard-stat blue">
                            <div class="visual">
                                <i class="fas fa-briefcase fa-icon-medium"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 3 liên hệ</div>
                                <div class="desc">3 liên hệ mới</div>
                            </div>
                            {{--<a class="more" href="javascript:;"> Chi tiết--}}
                            {{--<i class="m-icon-swapright m-icon-white"></i>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat red">
                            <div class="visual">
                                <i class="fas fa-shopping-cart"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 4 tư vấn</div>
                                <div class="desc"> 1 đơn tư vấn mới</div>
                            </div>
                            {{--<a class="more" href="javascript:;"> Chi tiết--}}
                            {{--<i class="m-icon-swapright m-icon-white"></i>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat green">
                            <div class="visual">
                                <i class="fas fa-group fa-icon-medium"></i>
                            </div>
                            <div class="details">
                                <div class="number">2 đại lí</div>
                                <div class="desc"> 1 đơn đăng ký đại lí mới</div>
                            </div>
                            {{--<a class="more" href="javascript:;"> Chi tiết--}}
                            {{--<i class="m-icon-swapright m-icon-white"></i>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                </div>
                {{--Tư vấn--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-share font-blue"></i>
                                    <span class="caption-subject font-blue bold uppercase">Tư vấn</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    @if($data_tuvan->isEmpty())
                                        <div style="margin-left: 10px;color: red">DANH SÁCH TRỐNG</div>
                                    @else
                                        @foreach($data_tuvan as $tuvan)
                                            <div class="col-md-4">
                                                <!-- BEGIN Portlet PORTLET-->
                                                <div class="portlet box green">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="icon-user"></i>{{$tuvan->name}}
                                                            @if($tuvan->is_new==IS_NEW)
                                                                <img style="width: 75px" src="/images/news.png" alt="">
                                                            @endif
                                                        </div>
                                                        <div class="tools">
                                                            <a href="javascript:;" class="expand"> </a>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body portlet-collapsed">
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name"> Tên khách hàng:</div>
                                                            <div class="col-md-7 value"> {{$tuvan->name}}</div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name"> Email:</div>
                                                            <div class="col-md-7 value"
                                                                 style="word-break: break-word;"> {{$tuvan->email}}</div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">Địa chỉ:</div>
                                                            <div class="col-md-7 value"
                                                                 style="word-break: break-word;"> {{$tuvan->address}}</div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name"> Số điện thoại:</div>
                                                            <div class="col-md-7 value"> {{$tuvan->phone}}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END Portlet PORTLET-->
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--MODAL REP EMAIL--}}
    <div class="modal fade" id="sendEmail" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <h5 class="modal-title font-white" id="exampleModalLabel">Phản hồi</h5>
                </div>
                <div class="modal-body">
                    <form id="form_send_email">
                        <div class="form-group">
                            <div class="row">
                                <label for="email_send_to" class="col-md-1">Tới:</label>
                                <div class="col-md-11">
                                    <input class="form-control" id="email_send_to" name="email_send_to" disabled
                                           value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Tiêu đề: </label>
                            <input class="form-control" id="email_title">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nội dung:</label>
                            <textarea class="form-control" id="editor1" name="email_content"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="button_send_email" class="btn btn-outline blue">Gửi</button>
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page_footer_script')
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.replace('editor1'); </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        })

        function sendEmail(contact_id) {
            $.ajax({
                type: 'POST',
                url: '{{route('reply.contact')}}',
                data: {
                    contact_id: contact_id
                },
                success: function (result) {
                    var data = $.parseJSON(result);
                    $('#email_send_to').val(data.email);
                    $('#sendEmail').modal('show');
                }
            });
        }

        $('#button_send_email').on('click', function (e) {
            e.preventDefault();
            var content = CKEDITOR.instances.editor1.getData();
            var email_to = $('#email_send_to').val();
            var email_title = $('#email_title').val();
            $('#button_send_email').attr('disabled', true);
            $.ajax({
                type: 'POST',
                url: '{{route('send.email')}}',
                data: {
                    content: content,
                    email_title: email_title,
                    email_to: email_to
                },
                success: function (result) {
                    $('#button_send_email').attr('disabled', false);
                    if (result == 'success') {
                        $('#sendEmail').modal('hide');
                        swal('Đã gửi thành công', '', 'success');
                    } else {
                        $('#sendEmail').modal('hide');
                        swal(result, '', 'error');
                        setTimeout(function () {
                            swal.close();
                            $('#sendEmail').modal('show');
                        }, 1500);
                    }
                }
            });
        });
    </script>
@stop
