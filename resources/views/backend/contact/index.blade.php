@extends('layouts.layout')
@section('page_head_include')
@stop
@section('page_content')
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="{{route('admin')}}">Trang chủ</a>
                                <i class="fas fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Liên hệ</a>
                            </li>
                        </ul>
                    </div>
                </div>
                {{--NỘI DUNG--}}
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
                        <div class="dashboard-stat blue">
                            <div class="visual">
                                <i class="fas fa-briefcase fa-icon-medium"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 1 liên hệ</div>
                                <div class="desc">3 liên hệ mới</div>
                            </div>
                            {{--<a class="more" href="javascript:;"> Chi tiết--}}
                                {{--<i class="m-icon-swapright m-icon-white"></i>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat red">
                            <div class="visual">
                                <i class="fas fa-shopping-cart"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 2 tư vấn</div>
                                <div class="desc"> 1 đơn tư vấn mới</div>
                            </div>
                            {{--<a class="more" href="javascript:;"> Chi tiết--}}
                                {{--<i class="m-icon-swapright m-icon-white"></i>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat green">
                            <div class="visual">
                                <i class="fas fa-group fa-icon-medium"></i>
                            </div>
                            <div class="details">
                                <div class="number">3 đại lí</div>
                                <div class="desc"> 1 đơn đăng ký đại lí mới</div>
                            </div>
                            {{--<a class="more" href="javascript:;"> Chi tiết--}}
                                {{--<i class="m-icon-swapright m-icon-white"></i>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-share font-blue"></i>
                                    <span class="caption-subject font-blue bold uppercase">Liên hệ</span>
                                </div>
                                <div class="actions">
                                    <input type="text" id="search_lienhe" placeholder="Tìm kiếm..." style="padding: 2px">
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="data_search_lienhe">
                                    @include('backend.contact.data_lienhe')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--MODAL REP EMAIL--}}
    <div class="modal fade" id="sendEmail" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <h5 class="modal-title font-white" id="exampleModalLabel">Phản hồi</h5>
                </div>
                <div class="modal-body">
                    <form id="form_send_email">
                        <div class="form-group">
                            <div class="row">
                                <label for="email_send_to" class="col-md-1">Tới:</label>
                                <div class="col-md-11">
                                    <input class="form-control" id="email_send_to" name="email_send_to" disabled
                                           value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Tiêu đề: </label>
                            <input class="form-control" id="email_title">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nội dung:</label>
                            <textarea class="form-control" id="editor1" name="email_content"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="button_send_email" class="btn btn-outline blue">Gửi</button>
                    <button type="button" class="btn btn-outline red" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page_footer_script')
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.replace('editor1'); </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        })

        function sendEmail(contact_id) {
            $.ajax({
                type: 'POST',
                url: '{{route('reply.contact')}}',
                data: {
                    contact_id: contact_id
                },
                success: function (result) {
                    var data = $.parseJSON(result);
                    $('#email_send_to').val(data.email);
                    $('#sendEmail').modal('show');
                }
            });
        }

        $('#button_send_email').on('click', function (e) {
            e.preventDefault();
            var content = CKEDITOR.instances.editor1.getData();
            var email_to = $('#email_send_to').val();
            var email_title = $('#email_title').val();
            $('#button_send_email').attr('disabled', true);
            $.ajax({
                type: 'POST',
                url: '{{route('send.email')}}',
                data: {
                    content: content,
                    email_title: email_title,
                    email_to: email_to
                },
                success: function (result) {
                    $('#button_send_email').attr('disabled', false);
                    if (result == 'success') {
                        $('#sendEmail').modal('hide');
                        swal('Đã gửi thành công', '', 'success');
                    } else {
                        $('#sendEmail').modal('hide');
                        swal(result, '', 'error');
                        setTimeout(function () {
                            swal.close();
                            $('#sendEmail').modal('show');
                        }, 1500);
                    }
                }
            });
        });

        $("#search_lienhe").on('keypress',function (e) {
            var search=$("#search_lienhe").val();
            if(e.which==13){
               $.ajax({
                   type: 'POST',
                   url: '{{route('search.lienhe')}}',
                   data: {
                       search: search
                   },
                   success: function(data){
                       $("#data_search_lienhe").html(data);
                   }
               });
            }
        })
        $(document).on('click','.pagination a',function(e){
            e.preventDefault();
            var page=$(this).attr('href').split('page=')['1'];
            fetch_data(page);
        })
        function fetch_data(page){
            $.ajax({
                type: 'POST',
                url: '{{route('search.lienhe')}}?page='+page,
                success: function(data){
                    $("#data_search_lienhe").html(data);
                }
            });
        }
    </script>
@stop
