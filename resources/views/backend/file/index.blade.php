@extends('layouts.layout')
@section('page_content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="fas fa-user"></i>HỒ SƠ NĂNG LỰC</div>
                        </div>
                        <div class="portlet-body">
                            @if(Session::has('errors'))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="padding: 10px;margin-bottom: 10px" class="alert-danger">{{Session::get('errors')}}</div>
                                    </div>
                                </div>
                            @endif
                                @if(Session::has('messeger'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div style="padding: 10px;margin-bottom: 10px" class="alert-success">{{Session::get('messeger')}}</div>
                                        </div>
                                    </div>
                                @endif
                            <form id="form_add_post" class="form-horizontal" method="POST"  enctype="multipart/form-data"
                                  action="{{route('backend.file.post.add')}}"
                            >
                                <input type="hidden" name="id" value="{{ data_get($data, 'id') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12"><br>
                                        <div class="col-md-12">
                                            <input autofocus class="form-control placeholder-no-fix" type="text" value="{{ !empty($data->title) ? $data->title : ''}}"
                                                   placeholder="Tiêu đề" name="title"/>
                                        </div>
                                        <div class="col-md-12"><br>
                                            <label for="button_img">Ảnh đại diện</label>
                                            <div>
                                                <input id="button_img" name="image1" onchange="loadImg(event,this, 1)"  accept="image/png, image/jpeg"
                                                       type="file" value="Chọn ảnh">
                                            </div>
                                        </div>
                                        <div class="col-md-12"><img style="width: 200px;margin-top: 10px"
                                                                    id="img_show1" src="{{ !empty($data->image1) ? "/images/file/".$data->image1 : '' }}" alt=""></div>
                                        <div class="col-md-12"><br>
                                            <label for="button_img">File PDF</label>
                                            <div>
                                                <input id="button_img" name="image2"  accept="application/pdf"
                                                       type="file" value="Chọn file">
                                            </div>
                                        </div>
                                        <div class="col-md-12">{{ !empty($data->image2) ? asset("/images/file/".$data->image2) : '' }}</div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center">
                                        <button class="btn btn-outline red" type="submit"><i class="fas fa-plus"></i> Tạo
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page_footer_script')
    <script>
        function loadImg(event, e, stt) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];
            if ($(e).val() == '') {
                $("#img_show"+stt).attr('src', '');
            } else {
                if ($.inArray($(e).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    $("#img_show").attr('src', '');
                    alert('Phải chọn ảnh kiểu ảnh và có đuôi: JPEG,JPG,PNG');
                    $(e).val('');
                } else {
                    var path = URL.createObjectURL(event.target.files[0]);
                    $("#img_show"+stt).attr('src', path);
                }
            }
        }
    </script>
@stop
