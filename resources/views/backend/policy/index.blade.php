@extends('layouts.layout')
@section('page_content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="fas fa-user"></i>CHÍNH SÁCH QUYỀN RIÊNG TƯ</div>
                        </div>
                        <div class="portlet-body">
                            @if(Session::has('errors'))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="padding: 10px;margin-bottom: 10px" class="alert-danger">{{Session::get('errors')}}</div>
                                    </div>
                                </div>
                            @endif
                            <form id="form_add_post" class="form-horizontal" method="POST"
                                  action="{{route('backend.policy.post.add')}}"
                            >
                                <input type="hidden" name="id" value="{{ data_get($data, 'id') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12"><br>
                                        <label>Nội dung</label>
                                        <textarea style="resize: none" id="description_post"
                                                  name="content" rows="20"
                                                  placeholder="content">{{ !empty($data->content) ? $data->content : '' }}</textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center">
                                        <button class="btn btn-outline red" type="submit"><i class="fas fa-plus"></i> Tạo
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page_footer_script')
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var editor = CKEDITOR.replace('description_post', {
                uiColor: '#CCEAEE',
                height: 400,
                filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
                filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
                filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
                filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
                filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
                filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
            });
            CKFinder.setupCKEditor(editor);
        });
    </script>
@endsection
