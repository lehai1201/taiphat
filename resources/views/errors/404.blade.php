@php
    $metaHeader = $metaFooter = collect();
@endphp

@extends('taiphat.layout')
@section('title')
    <title>Công ty Tài Phát | Công ty sơn Tài Phát</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/error.css">
@endsection
@section('page_content')
    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-4">
                <img src="/images/404.png" alt="404-not-found">
            </div>
            <div class="col-md-6">
                <div><p class="title_error">Truy cập của bạn có thể bị lỗi hoặc không tìm thấy nội dung</p></div>
                <button class="button_home" onclick="location.href='{{route('home')}}'"><i class="fas fa-home"></i> Quay lại trang chủ</button>
            </div>
        </div>
        <br>
    </div>
@endsection
