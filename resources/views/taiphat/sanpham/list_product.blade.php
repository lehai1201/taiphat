<div class="row block-4" style="min-height: 200px">
    @if(!$products->isEmpty())
        @foreach($products as $product)
            <div class="col-sm-4 col-md-4">
                <div class="item hover_product">
                    <div class="item_inner">
                        <a href="{{route('detail.product',['product'=>$product->convert(),'id'=>$product->id])}}" class="item_img"><img
                                    height="300"
                                    src="/images/product/{{$product->images}}"
                                    alt=""></a>
                        <a href="{{route('detail.product',['product'=>$product->convert(),'id'=>$product->id])}}"
                           class="item_title">{{$product->title}}</a>
                        <ul class="list_info">
                            <li class="description_product">{{$product->codeprice->description}}</li>
                            <li class="price_product">Giá: {{number_format($product->codeprice->price)}}
                                VNĐ/{{$product->codeprice->unit}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
        @else
        <h4 style="text-align: center;color: #8b131b">DANH SÁCH TRỐNG</h4>
    @endif
</div>
<div style="text-align: center">
    {{$products->links()}}
</div>
