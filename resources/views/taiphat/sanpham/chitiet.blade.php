@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Sơn NESAN, Sơn ANTINI, Sơn MALLEND">
    <meta name="keywords"
          content="taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND, sơn, antini, nesan, mallend">
    <title>{{$product->title}} | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_content')
    <div class="wrap-page-title-emty"></div>
    <div id="maincontent" class="product-detail">
        <div class="container">
            <div class="page-title"><h1 class="title">
                    <span>{{$product->title}} </br>{{$product->codeprice->description}}</span></h1></div>
            <div class="row top-detail">
                <div class="col-sm-6 col-left">
                    <div class="img-full"><a class="fancybox_img" href="/images/product/{{$product->images}}"><img
                                    src="/images/product/{{$product->images}}"></a></div>
                </div>
                <div class="col-sm-6 col-right">
                    <div class="item">
                        <div class="description">
                            <p style="text-align: justify;">{!! $product->description !!}</p>
                        </div>
                        <p>
							<span id="view-more" class="btn btn-primary btnshow">
								<span class="inner"><span class="more">Xem thêm</span><span class="less">Thu gọn</span></span>
							</span>
                        </p>
                    </div>
                    <div class="item">
                        <h4 class="color-main">{{$product->codeprice->description}}</h4>
                        <p>Giá: <span
                                    style="color: #f40c1c;font-weight: bold;">{{number_format($product->codeprice->price)}}</span>
                            VNĐ/{{$product->codeprice->unit}}</p>
                        <p>Khối lượng: {{$product->codeprice->kl}} Kg</p>
                    </div>
                    <div style=" border-top: 1px dashed #4A4A4A;">
                        <h5 style="color: #ac0303;margin-top: 10px">MÃ QR</h5>
                        <input id="text" type="hidden"
                               value="{{$qrlink}}"/>
                        <p class="text-center">
                        <div id="qrcode"></div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="TOA005">
        <div class="container">
            <div class="section-header"><h2 class="section-title">Hệ thống đề nghị</h2></div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <p style="text-align:justify;">{!! $product->content !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="TOA006">
        <div class="container">
            <div class="section-header"><h2 class="section-title">Sản phẩm khác</h2></div>
            <div class="slide4-arr top-arrows owl-carousel block-5">
                @if(!$product_same->isEmpty())
                    @foreach($product_same as $same)
                        <div class="item">
                            <div class="item_inner">
                                <a href="{{route('detail.product',['product'=>$same->convert(),'id'=>$same->id])}}" class="item_img"><img
                                            src="/images/product/{{$same->images}}"></a>
                                <a href="{{route('detail.product',['product'=>$same->convert(),'id'=>$same->id])}}" class="item_title">{{$same->title}}</a>
                                <p style="font-size: 14px;text-align: center">{{$same->codeprice->description}}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    </div>
    @include('taiphat.includes.list_footer')
@stop
@section('page_js')
    <script type="text/javascript" src="QR/qrcode.js"></script>
    <script>
        $(document).ready(function () {
            $(".fancybox_img").fancybox();
        });
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            width: 200,
            height: 200
        });

        function makeCode() {
            var elText = document.getElementById("text");

            if (!elText.value) {
                alert("Input a text");
                elText.focus();
                return;
            }

            qrcode.makeCode(elText.value);
        }

        makeCode();

        $("#text").on("blur", function () {
            makeCode();
        }).on("keydown", function (e) {
            if (e.keyCode == 13) {
                makeCode();
            }
        });
    </script>
@stop
