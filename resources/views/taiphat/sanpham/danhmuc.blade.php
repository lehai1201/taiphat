@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Danh mục sơn thương hiệu: Sơn NESAN, Sơn ANTINI, Sơn MALLEND">
    <meta name="keywords"
          content="Danh mục, Hãng sơn, Màu sơn,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Danh mục sơn | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_css')
    <link rel="stylesheet" type="text/css" href="/css/san-pham.css">
@stop
@section('page_content')
    <div class="wrap-page-title-emty"></div>
    <div id="maincontent">
        <div class="container">
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group has-feedback" style="margin-bottom:2.5em;">
                    <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span>
                        <input id="search_category_input" type="text" class="form-control"
                               placeholder="Tìm kiếm sản phẩm">
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <ul class="contentmenu-2">
                    <li><a href="{{route('sanpham')}}">Xem tất cả</a></li>
                    @if(!$categories->isEmpty())
                        @foreach($categories as $category)
                            <li class="category {{$category_id==$category->id ? 'active' : ''}}" ><a href="{{route('category.product',['title'=>$category->convert(),'id'=>$category->id])}}">{{$category->name}}</a></li>
                        @endforeach
                    @endif

                </ul>
            </div>
            <div class="page-list-products">
{{--                <div class="row block-4" style="clear: both">--}}
{{--                    <a class="fancybox_nesan"--}}
{{--                       href="https://docs.google.com/document/d/e/2PACX-1vSzQQk_itTSl8K_yn4ITYZjZ7YybE6vCh12FEJAUbzzDEgZtiIi4g8uGOuFTNOxoDZACYneurlrVukK/pub?embedded=true"><i--}}
{{--                                class="fa fa-tag"></i> Bảng giá sơn NESAN</a>--}}
{{--                </div>--}}
                <br>
                <div id="list_product_category">
                    @include('taiphat.sanpham.list_product')
                </div>
            </div>
        </div>
    </div>

@stop
@section('page_js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
            $(".fancybox_nesan").fancybox({
                'type': 'iframe'
            });
            $(".fancybox_atini").fancybox({
                'type': 'iframe'
            });
            $(".fancybox_mallend").fancybox({
                'type': 'iframe'
            });
        });
        $("#search_category_input").on('keypress', function (e) {
            var search_input = $("#search_category_input").val();
            if (e.which == 13) {
               if(search_input){
                   $.ajax({
                       type: 'POST',
                       url: '{{route('sp.search')}}',
                       data: {
                           search: search_input
                       },
                       success: function (data) {
                           $("#list_product_category").html(data);
                       }
                   });
               }else{
                   toastr.error('Phải nhập từ khóa','','error');
               }
            }
        })
    </script>
@stop
