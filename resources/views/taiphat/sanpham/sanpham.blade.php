@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Danh mục các dòng sơn đến từ thương hiệu NESAN, ANTINI, MALLEND. Các sản phẩm sơn Nội thất, Ngoại thất cao cấp">
    <meta name="keywords"
          content="Sản phẩm, danh sách , san pham,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Sản phẩm | Sơn Nội thất, Ngoại thất | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_css')
    <link rel="stylesheet" type="text/css" href="/css/san-pham.css">
@stop
@section('page_content')
    <div class="wrap-page-title-emty"></div>
    <div id="maincontent">
        <div class="container">
            <div style="text-align: center"><h1 class="title"><span>Sản phẩm</span></h1></div>
            <div class="col-md-6 col-md-offset-3">

                <div class="form-group has-feedback" style="margin-bottom:2.5em;">
                    <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span>
                        <input id="search_input" type="text" class="form-control" placeholder="Tìm kiếm sản phẩm">
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <ul class="contentmenu-2">
                    <li class="active"><a href="{{route('sanpham')}}">Xem tất cả</a></li>
                    @if(!$categories->isEmpty())
                        @foreach($categories as $category)
                            <li class="category"><a
                                        href="{{route('category.product',['title'=>$category->convert(),'id'=>$category->id])}}">{{$category->name}}</a>
                            </li>
                        @endforeach
                    @endif

                </ul>
            </div>
            <div class="page-list-products">
{{--                <div class="row block-4" style="clear: both">--}}
{{--                    <a class="fancybox_nesan" href="{{ route('price.list') }}"><i--}}
{{--                                class="fa fa-tag"></i> Bảng giá sơn NESAN</a>--}}
{{--                </div>--}}
                <br>
                <div id="list_sp">
                    @include('taiphat.sanpham.list_product')
                </div>
            </div>
        </div>
    </div>
@stop
@section('page_js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });

        $("#search_input").on('keypress', function (e) {
            var search_input = $("#search_input").val();
            if (e.which == 13) {
                if (search_input) {
                    $.ajax({
                        type: 'POST',
                        url: '{{route('sp.search')}}',
                        data: {
                            search: search_input
                        },
                        success: function (data) {
                            $("#list_sp").html(data);
                        }
                    });
                }else{
                    toastr.error('Phải nhập từ khóa','','error');
                }
            }
        })
    </script>
@stop
