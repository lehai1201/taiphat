@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content=" Liên hệ ngay với chúng tôi để được giải đáp các thắc mắc một cách nhanh nhất, Hay muốn nhận những lời tư vấn từ các chuyên gia sơn, màu sắc hàng đầu Việt Nam ">
    <meta name="keywords"
          content="Liên hệ Tài phát,taiphat,congtytaiphat, Tư vấn ,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Liên hệ với chúng tôi | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/lien-he.css">
@stop
@section('page_content')
    <div class="TOA045">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">&nbsp;</div>
                <div class="col-md-3 col-md-offset-1 col-sm-4"><h1 class="title "><span>Liên hệ</span></h1></div>
            </div>
            <div class="row">
                @if(!$norland->isEmpty())
                    <h3>Chi nhánh khu vực Miền Bắc</h3>
                    @foreach($norland as $agency)
                        <div class="col-md-4 col-sm-6 storelist">
                            <p class="title-18">{{$agency->name}}</p>
                            <ul class="store-item list-icon thirdary" data-latlng="20.912820, 105.848301"
                                data-map="hanoi"
                                data-title="{{$agency->name}}">
                                <li><i class="icon-map"></i>{{$agency->address}}</li>
                                <li><i class="icon-phone"></i>Số điện thoại: {{$agency->phone}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="row">
                @if(!$central->isEmpty())
                    <h3>Chi nhánh khu vực Miền Trung</h3>
                    @foreach($central as $agency)
                        <div class="col-md-4 col-sm-6 storelist">
                            <p class="title-18">{{$agency->name}}</p>
                            <ul class="store-item list-icon thirdary" data-latlng="20.912820, 105.848301"
                                data-map="hanoi"
                                data-title="{{$agency->name}}">
                                <li><i class="icon-map"></i>{{$agency->address}}</li>
                                <li><i class="icon-phone"></i>Số điện thoại: {{$agency->phone}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="row">
                @if(!$south->isEmpty())
                    <h3>Chi nhánh khu vực Miền Nam</h3>
                    @foreach($south as $agency)
                        <div class="col-md-4 col-sm-6 storelist">
                            <p class="title-18">{{$agency->name}}</p>
                            <ul class="store-item list-icon thirdary" data-latlng="20.912820, 105.848301"
                                data-map="hanoi"
                                data-title="{{$agency->name}}">
                                <li><i class="icon-map"></i>{{$agency->address}}</li>
                                <li><i class="icon-phone"></i>Số điện thoại: {{$agency->phone}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <!--Test-->
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header bg-primary ">LIÊN HỆ VỚI CHÚNG TÔI</div>
                    <div class="card-body">
                        <form method="POST" id="form_lienhe">
                            @csrf
                            <div class="form-group">
                                <label for="name">Họ và tên</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="phone">Số điện thoại</label>
                                <input type="text" class="form-control" name="phone">
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="text" class="form-control" name="email">
                            </div>
                            <div class="form-group">
                                <label for="content">Nội dung</label>
                                <textarea class="form-control" name="content" cols="20" rows="10"></textarea>
                            </div>
                            <button class="button_submit" onclick="sendInfo()" id="button_send_info">GỬI
                                THÔNG TIN <i
                                        class="fa fa-chevron-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div class="contact">
                    <h5>Nhà máy sản xuất: </h5>
                    <p>Khu công nghiệp Đồng Văn II, xã Bạch Thượng, huyện Duy Tiên, Tỉnh Hà Nam</p>
                    <hr>
                    <h5>Trụ sở chính:</h5>
                    <p>Thọ Tiến - Triệu Sơn - Thanh hóa</p>
                    <hr>
                    <h5>Văn phòng Miền Bắc: </h5>
                    <p>Tòa nhà Thanh Hoa - Đại Lộ Lê Lợi - Tp.Thanh Hóa</p>
                    <hr>
                    <h5>Văn phòng Miền Trung: </h5>
                    <p>108 Trịnh Đình Thảo - Quận Cẩm Lệ - TP. Đà Nẵng</p>
                    <hr><h5>Văn phòng Miền Nam: </h5>
                    <p>92H Đặng Văn Trơn - P.Hiệp Hoà - TP.Biên Hòa - Đồng Nai</p>
                    <hr>
                    <p><span><i class="icon-phone"></i> Số điện thoại: </span>0961226489 hoặc 0975436910</p>
                    <p><span><i class="fa fa-envelope"></i> Email: </span>pkdquoctetaiphat@gmail.com</p>
                </div>
            </div>
        </div>
    </div>

    <hr>
    @include('taiphat.includes.list_footer')
@endsection
@section('page_js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });

        function sendInfo() {
            $("#button_send_info").attr('disabled', true);
            var data_all = $("#form_lienhe").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('post.lienhe')}}',
                data: data_all,
                success: function (data) {
                    if (data == 'success') {
                        swal('Gửi tin thành công', '', 'success');
                        setTimeout(function () {
                            swal.close();
                            window.location.reload();
                        }, 1500);
                    } else {
                        $("#button_send_info").attr('disabled', false);
                        swal(data, '', 'error')

                    }
                }
            });
        }
    </script>
@endsection
