<!DOCTYPE html>
<html lang="vi">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content=”vi” />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html" charset="UTF-8">
    <meta name="_token" content="{{ csrf_token() }}"/>
    @yield('page_title')
    <link rel="shortcut icon" href="/images/logo2.png" type=”image/x-icon”>
    <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/css/styled7f4.css?rf=1' type='text/css' media='all'/>
    <link rel='stylesheet' href='/css/home.css' type='text/css' media='all'/>
    <link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css" type="text/css" media="screen"/>
    <link rel="stylesheet" type="text/css"  href="/css/select2.min.css" />
    <link rel="stylesheet" href="/toastr/toastr.min.css">
    <link rel="stylesheet" type="text/css"
          href="/css/font.css"/>
    <link rel="stylesheet" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/css/layout.css">
@yield('page_css')
    @include('taiphat.meta_header')
    <script src="/js/jquery.min.js" type="text/javascript"></script>

</head>

<body class="home page-about page-trang-chu-top vi">
<!-- Messenger Plugin chat Code -->
<div id="fb-root"></div>

<!-- Your Plugin chat code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>
<script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "105508511118032");
    chatbox.setAttribute("attribution", "biz_inbox");

    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v12.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div id="wrapper">
    <header id="header" style="background-color:#ac0303;line-height:70px;">
        <div class="container">
            <a href="{{route('home')}}" id="logo">
                <img class="hidden-xs Logo" src="/images/logo2.png">
                <img class="visible-xs" src="/images/logo2.png">
            </a>
            <div class="menu-mainmenu">
                <ul class="mainmenu">
                    <li class="parent mec" style="background-color:#ac0303">
                        <a href="{{route('home')}}" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Home</span>
                        </a>
                    </li>
                    <li class="" style="background-color:#ac0303">
                        <a href="{{route('gioithieu')}}" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Giới Thiệu</span>
                        </a>
                    </li>
                    <li class="parent" style="background-color:#ac0303">
                        <a href="{{route('lienhe')}}" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Liên Hệ</span>
                        </a>
                    </li>
                    <li class="parent" style="background-color:#ac0303">
                        <a href="{{route('tintuc')}}" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Tin Tức</span>
                        </a>
                    </li>
                    <li class="parent" style="background-color:#ac0303">
                        <a href="#" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Đại Lý</span>
                        </a>
                        <ul>
                            <li class="active"><a href="#">Chính Sách Dành Cho Đại Lý</a></li>
                            <li class="active"><a href="{{route('daili')}}">Đăng Ký Làm Đại Lý</a></li>
                        </ul>
                    </li>

                    <li class="parent" style="background-color:#ac0303">
                        <a href="#" class="btnT " style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Màu Sắc</span>
                        </a>
                        <ul>
                            <li class="active"><a href="{{route('bangmau')}}">Bảng Màu</a></li>
                            <li class="active"><a href="{{route('congtrinh')}}">Công Trình Tiêu Biểu</a></li>
                            <li class="active"><a href="{{route('camhung')}}">Cảm Hứng Màu Sắc</a></li>
                        </ul>
                    </li>
                    <li class="parent" style="background-color:#ac0303">
                        <a href="{{route('congthuc')}}" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Công Thức Tính Sơn</span>
                        </a>
                    </li>
                    <li class="parent" style="background-color:#ac0303">
                        <a href="#" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Tư Vấn </span>
                        </a>
                        <ul>
                            <li class="active"><a href="{{route('chuyengia')}}">Tư Vấn Từ Chuyên Gia</a></li>
                            <li class="active"><a href="{{route('dangky')}}">Đăng Ký Nhận Tư Vấn</a></li>
                        </ul>
                    </li>
                    <li class="parent" style="background-color:#ac0303">
                        <a href="#" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Sản Phẩm </span>
                        </a>
                        <ul>
                            @if(!$categories->isEmpty())
                                @foreach($categories as $category)
                                    <li class="active"><a
                                            href="{{route('category.product',['title'=>$category->convert(),'id'=>$category->id])}}">{{$category->name}}</a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                    <li class="parent" style="background-color:#ac0303">
                        <a href="{{route('video.list')}}" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Video</span>
                        </a>
                    </li>
                    <li class="parent" style="background-color:#ac0303">
                        <a href="{{route('price.list')}}" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Bảng Giá</span>
                        </a>
                    </li>

                    <li class="parent" style="background-color:#ac0303">
                        <a href="{{route('guarantee.index')}}" class="btnT" style="color:#fff;">
                            <span class="btnL"></span>
                            <span class="btnR"></span>
                            <span class="text">Bảo Hành</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="header-right"><span class="menu-btn"><span></span></span></div>
        </div>
    </header>
</div>
@yield('page_content')
<footer id="footer" style="background-color:#ac0303; margin-top: 100px">
    <div class="container">
        <div class="row">
            <div class="col-md-4" style="font-weight: bold;">
                <p style="font-size:16px;margin-top:-5px;color: #fff;"><a href="{{route('guarantee.policy')}}">Chính Sách Bảo Hành</a>
                </p>
                <p style="font-size:16px;margin-top:-5px;color: #fff"><a href="{{route('lienhe')}}">Liên Hệ</a></p>
                <p style="font-size:16px;margin-top:-5px;color: #fff"><a href="{{route('daili')}}">Đại Lý</a></p>
            </div>
            <div class="col-md-4" style="font-weight: bold;">
                <p style="font-size:16px;margin-top:-5px;color: #fff"><a href="{{route('sanpham')}}">Sản Phẩm</a></p>
                <p style="font-size:16px;margin-top:-5px;color: #fff"><a href="{{route('tintuc')}}">Tin Tức</a></p>
                <p style="font-size:16px;margin-top:-5px;color: #fff"><a href="{{route('sukien')}}">Sự Kiện</a></p>
            </div>
            <div class="col-md-4" style="font-weight: bold;">
                <p style="font-size:16px;margin-top:-5px;color: #fff"><a href="{{route('congtrinh')}}">Công Trình Tiêu
                        Biểu</a></p>
                <p style="font-size:16px;margin-top:-5px;color: #fff"><a href="{{route('policy.index')}}">Chính Sách Quyền Riêng Tư</a></p>
                <p style="font-size:16px;margin-top:-5px;color: #fff"><a href="{{route('recruit.index')}}">Tuyển Dụng</a></p>
            </div>

        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-6" style="font-weight: bold;margin: 0;">
                <p style="font-size:16px;margin-top:-5px;color: #fff;">Công ty Cổ phần Tập Đoàn Nesan - Tổng giám đốc: Trịnh Văn Tài
                </p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Địa chỉ: Thọ Tiến, Triệu Sơn, Thanh Hoá - Mã số thuế: 2802548940 </p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Nhà máy sản xuất: Lô P - Khu công nghiệp Đồng Văn - Duy Tiên - Hà Nam</p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Hotline: 0237.383.5555</p>
            </div>
            <div class="col-md-6" style="font-weight: bold;margin: 0;">
                <p style="font-size:16px;margin-top:-5px;color: #fff;">Văn phòng giao dịch 1: Toà nhà Thanh Hoa, Đại lộ Lê Lợi, Thành phố Thanh Hoá
                </p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Văn phòng giao dịch 2: Thị trấn Cày, Thạch Hà, Hà Tĩnh</p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Văn phòng giao dịch 3: Ngã ba Sân bay, Thành Phố Đồng Hới, Quảng Bình</p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Văn phòng giao dịch 4: Trịnh Đình Thảo, Quận Cẩm Lệ, Thành Phố Đà Nẵng</p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Văn phòng giao dịch 5: Bình Long, Bình Sơn, Quảng Ngãi</p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Văn phòng giao dịch 6: Biên Hoà, Đồng Nai</p>
                <p style="font-size:16px;margin-top:-5px;color: #fff">Văn phòng giao dịch 7: Phường Tân An, Thành phố Buôn Ma Thuột, Đăk Lăk</p>

            </div>
        </div>
    </div>
    <hr>
    <div class="copyright2">Copyright © 2019 favorite Dev Team CuZa</div>
</footer>

@yield('button_footer')
<!-- Xung Js giữa nút call và chat facebook | Chỉ chọn 1 trong 2 -->
<button class="scroll_top" onclick="scrollBody()"><i class="fa fa-arrow-up"></i></button>
<div class='fixed call-button left'>
    <a href="tel:0961226489"><span class='pulse-button'><i class="fa fa-phone" aria-hidden="true"></i></span></a>
    <div class="text-phone pull-left"><a href="tel:0961226489"><p>Gọi Ngay: 0961226489</p></a></div>
</div>
@include('taiphat.meta_footer')
<script type='text/javascript' src='/js/owl.carousel.min.js'></script>
<script type='text/javascript' src='/js/bootstrap.min.js'></script>
<script type='text/javascript' src='/js/imagesloaded.pkgd.min.js'></script>
<script type="text/javascript" src="/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/js/select2.min.js"></script>
<script type='text/javascript' src='/js/script.js'></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="/fancybox/lib/jquery.mousewheel.pack.js"></script>
<script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
<script type="text/javascript" src="/toastr/toastr.min.js"></script>
@yield('page_js')
<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            var topPos = $(this).scrollTop();
            if(topPos>300){
                $(".scroll_top").css('opacity',1);
            }else{
                $(".scroll_top").css('opacity',0);
            }
        });
    })
    function scrollBody(){
        $('html, body').animate({
            scrollTop: 0
        }, 800);
    }
</script>
</body>
</html>
