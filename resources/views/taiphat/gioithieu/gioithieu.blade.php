@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content=" Công ty cổ phần Tập Đoàn Nesan đặt phương châm  'Khách hàng là niềm tin' làm kim chỉ nam hoạt động. Chúng tôi luôn cung cấp những sản phẩm sơn tốt nhất, uy tín nhất cho ngôi nhà của bạn ">
    <meta name="keywords"
          content="Giới thiệu,taiphat,congtytaiphat, Tập Đoàn Nesan,ANTINI,NESAN,MALLEND">
    <title>Giới thiệu về công ty | Tập Đoàn Nesan | Taiphat</title>
@endsection
@section('page_content')
    <div class="wrap-page-title bg-lazy" data-src="../images/congtrinh/ctd2.jpg">
        <div class="container">
            <div class="display-table">
                <div class="table-cell"><h1 class="title">VỀ CHÚNG TÔI</h1></div>
            </div>
        </div>
    </div>
    <section class="TOA018 row-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 fs-18">
                    <p class="fw-4">
                        Thấu hiểu và luôn muốn truyền cảm hứng cho tất cả cộng đồng một môi trường thân thiện, sang
                        trọng và thoải mái, Nesan Việt Nam đã ra đời với phương châm “ Nơi khách hàng đặt niềm tin”.
                        Trong quá trình hình thành và phát triển Nesan Việt Nam đã có những bước phát triển không ngừng,
                        từng bước khẳng định vị thế của mình trong lĩnh vực sơn. Nesan Việt Nam được sản xuất bởi Công ty cổ phần Tập Đoàn Nesan. MST: 2802548940. Nhà máy sản xuất: Lô P – Khu công nghiệp Đồng
                        Văn II – Duy Tiên – Hà Nam.
                    </p>
                    <p>
                        Ngay từ những ngày đầu thành lập Nesan Việt Nam đã có những định hướng chiến lược, mục tiêu rõ
                        ràng. Quyết tâm đưa Nesan Việt Nam thành một thương hiệu lớn mạnh trong tương lai. Chúng tôi cam
                        kết mang lại những lợi ích tối đa cho khách hàng bằng việc cung cấp các sản phẩm sơn được sản
                        xuất trên dây chuyền hiện đại theo công nghệ nhập khẩu từ Châu Âu. Song song đó, cùng với việc
                        phát triển kinh doanh cụ thể thì sự phát triển văn hóa chuẩn mực với giá trị cốt lõi: Ân cần –
                        Thân thiện – Chuyên nghiệp – Uy tín - Trách nhiệm cũng được đặt lên hàng đầu. Mỗi nhân viên
                        trong công ty luôn làm việc với phương châm luôn luôn lắng nghe, thấu hiểu mọi phản hồi từ phía
                        khách hàng để xây dựng nền móng vững chắc cho các hoạt động của công ty, đảm bảo lợi ích hài hoà
                        giữa Nesan Việt Nam với đối tác và khách hàng.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="TOA042 mb-bg-secondary bg-lazy" data-src="../images/bg-13.jpg">
        <div class="container">
            <div class="block">
                <h2 class="section-title">TẦM NHÌN &amp; SỨ MỆNH</h2>
                <p>
                    Một trong những nhân tố quyết định đến sự thành công của Tập Đoàn Nesan là luôn nghiên cứu và ứng dụng các
                    công nghệ tiên tiến nhất đạt tiêu chuẩn quốc tế để tạo nên các sản phẩm sơn có tính năng vượt trội.
                    Bên cạnh đó, tiết kiệm năng lượng, tài nguyên thiên nhiên và bảo vệ môi trường cũng là mục tiêu mà
                    công ty chúng tôi nhắm đến. Những mục tiêu này giúp Tập Đoàn Nesan đáp ứng các nhu cầu ngày càng cao
                    của khách hàng và hướng đến mục tiêu phát triển bền vững.
                </p>
            </div>
        </div>
    </section>
    <section class="TOA044 bg-1">
        <div class="row block ">
            <div class="col-md-6 block_img bg-lazy" data-src="../images/img-22.jpg">&nbsp;</div>
            <div class="col-md-6 block_text">
                <div class="display-table">
                    <div class="table-cell">
                        <h2 class="section-title">cam kết</h2>
                        <p class="fw-4">
                            Công ty cổ phần Tập Đoàn Nesan chúng tôi luôn mong muốn tạo ra những sản phẩm và dịch
                            vụ tốt nhất; đồng thời cam kết thực hiện việc tiết kiệm năng lượng, tài nguyên thiên nhiên
                            và bảo vệ môi trường.
                        </p>
                        <ul class="list-check primary">
                            <li>Luôn tuân thủ các yêu cầu liên quan đến Quản lý chất lượng, An toàn sức khoẻ nghề
                                nghiệp, Quản lý môi trường và năng lượng.
                            </li>
                            <li>Áp dụng theo tiêu chuẩn ISO 9001, OHSAS 18001, ISO 14001 và ISO 50001.</li>
                            <li>Cải tiến để đạt được những tiến bộ không ngừng trong chất lượng, đáp ứng mọi nhu cầu của
                                khách hàng.
                            </li>
                            <li>Kiểm soát các yếu tố nguy hiểm có thể gây ra tai nạn, gây thiệt hại tài sản và tác động
                                xấu tới môi trường.
                            </li>
                            <li>Nâng cao nhận thức cá nhân trong tổ chức, đảm bảo toàn bộ nhân viên đều thấu hiểu chính
                                sách này.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="TOA045  row-section">
        <div class="container">
            <div class="section-header"><h2 class="section-title">NHỮNG CHẶNG ĐƯỜNG<br/>PHÁT TRIỂN</h2></div>
            <p class="fs-26 text-center">Nesan Việt Nam và lịch sử phát triển<br/>(1999 -2020).</p>
            <hr>
        </div>
        @include('taiphat.includes.list_footer')
    </section>
@endsection
