@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Video nổi bật của Công ty Tài Phát">
    <meta name="keywords"
          content="Tin tức, tin tuc ,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Video | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_content')
    <div class="wrap-page-title-emty"></div>
    <div id="maincontent">
        <div class="TOA026 container" ng-app="TOA" ng-controller="ColorTableTab as tabs"
             json-url="/json/oil-colors.json">
            <div class="page-title"><h1 class="title"><span>VIDEO</span></h1></div>
            <div class="TOA021 content-item" style="margin-bottom: 20px">
                <div class="block_color row">
                    <br>
                    @if($videos->isEmpty())
                        <div class="col-md-12" style="text-align: center">DANH SÁCH TRỐNG</div>
                    @else
                        <div class="row block-1">
                            @if(!$videos->isEmpty())
                                @foreach($videos as $video)
                                    <div class="col-sm-4 col-md-4">
                                        <div class="list-video">
                                            <iframe width="300" frameborder="0" src="{{ $video->genUrl() }}"></iframe>
                                            <div class="content">
                                                <p style="color: #0E1112"
                                                   class="item_date">{{$video->getTimeCreated()}}</p>
                                                <p class="item_title">{{$video->title}}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div style="text-align: center">
                            {!! $videos->links() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
