@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Hồ sơ năng lực">
    <meta name="keywords"
          content="bài viết,post, chủ để ,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Tuyển dụng | Nesan Việt Nam</title>
@endsection
@section('page_content')
    <div class="wrap-page-title-emty no-mobile"></div>
    <div id="maincontent">
        <div class="container">
            <h1 class="title color-main text-center" style="margin-bottom: 45px"><span>{!! data_get($data, 'title') !!}</span></h1>
            @if(!empty($data->image2))
                <iframe src="{{ asset('images/file/'.$data->image2) }}" class="iframe-pdf" width="100%" style="min-height: 1000px">
                    This browser does not support PDFs. Please download the PDF to view it: Download PDF
                </iframe>

                <a class="btn  pdf-button" href="{{ route('file.view', ['file' => 'images/file/'.$data->image2]) }}"
                   target="_blank">Đọc ngay</a>
            @endif
        </div>
    </div>
@stop
