@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Các công trình kiến trúc đẹp, hiện đại">
    <meta name="keywords"
          content="Công trình đẹp, Công trình Tài phát , Màu sơn,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Công trình tiêu biểu | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/cong-trinh.css">
@stop
@section('page_content')
    <div class="TOA046 bg-1">
        <div class="container">
            <h1 class="title">Công trình tiêu biểu</h1>
            <div class="row row-mg">
                @if($construct->isNotEmpty())
                    @foreach($construct as $data)
                        <div class="col-md-12" style="margin-bottom: 20px">
                            <a class="img_construct" rel="galery_construct"
                               href="/images/congtrinh/{{$data->images}}"><img style="width: 100%"
                                                                                       src="/images/congtrinh/{{$data->images}}"
                                                                                       alt=""></a>
                            <div class="text-center">{{$data->title}}</div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    @include('taiphat.includes.list_footer')
@stop
@section('page_js')
    <script>
        $(document).ready(function () {
            $('.img_construct').fancybox({
                prevEffect: 'none',
                nextEffect: 'none'
            });
        })
    </script>
@endsection
