@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Bảng màu của các dòng sơn thương hiệu: Sơn NESAN, Sơn ANTINI, Sơn MALLEND">
    <meta name="keywords"
          content="Màu sắc, Bảng màu , Màu sơn,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Bảng màu | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_content')
    <div class="wrap-page-title-emty"></div>
    <div id="maincontent">
        <div class="TOA026 container" ng-app="TOA" ng-controller="ColorTableTab as tabs"
             json-url="/json/oil-colors.json">
            <div class="page-title"><h1 class="title"><span>Bảng màu</span></h1></div>
            <div class="TOA021 content-item">
                <div class="block_color row">
                    @if($colors->isEmpty())
                        <div class="col-md-12" style="text-align: center">DANH SÁCH TRỐNG</div>
                    @else
                        @foreach($colors as $color)
                            <div class="col-xs-6 col-md-4 item-hover">
                                <div class="item"
                                     style="background-color: {{$color->color_code}} !important;">{{$color->name}}
                                    <div class="overflow"></div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div>{{$colors->links()}}</div>
            </div>
        </div>
    </div>
    </div>
    <div class="TOA020">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    Ngôi nhà không chỉ để ở, đó còn là tổ ấm, là nơi thể hiện phong cách riêng của từng gia chủ. Chính
                    vì vậy, việc phối màu sơn nhà sao cho đẹp mắt, hài hoà và phù hợp với xu hướng là điều hết sức quan
                    trọng. Nếu bạn đang phân vân không biết nên sơn nhà màu gì, hãy gửi hình ảnh cho chúng tôi &amp; bạn
                    sẽ nhận được bản phối màu 2D, 3D HOÀN TOÀN MIỄN PHÍ!
                </div>
            </div>
        </div>
    </div>
    @include('taiphat.includes.list_footer')
@stop