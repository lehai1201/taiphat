@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Là nơi chia sẻ các cảm hứng về màu sắc, màu sắc pù hợp với tuổi của bạn. Được lấy từ các chuyên gia hàng đầu của Tài Phát">
    <meta name="keywords"
          content="Cảm hứng màu sắc,Màu sắc, Bảng màu , Màu sơn,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Cảm hứng màu sắc | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/camhung.css">
@stop
@section('page_content')
    <div class="TOA046 bg-1">
        <div class="container">
            <h1 class="title">Cảm hứng về màu sắc</h1>
            <div style="text-align: center;">
                <img src="/images/camhung/ms1.jpg" alt="">
                <p class="description">Màu sắc là yếu tố quan trọng trong việc trang trí nhà. Lựa chọn màu sắc phù hợp
                    sẽ giúp không gian sống vui vẻ, truyền cảm hứng cho bạn và khách khứa tới chơi nhà. Hãy cùng xem xét
                    một vài màu sắc có thể “đánh thức” hạnh phúc trong ngôi nhà của bạn</p>
                <div class="color">
                    <h4>Màu vàng</h4>
                    <p>Gợi nhớ đến mặt trời, màu vàng là một màu sắc rất vui vẻ. Nó sôi nổi, năng động và là một sự lựa
                        chọn rất tuyệt cho lối trang trí mang phong cách nhiệt đới. Bạn có thể sử dụng màu này cho hầu
                        hết các phòng của ngôi nhà với tư cách là màu chủ đạo hoặc màu nhấn trang trí. Nếu sợ màu vàng
                        quá chói, bạn có thể phối nó với màu trắng hoặc xám để làm giảm bớt sắc độ rực rỡ mà vẫn thời
                        trang.</p>
                    <div class="row">
                        <div class="col-md-6"><img src="/images/camhung/yellow.jpg" alt=""></div>
                        <div class="col-md-6"><img src="/images/camhung/yellow1.jpg" alt=""></div>
                    </div>
                </div>
                <div class="color">
                    <h4>Tím oải hương</h4>
                    <p>Màu sắc này rất phù hợp cho phòng ngủ. Không chỉ tinh tế, tím oải hương còn rất hoàn hảo để tạo
                        ra một phong cách yên tĩnh và thư giãn. Kết hợp màu tím oải hương với màu trắng sẽ giúp không
                        gian nghỉ ngơi của bạn thêm nhẹ nhàng, phong cách. Ngoài ra, bạn cũng có thể kết hợp màu tím với
                        những màu sắc rực rỡ để mang đến sự sinh động cho căn phòng.</p>
                    <div class="row">
                        <div class="col-md-12"><img src="/images/camhung/tim.jpg" alt=""></div>
                    </div>
                </div>
                <div class="color1">
                    <h4>Màu hồng phấn</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <p>Màu hồng phấn là một màu sắc rất vui vẻ, sức sống. Tuy nhiên, bạn không nên làm dụng nó
                                quá nhiều bởi hồng phấn dễ tạo ra khiến không gian sống thừa sự nữ tính. Nếu muốn sử
                                dụng màu hồng, tốt nhất bạn nên chọn tông hồng pastel. Bạn cũng có thể mang màu hồng đến
                                căn phòng thông qua các chi tiết trang trí như gối ôm, hoa tươi. Phối màu hồng với màu
                                trắng là một cách tuyệt vời để trang trí nhà "đẹp dịu dàng mà không chói lóa"</p>
                        </div>
                        <div class="col-md-6">
                            <img src="/images/camhung/hong.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="color2">
                    <h4>Màu xanh lam</h4>
                    <img src="/images/camhung/blue.jpg" alt="">
                    <p>Tươi mát và sôi động, màu xanh lam là một trong những màu sắc không bao giờ lỗi mốt. Nếu muốn sử
                        dụng màu xanh lam là màu chủ đạo cho toàn bộ ngôi nhà, bạn chỉ nên sử dụng gam xanh lam nhạt.
                        Còn đối với màu xanh lam đậm, bạn chỉ nên sử dụng cho một mảng tường hoặc dùng cho các món đồ
                        nội thất. Bạn cũng có thể kết hợp nhiều tông ngọc lam với các sắc độ khác nhau để tạo ra điểm
                        nhấn hút mắt cho ngôi nhà.</p>
                </div>
                <div class="color3">
                    <h4>Màu xanh lá cây nhạt</h4>
                    <div class="row">
                        <div class="col-md-6"><img src="/images/camhung/green.jpg" alt=""></div>
                        <div class="col-md-6">
                            <p>Màu xanh lá cây là một màu sắc rất mạnh mẽ và đó là lý do tại sao rất khó để phối màu này
                                trong trang trí nội thất. Tuy nhiên, tông màu xanh lá nhạt lại hoàn hảo cho phòng ngủ
                                cũng như đối với bất kỳ không gian nào khác. Bạn có thể lựa chọn màu xanh lá nhạt cho
                                các bức tường, song hành là trần nhà màu trắng. Với cách trang trí này, bạn cũng có thể
                                kết hợp thêm các màu sắc pastel khác để tạo hiệu ứng năng động hơn. </p>
                        </div>
                    </div>
                </div>
                <div class="color4">
                    <h4>Màu trắng</h4>
                    <p>Mặc dù màu trắng là một màu trung lập và cực kỳ đơn giản nhưng nó lại là câu trả lời cho phong
                        cách trang trí nội thất thanh lịch và sang trọng. Một căn phòng màu trắng thường rất tươi sáng,
                        mát mẻ và thoáng rộng. Điều này cũng khiến không gian mang màu sắc ấy trở nên tinh tế. Với sự
                        linh hoạt của mình, màu trắng có thể kết hợp với bất kỳ màu sắc nào khác, do đó bạn có thể thỏa
                        sức lựa chọn màu để “bắt cặp” nhé.</p>
                    <img src="/images/camhung/white.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
    @include('taiphat.includes.list_footer')
@stop