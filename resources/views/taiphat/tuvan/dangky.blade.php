@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Đăng ký để nhận được các tư vấn hoàn hảo nhất cho ngôi nhà của bạn. Công ty Tài Phát">
    <meta name="keywords"
          content="Đăng ký tư vấn, chuyên giá, Màu sơn,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Đăng ký nhận tư vấn | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_content')
    <div class="TOA046 bg-1">
        <div class="container">
            <form class="row mangosteen" id="form_tuvan" method="POST">
                @csrf
                <div class="col-md-10 col-md-offset-1">
                    <h1 class="section-title">ĐĂNG KÝ NHẬN TƯ VẤN</h1>
                    <img src="/images/sp.jpg" alt="">
                    <br>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <label class="row-label">
                                <span class="title">Họ & tên <p class="error-message"></p></span>
                                <input class="input input-sm" name="name" type="text" placeholder="Họ & tên">

                            </label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label class="row-label">
                                <span class="title">Email <p class="error-message"></p></span>
                                <input name="email" class="input input-sm" type="text" placeholder="Email">
                            </label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label class="row-label">
                                <span class="title">Số điện thoại <p class="error-message"></p></span>
                                <input class="input input-sm" name="phone" type="text" placeholder="Số điện thoại">
                            </label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label class="row-label">
                                <span class="title">Địa chỉ <p class="error-message"></p></span>
                                <input class="input input-sm" name="address" type="text" placeholder="Địa chỉ">
                            </label>
                        </div>
                        <div class="col-sm-12 text-center pt-10">
                            <button class="btn btn-primary" id="button_lienhe" onclick="sendLienHe()"><span
                                        class="inner">Gửi</span></button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr>
    @include('taiphat.includes.list_footer')
@stop
@section('page_js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });

        function sendLienHe() {
            $("#button_lienhe").attr('disabled', true);
            var data_all = $("#form_tuvan").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('dangky.tuvan')}}',
                data: data_all,
                success: function (data) {
                    if (data == 'success') {
                        swal('Đăng ký nhận tư vấn thành công', '', 'success');
                        setTimeout(function () {
                            swal.close();
                            window.location.reload();
                        }, 1500);
                    } else {
                        $("#button_lienhe").attr('disabled', false);
                        swal(data, '', 'error')

                    }
                }
            });
        }
    </script>
@endsection