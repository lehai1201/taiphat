@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Nơi chia sẻ kinh nghiệm về chọn lựa màu sắc, sản phẩm đến từ các chuyên gia nổi tiếng của Công ty Tài Phát">
    <meta name="keywords"
          content="Tư vấn chuyên gia, chuyên gia, Đăng ký tư vấn, Màu sơn,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
<title>Tư vấn từ chuyên gia | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/chuyen-gia.css">
@stop
@section('page_content')
    <div class="TOA046 bg-1">
        <div class="container">
            <form class="row mangosteen" id="lienhe">
                <input type="hidden" name="lang" value="vi"/>
                <input type="hidden" name="language" value="vi"/>
                <div class="col-md-10 col-md-offset-1">
                    <h1 class="section-title" style="text-align: center">Tư vấn từ chuyên gia</h1>
                </div>
            </form>
        </div>
    </div>
    <div class="#013" style="background-image: url('../images/chuyengia-bg.png');padding: 50px 20px 0px 20px">
        <div class="row block-1">
            @if(!$posts->isEmpty())
                @foreach($posts as $post)
                    <div class="col-sm-3 col-md-3">
                        <a href="{{route('chitiettuvan',$post->id)}}"><img class="img_item" src="/images/post/{{$post->images}}"></a>
                        <div class="item_content">
                            <h5>{{$post->title}}</h5>
                            <div>
                                <a href="{{route('chitiettuvan',$post->id)}}">XEM CHI TIẾT <i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
    <hr>
    @include('taiphat.includes.list_footer')
@stop
