@extends('taiphat.layout')
@section('page_title')
    <title>Tư vấn từ chuyên gia | Công ty Tài Phát</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/chuyen-gia-chitiet.css">
@stop
@section('page_content')
    <div class="TOA003 bg-3">
        <div class="container">
            <div class="page_title">{{$post->title}}</div>
            <div class="content">
                <div class="date_time">
                    <i class="fa fa-calendar"></i> <span>{{$post->getTimeCreated()}}</span>
                    <span>Tạo bởi <b>{{$post->user->name}}</b></span>
                </div>
                <div class="description">{{$post->description}}</div>
                <div class="text">
                    {!! $post->content !!}
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="TOA003">
        <div class="container">
            <div class="note_title">QUÝ KHÁCH HÀNG MUỐN NHẬN SỰ TRỢ GIÚP TƯ VẤN CỦA CÔNG TY VỀ MÀU SẮC, LỰA CHỌN SẢN
                PHẨM VUI LÒNG GỬI THÔNG TIN LIÊN HỆ, NỘI DUNG MUỐN TƯ VẤN VÀO EMAIL:
                <span>pkdquoctetaiphatkv2@gmail.com</span> HOẶC GỬI TRỰC TIẾP CÂU HỎI <a href="{{route('dangky')}}">TẠI
                    ĐÂY.</a></div>
        </div>
    </div>
    @include('taiphat.includes.list_footer')

@stop
