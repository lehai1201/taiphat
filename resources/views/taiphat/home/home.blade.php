@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Công ty cổ phần Tập Đoàn Nesan, Nhà sản xuất và cung cấp chính hãng các dòng sơn Nội thất Ngoại thất với các thương hiệu Sơn NESAN, Sơn ANTINI, Sơn MALLEND">
    <meta name="keywords" content="TaiPhat,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát">
    <title>Nesan Việt Nam | Nhà sản xuất và cung cấp sơn nội, ngoại thất số 1 Việt Nam </title>
@endsection
@section('page_content')
    <div id="wrap-slideshow">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">

                <div class="item active">
                    <img src="../images/slider/sl4.jpg" alt="Chicago">
                </div>

                <div class="item">
                    <img src="../images/slider/sl2.jpg" alt="Chicago">
                </div>

                <div class="item">
                    <img src="../images/slider/sl3.jpg" alt="New York">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <i class="fa-2x fa fa-chevron-left icon_left"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <i class="fa-2x fa fa-chevron-right icon_left"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="row" style="margin-top:1.5em;">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="section-title">Vì sao chọn chúng tôi</h2>
                    <p style="font-size:1.5525rem;">
                        Nesan Việt Nam cam kết mang lại lợi ích tối đa cho khách hàng bằng việc cung cấp các sản phẩm
                        sơn chất lượng tốt nhất, được sản xuất trên dây chuyền tiên tiến, hiện đại theo công nghệ nhập
                        khẩu từ Châu Âu, giá thành hợp lý. Song song đó đặc biệt chú ý văn hóa chuẩn mực của công ty với
                        giá trị cốt lõi: <b>“Ân cần – Thân thiện – Uy tín – Trách nhiệm”</b> được đặt lên hàng đầu.
                    </p>
                </div>
            </div>
            <div class="row" style="margin-top:3em;">
                <div class="col-md-4 text-center">
                    <div class="info">
                        <div class="icon icon-info" style="color:#00bcd4;">
                            <i class="material-icons" style="font-size:5.85rem">chat</i>
                        </div>
                        <h4 class="info-title">Tư vấn</h4>
                        <p style="font-size:14px;">Phục vụ 24/7. Luôn luôn lắng nghe ý kiến, phản hồi từ khách hàng.</p>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="info">
                        <div class="icon icon-success" style="color:#4caf50;">
                            <i class="material-icons" style="font-size:5.85rem">verified_user</i>
                        </div>
                        <h4 class="info-title">Bảo Hành</h4>
                        <p style="font-size:14px">Tất cả mặt hàng do chúng tôi cung cấp được bảo hành 3 đến 5
                            năm. </p>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="info">
                        <div class="icon icon-danger" style="color:#f44336;">
                            <i class="material-icons" style="font-size:5.85rem">local_shipping</i>
                        </div>
                        <h4 class="info-title">Vận Chuyển</h4>
                        <p style="font-size:14px;">Giao hàng tính phí từ kho đến địa điểm nhận hàng của quý khách
                            một cách nhanh nhất. </p>
                    </div>
                </div>
            </div>
            @if($data_post_check!=null)
                <div class="section-header"><h2 class="section-title"> Tin tức nổi bật </h2></div>
                <div class="page-list-news">
                    <div class="feature-post block-3">
                        <div class="item">
                            <a href="{{route('chitiet.tintuc',['post'=>$data_post_check->convert(),'id'=>$data_post_check->id])}}"
                               class="item_img"><img
                                        src="/images/post/{{$data_post_check->images}}"></a>
                            <div class="item_content">
                                <a href="{{route('chitiet.tintuc',['post'=>$data_post_check->convert(),'id'=>$data_post_check->id])}}"
                                   class="item_title">{{$data_post_check->title}}</a>
                                <div class="item_decs">{{$data_post_check->description}}</div>
                                <a href="{{route('chitiet.tintuc',['post'=>$data_post_check->convert(),'id'=>$data_post_check->id])}}"
                                   class="btn  btn-primary"><span class="inner">Xem thêm</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="TOA006">
        <div class="container">
            <div class="section-header"><h2 style="text-align: center;" class="section-title">SƠN NESAN</h2></div>
            <div class="slide4-arr top-arrows owl-carousel block-5">
                @if(!$products_nessan->isEmpty())
                    @foreach($products_nessan as $nessan)
                        <div class="item img_product">
                            <div class="item_inner">
                                <a href="{{route('detail.product',['product'=>$nessan->convert(),'id'=>$nessan->id])}}" class="item_img"><img
                                            src="images/product/{{$nessan->images}}"></a>
                                <a href="{{route('detail.product',['product'=>$nessan->convert(),'id'=>$nessan->id])}}"
                                   class="item_title">{{$nessan->title}}</a>
                                <p class="product_info">{{$nessan->codeprice->description}}</p>
                                <p class="product_info">Giá: {{number_format($nessan->codeprice->price)}}
                                    VNĐ/{{$nessan->codeprice->unit}}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    @if(!$constructs->isEmpty())
        <div class="TOA006">
            <div class="container">
                <div class="section-header"><h2 style="text-align: center;" class="section-title">CÔNG TRÌNH TIÊU BIỂU</h2></div>
                <div class="slide4-arr top-arrows owl-carousel block-5">
                    @foreach($constructs as $construct)
                        <div class="item img_product">
                            <div class="item_inner">
                                <a title="{{$construct->title}}" href="/images/congtrinh/{{$construct->images}}"
                                   class="item_img fancybox" data-fancybox="images" data-width="2400" data-height="1600"
                                   rel="galery2"><img src="/images/congtrinh/{{$construct->images}}" \></a>
                                <div class="item_content" style="background-color:#fff;"><p
                                        class="item_title">{{$construct->title}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if(!$videos->isEmpty())
        <div class="TOA006">
            <div class="container">
                <div class="section-header"><h2 style="text-align: center;" class="section-title">VIDEO</h2></div>
                <div class="slide4-arr top-arrows owl-carousel block-5 vd-yt">
                    @foreach($videos as $video)
                        <div class="item_inner" style="max-width: 90%">
                            <div class="text-center video-yt">
                                <iframe frameborder="0" src="{{ $video->genUrl() }}"></iframe>
                                <div>{{ $video->title }}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if(!empty($files))
        <div class="container-fluid">
            <div class="container">
                <div class="section-header"><h2 class="section-title">{{ $files->title }}</h2></div>
                <div class="page-list-news">
                    <div class="feature-post block-3">
                        <div class="item">
                            <a href="{{route('file.index')}}"
                               class="item_img"><img
                                    src="/images/file/{{$files->image1}}"></a>
                            <div class="item_content" style="width: 100% !important;background-color: unset !important;">
                                <a href="{{route('file.index')}}"
                                   class="btn  btn-primary"><span class="inner">Xem ngay</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div style="height: 150px;background-repeat: repeat-x;background-image: url('/images/background/background.jpg')"></div>
    <div class="container-fluid row-2 bg-lazy " style="background-color: #157ca9;margin-top: -3px">
        <div class="container" style="padding-top: 70px">
            <div class="row block-1">
                <div class="col-md-4 col-md-offset-2"><img style="height: 300px" src="/images/background/image2.png"
                                                           alt=""></div>
                <div class="col-md-4 block_text">
                    <div class="display-table ">
                        <div class="table-cell">
                            <h2 class="block_title" style="color: #ffff">CẢM HỨNG MÀU SẮC</h2>
                            <div class="block_decs" style="color: #ffff">
                                Chuyên mục cập nhật các xu hướng màu sắc mới cùng những phong cách thiết kế trên toàn
                                thế
                                giới. Với các bài viết này, hy vọng chúng tôi sẽ truyền cảm hứng cho bạn tạo ra câu
                                chuyện
                                riêng trong hành trình thiết kế tổ ấm thân yêu của mình.
                            </div>
                            <br>
                            <a class="btn btn-thirdary " href="{{route('camhung')}}"><span class="inner">Xem thêm</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 250px;background-color: #e46c86;background-repeat: repeat-x;margin-top: -1px;background-image: url('/images/background/background2.jpg')"></div>
    <div class="container-fluid row-1 bg-lazy" style="padding:2em 0 ;margin-top: -5px;background-color: #00a2a2">
        <div class="container">
            <div class="row block ">
                <div class="col-md-4 block_text">
                    <div class="display-table ">
                        <div class="table-cell">
                            <h2 class="block_title color-thirdary  " style="color: white">TƯ VẤN<br/>MÀU SƠN</h2>
                            <div class="block_decs" style="color: white">
                                Nếu bạn đang băn khoăn không biết ngôi nhà thân yêu của mình nếu khoác lên màu sơn nào
                                sẽ
                                tốt nhất, đẹp đẽ nhất thì hãy yên tâm vì có chúng tôi. Chỉ cần vài thao tác đơn giản bạn
                                sẽ
                                nhận được lời tư vấn chi tiết nhất, có tâm nhất đến từ các chuyên gia màu sắc hàng đầu
                                từ
                                chúng tôi.
                            </div>
                            <br>
                            <a class="btn btn-second " href="{{route('chuyengia')}}"><span
                                        class="inner">Xem thêm </span> </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2"><img src="/images/background/support.png" alt=""></div>
            </div>
        </div>
    </div>
    <br>
    @if(!$rates->isEmpty())
        <div class="TOA006">
            <div class="container">
                <div class="section-header"><h2 style="text-align: center;" class="section-title">Ý KIẾN KHÁCH HÀNG</h2></div>
                <div class="slide4-arr top-arrows owl-carousel block-5">
                    @foreach($rates as $rate)
                        <div class="item">
                            <div class="item_inner custom_item">
                                <div class="">
                                    <div class="card card-testimonial card-plain customer">
                                        <div class="card-avatar customer_avt">
                                            <a href="#pablo" style="text-decoration:none;">
                                                <img style="width: 100px; height: 100px" class="img"
                                                     src="/images/rate/{{ $rate->avatar }}">
                                            </a>
                                        </div>
                                        <div class="card-body "
                                             style="margin-top:15px;padding-right:5px;padding-left:5px;padding:.9375rem 1.875rem;flex:1 1 auto;">
                                            <h4 class="card-title customer_name  crop-text">{{ $rate->name }}</h4>
                                            <h6 class="card-category text-muted  crop-text">{{ $rate->address }}</h6>
                                            <p class="card-description note_customer crop-text">{{ $rate->comment }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if(!$chairs->isEmpty())
        <div class="TOA006">
            <div class="container">
                <div class="section-header"><h2 style="text-align: center;" class="section-title">GHẾ MASSAGE</h2></div>
                <div class="slide4-arr top-arrows owl-carousel block-5">
                    @foreach($chairs as $chair)
                        <div class="item img_product">
                            <div class="item_inner">
                                <a href="{{route('detail.product',['product'=>$chair->convert(),'id'=>$chair->id])}}"
                                   class="item_img"><img
                                        src="images/product/{{$chair->images}}"></a>
                                <a href="{{route('detail.product',['product'=>$chair->convert(),'id'=>$chair->id])}}"
                                   class="item_title">{{$chair->title}}</a>
                                <p class="product_info">{{$chair->codeprice->description}}</p>
                                <p class="product_info">Giá: {{number_format($chair->codeprice->price)}}
                                    VNĐ/{{$chair->codeprice->unit}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <div class="container-fluid bg_footer" style="padding-right: 0px">
        <div><img style="width: 100%" src="/images/background/background3.jpg" alt=""></div>
        <div class="bg_full" style="background-color: #f0d5ac">
            <div class="row" style="margin: 0 !important;">
                <div class="col-md-5 col-md-offset-4 text-center" style="padding:3em 0;">
                    <h3 class="tite_bg_footer">
                        Nhận thông tin mới nhất về sản phẩm</h3>
                    <div class="buttonInside">
                        <input class="input_footer" type="text" id="customer_email"
                               placeholder="Email của bạn">
                        <button id="button_submit_footer" onclick="registerEmail()" class="button_footer">Gửi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page_js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
        });

        $(".fancybox").fancybox({
            minWidth : 700,
            maxHeight: 500,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none'
        });
        $("#customer_email").on('keypress', function (e) {
            if (e.which == 13) {
                registerEmail();
            }
        });

        function registerEmail() {
            $("#button_submit_footer").attr('disabled', true);
            var email = $("#customer_email").val();
            if (email) {
                $.ajax({
                    type: 'POST',
                    url: '{{route('register')}}',
                    data: {
                        email: email
                    },
                    success: function (data) {
                        $("#button_submit_footer").attr('disabled', false);
                        if (data == 'success') {
                            $("#customer_email").val('');
                            swal('Đăng ký thành công', '', 'success');
                        } else {
                            swal(data, '', 'error');
                        }
                    }
                });
            } else {
                toastr.error('Bạn chưa nhập email');
                $("#button_submit_footer").attr('disabled', false);
            }
        }
    </script>
@stop
