@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Bảng giá sơn Nesan, bảng giá sơn nesan, bang gia son nesan, nesan.com.vn, Nesan">
    <meta name="keywords"
          content="Bảng giá sơn Nesan, bảng giá sơn nesan, bang gia son nesan, nesan.com.vn, Nesan">
    <title>Bảng giá sơn Nesan | Nesan</title>
    <meta property="og:image" content="{{ asset('images/price/bang-gia-son-nesan.jpg') }}">
    <meta property="og:url" content="{{ route('price.list') }}">
@endsection
@section('page_content')
    <div class="container pad-100 price-list">
        <h1 class="hide" style="display: none">Bảng giá sơn Nesan, bang gia son nesan, son nesan</h1>
        <div class="title hide">
            <h3>BẢNG GIÁ TIÊU CHUẨN</h3>
            <span>Bảng giá áp dụng từ ngày 01/05/2021 cho tới khi có bảng giá mới</span>
        </div>
        <div class="hide" style="overflow-x:auto;">
            <table class="table table-bordered table-price-list">
                <thead>
                <tr>
                    <th>STT</th>
                    <th class="table-price-name">TÊN SẢN PHẨM</th>
                    <th>MÃ SẢN PHẨM</th>
                    <th>ĐƠN VỊ TÍNH</th>
                    <th>ĐÓNG GÓI</th>
                    <th>ĐƠN GIÁ (VNĐ)</th>
                </tr>
                </thead>
                <tr>
                    <td class="table-price-label" colspan="6">SƠN CHỐNG NÓNG</td>
                </tr>
                <tr>
                    <td rowspan="2">1</td>
                    <td class="text-left" rowspan="2">SƠN CHỐNG NÓNG ĐẶC BIỆT CAO CẤP <br>
                        Nesan Wathershield là sản phẩm có khẳ năng cách nhiệt, chống thấm tốt. Dùng cho mái tôn, tường
                        ngoài, sàn mái, bê tông, kính, ngói, ...
                    </td>
                    <td class="table-price-label" rowspan="2">N 88</td>
                    <td>18 L</td>
                    <td>Thùng</td>
                    <td>{{ number_format(4838000) }}</td>
                </tr>
                <tr>
                    <td>5 L</td>
                    <td>Lon</td>
                    <td>{{ number_format(1388000) }}</td>
                </tr>
                <tr>
                    <td class="table-price-label" colspan="6">CHẤT CHỐNG THẤM</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td class="text-left">
                        CHỐNG THẤM SIÊU ĐÀN HỒI <br>
                        Nesan Water Proof - Chất chống thấm 2 thành phần được sử dụng để chống thấm các nơi ẩm ướt như
                        bể
                        nước ăn, nhà vệ sinh, tầng thượng, ban công, bể nước ngầm. Xử lí tường sùi muối. Chống thấm
                        ngược

                    </td>
                    <td class="table-price-label">N 89</td>
                    <td>21kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(2761000) }}</td>
                </tr>
                <tr>
                    <td class="table-price-label" colspan="6">SƠN GIẢ ĐÁ</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td class="text-left">
                        SƠN GIẢ ĐÁ <br>
                        Vẩy trung: GĐ: 01,02,03,04,05,07,08,09,10,11,12,13, 14,
                        15,16,17,18,19,20,21,22,23,24,25,26,28,30,31,32,33.
                        Vẩy mịn: GĐ: 06,27,29,34,35,36,37,38,39,40,41,42, 43,44,45, 46,47,48.
                    </td>
                    <td class="table-price-label">N 90</td>
                    <td>4kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(688000) }}</td>
                </tr>
                <tr>
                    <td class="table-price-label" colspan="6">CÁC SẢN PHẨM ĐẶC BIỆT KHÁC</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td class="text-left">
                        LÓT PU ĐA NĂNG CAO CẤP <br>
                        Liên kết tất cả bề mặt, chống thấm, kháng kiềm, kháng muối
                    </td>
                    <td class="table-price-label">N 91</td>
                    <td>4 L</td>
                    <td>Lon</td>
                    <td>{{ number_format(1456000) }}</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td class="text-left">
                        NESAN LATEX <br>
                        Lót liên kết đa bề mặt, tăng kết dính, keo dán gạch
                    </td>
                    <td class="table-price-label">N 92</td>
                    <td>10kg</td>
                    <td>Can</td>
                    <td>{{ number_format(1248000) }}</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td class="text-left">
                        LÓT PU ĐA NĂNG CAO CẤP <br>
                        Liên kết tất cả bề mặt, chống thấm, kháng kiềm, kháng muối
                    </td>
                    <td class="table-price-label">N 93</td>
                    <td>25kg</td>
                    <td>BAO</td>
                    <td>{{ number_format(436000) }}</td>
                </tr>
            </table>
        </div>
        <br>
        <div class="title hide">
            <h3>BẢNG BÁO GIÁ SƠN NESAN</h3>
            <span>Bảng giá áp dụng từ ngày 01/05/2021 cho tới khi có bảng giá mới</span>
        </div>
        <div class="hide" style="overflow-x:auto;">
            <table class="table table-bordered table-price-list">
                <thead>
                <tr>
                    <th>STT</th>
                    <th class="table-price-name">TÊN SẢN PHẨM</th>
                    <th>MÃ SẢN PHẨM</th>
                    <th>ĐƠN VỊ TÍNH</th>
                    <th>ĐÓNG GÓI</th>
                    <th>ĐƠN GIÁ (VNĐ)</th>
                </tr>
                </thead>
                <tr>
                    <td class="table-price-label" colspan="6">CÁC SẢN PHẨM SƠN TRONG NHÀ</td>
                </tr>
                <tr>
                    <td rowspan="2">1</td>
                    <td class="text-left" rowspan="2">
                        SƠN NỘI THẤT <br>
                        Màu tiêu chuẩn, dễ thi công
                    </td>
                    <td class="table-price-label" rowspan="2">N21.2</td>
                    <td>23kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(686000) }}</td>
                </tr>
                <tr>
                    <td>5,32kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(255000) }}</td>
                </tr>

                <tr>
                    <td rowspan="2">2</td>
                    <td class="text-left" rowspan="2">
                        SƠN MỊN NỘI THẤT SIÊU TRẮNG <br>
                        Màng sơn mịn, độ phủ cao, siêu trắng
                    </td>
                    <td class="table-price-label" rowspan="2">N18</td>
                    <td>24.48kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(1738000) }}</td>
                </tr>
                <tr>
                    <td>5,32kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(486000) }}</td>
                </tr>

                <tr>
                    <td rowspan="2">3</td>
                    <td class="text-left" rowspan="2">
                        SƠN MỊN NỘI THẤT CAO CẤP <br>
                        Màng sơn nhẵn mịn, chống nấm mốc
                    </td>
                    <td class="table-price-label" rowspan="2">N19</td>
                    <td>24,48kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(1838000) }}</td>
                </tr>
                <tr>
                    <td>5,76kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(550000) }}</td>
                </tr>

                <tr>
                    <td rowspan="3">4</td>
                    <td class="text-left" rowspan="3">
                        SƠN BÓNG MỜ NỘI THẤT CAO CẤP <br>
                        Màng sơn bóng, chống thấm và chống rêu mốc
                    </td>
                    <td class="table-price-label" rowspan="3">N16</td>
                    <td>19,72kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(3123000) }}</td>
                </tr>
                <tr>
                    <td>4,64kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(939000) }}</td>
                </tr>
                <tr>
                    <td>1,16kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(210000) }}</td>
                </tr>

                <tr>
                    <td rowspan="3">5</td>
                    <td class="text-left" rowspan="3">
                        SƠN BÓNG NỘI THẤT CAO CẤP <br>
                        Mặt sơn siêu bóng, màu sắc sang trọng, thách thức thời gian
                    </td>
                    <td class="table-price-label" rowspan="3">N15</td>
                    <td>18,36kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(3836000) }}</td>
                </tr>
                <tr>
                    <td>5,4kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(1135000) }}</td>
                </tr>
                <tr>
                    <td>1,08kg</td>
                    <td>LLít</td>
                    <td>{{ number_format(255000) }}</td>
                </tr>

                <tr>
                    <td rowspan="3">6</td>
                    <td class="text-left" rowspan="3">
                        SƠN NỘI THẤT SIÊU BÓNG ĐẶC BIỆT <br>
                        Đẹp, bền màu, dễ lau chùi, có khả năng che khuất vết nứt nhỏ, màng sơn cứng, bóng ngọc trai,
                        không chứa chì và thủy ngân.
                    </td>
                    <td class="table-price-label" rowspan="3">N 14</td>
                    <td>18,36kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(4555000) }}</td>
                </tr>
                <tr>
                    <td>5,4kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(1350000) }}</td>
                </tr>
                <tr>
                    <td>1,08kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(388000) }}</td>
                </tr>

                <tr>
                    <td class="table-price-label" colspan="6">CÁC SẢN PHẨM SƠN NGOÀI TRỜI</td>
                </tr>
                <tr>
                    <td rowspan="3">1</td>
                    <td class="text-left" rowspan="3">
                        SƠN MỊN NGOẠI THẤT CAO CẤP <br>
                        Độ phủ cao, chống thấm, chống rêu mốc
                    </td>
                    <td class="table-price-label" rowspan="3">N10</td>
                    <td>22,95kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(2015000) }}</td>
                </tr>
                <tr>
                    <td>5,4kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(621000) }}</td>
                </tr>
                <tr>
                    <td>1,35kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(183000) }}</td>
                </tr>

                <tr>
                    <td rowspan="3">2</td>
                    <td class="text-left" rowspan="3">
                        SƠN BÓNG MỜ NGOẠI THẤT CAO CẤP <br>
                        Mặt sơn bóng, lau chùi hiệu quả
                    </td>
                    <td class="table-price-label" rowspan="3">N09</td>
                    <td>19,72kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(3570000) }}</td>
                </tr>
                <tr>
                    <td>4,64kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(1100000) }}</td>
                </tr>
                <tr>
                    <td>1,16kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(239000) }}</td>
                </tr>

                <tr>
                    <td rowspan="3">3</td>
                    <td class="text-left" rowspan="3">
                        SƠN BÓNG NGOẠI THẤT CAO CẤP <br>
                        Bảo vệ 10 năm, kháng tia cực tím,
                        100% nhựa nguyên chất, không chứa APEO
                    </td>
                    <td class="table-price-label" rowspan="3">N08</td>
                    <td>18,36kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(4255000) }}</td>
                </tr>
                <tr>
                    <td>5,4kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(1298000) }}</td>
                </tr>
                <tr>
                    <td>1,08kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(363000) }}</td>
                </tr>

                <tr>
                    <td rowspan="3">4</td>
                    <td class="text-left" rowspan="3">
                        SƠN NGOẠI THẤT BỀN MÀU TỐI ƯU <br>
                        10 năm bảo vệ màu, bền màu dài lâu, màng sơn chai cứng, ít bám bụi, chống rong rêu và nấm mốc,
                        kháng tia cực tím tối đa.
                    </td>
                    <td class="table-price-label" rowspan="3">N06</td>
                    <td>18,36kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(4886000) }}</td>
                </tr>
                <tr>
                    <td>5,4kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(1450000) }}</td>
                </tr>
                <tr>
                    <td>1,08kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(418000) }}</td>
                </tr>

                <tr>
                    <td rowspan="2">5</td>
                    <td class="text-left" rowspan="2">
                        SƠN MEN SỨ NGOẠI THẤT ĐẶC BIỆT <br>
                        12 năm bảo vệ, màng sơn co giãn gấp 6 lần chống rạn nứt và chống thấm vượt trội, chống nóng,
                        chống trầy xước, thân thiện với môi trường.
                    </td>
                    <td class="table-price-label" rowspan="2">N05</td>
                    <td>5,5kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(1978000) }}</td>
                </tr>
                <tr>
                    <td>1,1kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(399000) }}</td>
                </tr>
                <tr>
                    <td class="table-price-label" colspan="6">CÁC SẢN PHẨM SƠN LÓT</td>
                </tr>

                <tr>
                    <td rowspan="2">1</td>
                    <td class="text-left" rowspan="2">
                        SƠN LÓT KHÁNG KIỀM NỘI THẤT CAO CẤP <br>
                        Chống thấm, chống rêu mốc
                    </td>
                    <td class="table-price-label" rowspan="2">N04</td>
                    <td>23,4kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(1830000) }}</td>
                </tr>
                <tr>
                    <td>5,2kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(671000) }}</td>
                </tr>

                <tr>
                    <td rowspan="2">2</td>
                    <td class="text-left" rowspan="2">
                        SƠN LÓT KHÁNG KIỀM NGOẠI THẤT CAO CẤP <br>
                        Trung hòa độ pH tối đa, chống thấm theo thời gian
                    </td>
                    <td class="table-price-label" rowspan="2">N03</td>
                    <td>22kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(2359000) }}</td>
                </tr>
                <tr>
                    <td>5kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(810000) }}</td>
                </tr>

                <tr>
                    <td rowspan="2">3</td>
                    <td class="text-left" rowspan="2">
                        SƠN LÓT ĐẶC BIỆT CAO CẤP <br>
                        Chống thấm, kháng muối, kháng kiềm, bám dính tuyệt hảo
                    </td>
                    <td class="table-price-label" rowspan="2">N02</td>
                    <td>20,4kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(2838000) }}</td>
                </tr>
                <tr>
                    <td>6kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(896000) }}</td>
                </tr>
                <tr>
                    <td class="table-price-label" colspan="6">CÁC SẢN PHẨM CHỐNG THẤM</td>
                </tr>
                <tr>
                    <td rowspan="2">1</td>
                    <td class="text-left" rowspan="2">
                        SƠN CHỐNG THẤM ĐA NĂNG CAO CẤP <br>
                        Chống thấm, chống rêu mốc tối đa
                    </td>
                    <td class="table-price-label" rowspan="2">N22</td>
                    <td>19,5kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(2868000) }}</td>
                </tr>
                <tr>
                    <td>4,6kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(836000) }}</td>
                </tr>
                <tr>
                    <td rowspan="2">2</td>
                    <td class="text-left" rowspan="2">
                        SƠN CHỐNG THẤM MÀU CAO CẤP <br>
                        Chống thấm, màu sắc đẹp tự chọn
                    </td>
                    <td class="table-price-label" rowspan="2">N24</td>
                    <td>18,36kg</td>
                    <td>Thùng</td>
                    <td>{{ number_format(4268000) }}</td>
                </tr>
                <tr>
                    <td>4,32kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(1358000) }}</td>
                </tr>
                <tr>
                    <td class="table-price-label" colspan="6">SẢN PHẨM DẦU BÓNG (CLEAR)</td>
                </tr>
                <tr>
                    <td rowspan="2">1</td>
                    <td class="text-left" rowspan="2">
                        SƠN PHỦ BÓNG
                    </td>
                    <td class="table-price-label" rowspan="2">N72</td>
                    <td>4kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(993000) }}</td>
                </tr>
                <tr>
                    <td>1kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(272000) }}</td>
                </tr>
                <tr>
                    <td class="table-price-label" colspan="6">CÁC SẢN PHẨM BỘT BẢ</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td class="text-left">
                        BỘT BẢ NỘI THẤT (Dùng cho sơn mịn)
                    </td>
                    <td class="table-price-label">N25</td>
                    <td>40kg</td>
                    <td>Bao</td>
                    <td>{{ number_format(332000) }}</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td class="text-left">
                        BỘT BẢ NGOẠI THẤT (Dùng cho sơn mịn)
                    </td>
                    <td class="table-price-label">N26</td>
                    <td>40kg</td>
                    <td>Bao</td>
                    <td>{{ number_format(432000) }}</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td class="text-left">
                        BỘT BẢ CAO CẤP ĐẶC BIỆT (5 IN 1)
                    </td>
                    <td class="table-price-label">N28</td>
                    <td>40kg</td>
                    <td>Bao</td>
                    <td>{{ number_format(455000) }}</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td class="text-left">
                        BỘT BẢ CHỐNG THẤM ĐB NGOẠI THẤT
                    </td>
                    <td class="table-price-label">N29</td>
                    <td>40kg</td>
                    <td>Bao</td>
                    <td>{{ number_format(498000) }}</td>
                </tr>
                <tr>
                    <td class="table-price-label" colspan="6">CÁC SẢN PHẨM SƠN NHŨ ĐẶC BIỆT</td>
                </tr>
                <tr>
                    <td rowspan="2">1</td>
                    <td class="text-left" rowspan="2">
                        SƠN MEN SỨ ĐẶC BIỆT
                    </td>
                    <td class="table-price-label" rowspan="2">N13</td>
                    <td>4,4kg</td>
                    <td>Lon</td>
                    <td>{{ number_format(1568000) }}</td>
                </tr>
                <tr>
                    <td>1,1kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(398000) }}</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td class="text-left">
                        SƠN NHŨ BẠC NỘI-NGOẠI THẤT 5555
                    </td>
                    <td class="table-price-label">N12</td>
                    <td>1kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(400000) }}</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td class="text-left">
                        SƠN NHŨ VÀNG NỘI-NGOẠI THẤT 9898
                    </td>
                    <td class="table-price-label">N11</td>
                    <td>1kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(400000) }}</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td class="text-left">
                        SƠN NHŨ VÀNG ĐẶC BIỆT 9999
                    </td>
                    <td class="table-price-label">N49</td>
                    <td>1kg</td>
                    <td>Lít</td>
                    <td>{{ number_format(598000) }}</td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <div class="text-center margin-top-40 title">
            <h3>ẢNH BẢNG GIÁ</h3>
            @if(!empty($listPrice))
                @foreach($listPrice as $price)
                    <br>
                    <img class="price-image" src="{{ asset('images/price/'.$price->image) }}"
                         alt="Bảng giá sơn Nesan, nesan">
                    <br>
                @endforeach
            @endif
        </div>
    </div>
@endsection
