@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Bài viết hay, nổi bật đến từ Công ty Tài Phát">
    <meta name="keywords"
          content="bài viết,post, chủ để ,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>{{$post->title}} | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_content')
    <div class="wrap-page-title-emty no-mobile"></div>
    <div id="maincontent">
        <div class="container">
            <div class="max-830">
                <h1 class="title color-main text-center" style="margin-bottom: 45px"><span>{{$post->title}}</span></h1>
                <p style="text-align: justify;">{{$post->description}}</p>
                <p style="text-align: justify;"><br/>{!! $post->content !!}</p>
                <p style="text-align: justify;">&nbsp;</p>
            </div>
            <!-- Chia sẻ mạng xã hội -->
            <div class="share">
                <a target="_blank"
                   href="https://facebook.com/sharer/sharer.php?app_id=1101735459917442&amp;kid_directed_site=0&amp;sdk=joey&amp;u={{route('chitiet.tintuc',['post'=>$post->convert(),'id'=>$post->id])}}&amp;display=popup&amp;ref=plugin&amp;src=share_button"
                   class="fb"><img width="40px" src="/images/fb.png" alt=""></a>
                <div style="width: 40px" class="zalo-share-button" data-href="{{route('chitiet.tintuc',['post'=>$post->convert(),'id'=>$post->id])}}" data-oaid="579745863508352884"
                     data-layout="2"
                     data-color="blue" data-customize=false></div>
            </div>
            <!-- Chia sẻ mạng xã hội -->
        </div>
        <div class="bottom-content bg-1 TOA001">
            <div class="container">
                <div class="row block-1">
                    @if($post_same->isEmpty())
                        <div class="col-md-12" style="margin-top: 10px;text-align: center"><h4>Không có tin tức cùng
                                danh mục</h4></div>
                    @else
                        @foreach($post_same as $same)
                            <div class="col-sm-4 col-md-4">
                                <div class="item">
                                    <a href="{{route('chitiet.tintuc',['post'=>$same->convert(),'id'=>$same->id])}}" class="item_img">
                                        <img src="/images/post/{{$same->images}}">
                                    </a>
                                    <div class="item_content">
                                        <p class="item_date">{{$same->getTimeCreated()}}</p>
                                        <a href="{{route('chitiet.tintuc',['post'=>$same->convert(),'id'=>$same->id])}}" class="item_title">{{$same->title}}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <a href="{{route('tintuc')}}" class="more btn  btn-primary"><span class="inner">Xem thêm tin tức</span></a>
            </div>
        </div>
    </div>
@stop
@section('page_js')
    <script src="https://sp.zalo.me/plugins/sdk.js"></script>
@endsection