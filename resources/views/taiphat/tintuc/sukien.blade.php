@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Sự kiện nổi bật, Sự kiện đặc sắc của Công ty Tài Phát">
    <meta name="keywords"
          content="Sự kiện , sukien ,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Sự kiện nổi bật | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_content')
    <br>
    <div id="maincontent">
        <div class="container">
            <ul class="contentmenu">
                <li><a href="{{route('tintuc')}}">Tin tức</a></li>
                <li class="active"><a href="{{route('sukien')}}">Sự kiện</a></li>
            </ul>
            <div class="page-list-news">
                <div class="feature-post block-3">
                    <div class="item">
                        <a href="{{route('chitiet.tintuc',['post'=>$post_check->convert(),'id'=>$post_check->id])}}" class="item_img"><img
                                    src="/images/post/{{$post_check->images}}"></a>
                        <div class="item_content">
                            <a href="{{route('chitiet.tintuc',['post'=>$post_check->convert(),'id'=>$post_check->id])}}"
                               class="item_title">{{$post_check->title}}</a>
                            <div class="item_decs">{{$post_check->description}}</div>
                            <a href="{{route('chitiet.tintuc',['post'=>$post_check->convert(),'id'=>$post_check->id])}}" class="btn  btn-primary"><span
                                        class="inner">Xem thêm</span></a>
                        </div>
                    </div>
                </div>
                <div class="row block-1">
                    @if(!$events->isEmpty())
                        @foreach($events as $event)
                            <div class="col-sm-4 col-md-4">
                                <div class="item">
                                    <a href="{{route('chitiet.tintuc',['post'=>$event->convert(),'id'=>$event->id])}}" class="item_img"><img
                                                src="images/post/{{$event->images}}"></a>
                                    <div class="item_content">
                                        <p style="color: #0E1112" class="item_date">{{$event->getTimeCreated()}}</p>
                                        <a href="{{route('chitiet.tintuc',['post'=>$event->convert(),'id'=>$event->id])}}"
                                           class="item_title">{{$event->title}}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <hr>
    @include('taiphat.includes.list_footer')
@endsection