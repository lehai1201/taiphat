@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Bài viết hay, nổi bật đến từ Công ty Tài Phát">
    <meta name="keywords"
          content="bài viết,post, chủ để ,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Tuyển dụng | Nesan Việt Nam</title>
@endsection
@section('page_content')
    <div class="wrap-page-title-emty no-mobile"></div>
    <div id="maincontent">
        <div class="container">
            <div class="max-830">
                <h1 class="title color-main text-center" style="margin-bottom: 45px"><span>TUYỂN DỤNG</span></h1>
                <p style="text-align: justify;"><br/>{!! data_get($data, 'content') !!}</p>
                <p style="text-align: justify;">&nbsp;</p>
            </div>
        </div>
    </div>
@stop
