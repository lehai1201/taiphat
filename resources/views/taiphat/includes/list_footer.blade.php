<div class="TOA003">
    <div class="container">
        <div class="row block grid-space-10">
            <div class="col-sm-3 col-xs-6 ">
                <a class="item item_1" href="{{route('daili')}}">
                    <span class="item_inner">
                        <span class="item_icon">
                            <span class="hexagon-140"></span><i class="icon-icon-1"> </i>
                        </span>
                        <span class="item_title upercase color-red">Tìm đại lý</br> gần nhất </span>
                    </span>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6 ">
                <a class="item item_2" href="{{route('congthuc')}}">
                    <span class="item_inner">
                        <span class="item_icon">
                            <span class="hexagon-140"></span>
                            <i class="icon-icon-2"> </i>
                        </span>
                        <span class="item_title upercase color-red">Công thức </br>tính sơn</span>
                    </span>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6 ">
                <a class="item item_3" href="{{route('sanpham')}}">
                    <span class="item_inner">
                        <span class="item_icon"><span class="hexagon-140"></span><i class="icon-icon-3"></i></span>
                        <span class="item_title upercase color-red">Sản phẩm <br>tiêu biểu</span>
                    </span>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6 ">
                <a class="item item_4" href="{{route('bangmau')}}">
                    <span class="item_inner">
                        <span class="item_icon"><span class="hexagon-140"></span><i class="icon-icon-4"></i></span>
                        <span class="item_title upercase color-red">Bảng màu</span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
