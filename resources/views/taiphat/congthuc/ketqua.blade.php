@extends('taiphat.layout')
@section('page_content')
    <div class="container">
        <div class="row">
            <h1 style="margin-top: 100px;text-align: center">Số diện tích cần phải sơn là: {{$ketqua}} m2</h1>
        </div>
        <div class="row">
            <h3 style="margin-top: 100px;text-align: center">Sản phẩm đề nghị</h3>
        </div>
        <div class="page-list-products">
            <div class="row block-4">
                @if(!$products->isEmpty())
                    @foreach($products as $product)
                        <div class="col-sm-4 col-md-4">
                            <div class="item hover_product">
                                <div class="item_inner">
                                    <a href="{{route('detail.product',['product'=>$product->title,'id'=>$product->id])}}" class="item_img"><img
                                                height="300"
                                                src="/images/product/{{$product->images}}"
                                                alt=""></a>
                                    <a href="{{route('detail.product',['product'=>$product->title,'id'=>$product->id])}}"
                                       class="item_title">{{$product->title}}</a>
                                    <ul class="list_info">
                                        <li class="description_product">{{$product->description}}</li>
                                        <li class="price_product">Giá: {{number_format($product->price)}}
                                            VNĐ/{{$product->unit}}</li>
                                        <li class="description_product">{{$product->kl}}Kg</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <h4 style="text-align: center;color: #8b131b">DANH SÁCH TRỐNG</h4>
                @endif
            </div>
        </div>
    </div>
@stop