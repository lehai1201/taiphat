@extends('taiphat.layout')
@section('page_title')
    <title>Công thức tính sơn | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_content')
    <div class="container">
        <div class="row">
            <br>
            <div class="col-md-12">
                <h1 class="title">Diện tích sơn trong nhà</h1>
                <p>Diện tích sơn trong nhà = Diện tích mặt sàn x số tầng x hệ số sơn trong nhà</p>
                <p>Trong đó:</p>
                <p>Nếu nhà nhiều phòng ít cửa sổ, hệ số sơn: 4.5</p>
                <p>Nếu nhà nhiều phòng ít cửa sổ, hệ số sơn: 4</p>
                <p>Nếu nhà cấp 4 ít cửa, hệ số sơn: 3.5</p>
                <p>Nếu nhà cấp 4 không có trần, hệ số sơn: 3</p>
                <p>Lưu ý: Hệ số trên chỉ đúng khi tính tổng thể của một ngôi nhà, nhưng tính riêng lẻ cho từng phòng thì
                    không còn chính xác nữa</p>
            </div>
        </div>
        @if(!empty($errors) && count($errors) !=0)
            <div style="padding: 5px;text-align: center;color: blue;margin: 10px 0px" class="alert-danger">{{$errors->first()}}</div>
        @endif
        <div class="row" style="margin-left: 20px">
            <form method="POST" action="{{route('post.congthuc')}}">
                @csrf
                <div class="col-md-3">
                    <label class="row-label">
                        <span class="title">Diện tích mặt sàn <p class="error-message"></p></span>
                        <input class="input input-sm" name="dientich" type="text" placeholder="Diện tích mặt sàn">
                    </label>
                </div>
                <div class="col-md-3">
                    <label class="row-label">
                        <span class="title">Số tầng <p class="error-message"></p></span>
                        <input class="input input-sm" name="tang" type="text" placeholder="Số tầng">
                    </label>
                </div>
                <div class="col-md-3">
                    <label class="row-label">
                        <span class="title">Diện tích mặt sàn <p class="error-message"></p></span>
                        <input class="input input-sm" name="san" type="text" placeholder="Diện tích mặt sàn">
                    </label>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary" id="button_caculator"><span
                                class="inner">Tính</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <hr>
    @include('taiphat.includes.list_footer')
@stop
