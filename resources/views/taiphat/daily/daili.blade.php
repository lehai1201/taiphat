@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content="Công ty Tài Phát với hơn 20 nhà phân phối, đại lí sản phẩm tại các tỉnh như Hà Nam, Thanh Hóa, Bắc Ninh... Với mạng lưới cửa hàng rộng khắp, chúng tôi luôn sẵn sàng phục vụ khách hàng 1 cách tốt nhất">
    <meta type="keywords" content="Danh sách đại lý Tài phát, Đại lý Tài phát, Đăng ký làm đại lý, Taiphat, Công ty Tài Phát">
    <title>Đại lý | Công ty Tài Phát | Taiphat</title>
@endsection
@section('page_content')
    <div class="wrap-page-title-emty">&nbsp;</div>
    <div class="TOA045">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center"><h1 class="title"><span>Danh sách đại lý</span></h1></div>
            </div>
            <div class="row">
                @if(!$norland->isEmpty())
                 <h3>Chi nhánh khu vực Miền Bắc</h3>
                @foreach($norland as $agency)
                        <div class="col-md-4 col-sm-6 storelist">
                            <p class="title-18">{{$agency->name}}</p>
                            <ul class="store-item list-icon thirdary">
                                <li><i class="icon-map"></i> {{$agency->address}}</li>
                                <li style="display: none"><i class="icon-phone"></i> Tel: {{$agency->phone}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="row">
                @if(!$central->isEmpty())
                    <h3>Chi nhánh khu vực Miền Trung</h3>
                    @foreach($central as $agency)
                        <div class="col-md-4 col-sm-6 storelist">
                            <p class="title-18">{{$agency->name}}</p>
                            <ul class="store-item list-icon thirdary">
                                <li><i class="icon-map"></i> {{$agency->address}}</li>
                                <li style="display: none"><i class="icon-phone"></i> Tel: {{$agency->phone}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="row">
                @if(!$south->isEmpty())
                    <h3>Chi nhánh khu vực Miền Nam</h3>
                    @foreach($south as $agency)
                        <div class="col-md-4 col-sm-6 storelist">
                            <p class="title-18">{{$agency->name}}</p>
                            <ul class="store-item list-icon thirdary">
                                <li><i class="icon-map"></i> {{$agency->address}}</li>
                                <li style="display: none"><i class="icon-phone"></i> Tel: {{$agency->phone}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="row" style="margin-top:2em;">

                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="formGroupInputSmall"><h4 style="color: #ac0303;">Tìm
                                kiếm đại lý</h4></label>
                        <div class="col-md-6">
                            @if($data_agencies->isEmpty())
                                <h3>Danh sách trống</h3>
                            @else
                                <select id="agency_map" class="form-control" style="padding-left:1em;">
                                    @foreach($data_agencies as $agency)
                                        <option value="{{$agency->id}}">{{$agency->name}}
                                            - {{$agency->address}}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                </form>
                <div id="map_agency" style="text-align: center">

                </div>
            </div>
        </div>
    </div>
    <div class="TOA046 bg-1">
        <div class="container">
            <form class="row mangosteen" id="formDaili" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1">
                    <h2 class="section-title">Đăng ký làm đại lý</h2>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <label class="row-label">
                                <span class="title">Họ & tên <p class="error-message"></p></span>
                                <input class="input input-sm" name="name" type="text" placeholder="Họ & tên">
                            </label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label class="row-label">
                                <span class="title">Email <p class="error-message"></p></span>
                                <input name="email" class="input input-sm" type="text" placeholder="Email">
                            </label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label class="row-label">
                                <span class="title">Số điện thoại <p class="error-message"></p></span>
                                <input class="input input-sm" name="phone" type="text" placeholder="Số điện thoại">
                            </label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label class="row-label">
                                <span class="title">Địa chỉ <p class="error-message"></p></span>
                                <input class="input input-sm" name="address" type="text" placeholder="Địa chỉ">
                            </label>
                        </div>
                        <div class="col-sm-12 text-center pt-10">
                            <button id="button_submit_daili" onclick="registerDaili()" class="btn btn-primary"><span
                                        class="inner">Gửi</span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('taiphat.includes.list_footer')
@stop
@section('page_js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
            });
            //    LOAD MAP ĐẦU TIÊN
            var id = $("#agency_map").select().val();
            loadMapAgency(id)
        });

        $("#agency_map").on('change', function () {
            id = $("#agency_map").val();
            loadMapAgency(id);
        });

        function loadMapAgency(id) {
            $.ajax({
                type: 'POST',
                url: '{{route('daily.load.map')}}',
                data: {
                    id: id
                },
                success: function (data) {
                    $("#map_agency").html(data);
                }
            });
        }

        function registerDaili() {
            $("#button_submit_daili").attr('disabled', true);
            var data_all = $("#formDaili").serialize();
            $.ajax({
                type: 'POST',
                url: '{{route('daili.dangki')}}',
                data: data_all,
                success: function (data) {
                    if (data == 'success') {
                        swal('Gửi đơn thành công', '', 'success');
                        setTimeout(function () {
                            swal.close();
                            window.location.reload();
                        }, 1500);
                    } else {
                        $("#button_submit_daili").attr('disabled', false);
                        swal(data, '', 'error')

                    }
                }
            });
        }
    </script>
@stop
