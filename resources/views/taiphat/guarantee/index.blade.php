@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content=" Công ty cổ phần Tập Đoàn Nesan đặt phương châm  'Khách hàng là niềm tin' làm kim chỉ nam hoạt động. Chúng tôi luôn cung cấp những sản phẩm sơn tốt nhất, uy tín nhất cho ngôi nhà của bạn ">
    <meta name="keywords"
          content="Bảo hành,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Bảo hành</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/baohanh.css">
@stop
@section('page_content')
    <div class="wrap-page-title-emty">&nbsp;</div>
    <div class="container">
        <div class="row">
            <div class="page-title"><h1 class="title"><span>BẢO HÀNH</span></h1></div>
        </div>
        <div class="row">
        </div>
    </div>
    <div class="container">
        @if(Session::has('msg_error'))
            <div class="alert-danger"
                 style="padding: 10px;margin-bottom: 50px">{{ Session::get('msg_error') }}</div>
        @endif
        @if(Session::has('msg_success'))
            <div class="alert-success"
                 style="padding: 10px;margin-bottom: 50px">{{ Session::get('msg_success') }}</div>
        @endif
        <div class="row row-eq-height">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header bg-primary ">Đăng ký bảo hành</div>
                    <div class="card-body">
                        <form method="POST" id="form_lienhe" action="{{ route('guarantee.add') }}">
                            @csrf
                            <div class="form-group">
                                <label for="name">Tên khách hàng <span class="req">*</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" maxlength="64">
                                @include('layouts.msg_error', ['item' => 'name'])
                            </div>
                            <div class="form-group">
                                <label for="phone">Số điện thoại <span class="req">*</span></label>
                                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" maxlength="14">
                                @include('layouts.msg_error', ['item' => 'phone'])
                            </div>
                            <div class="form-group">
                                <label for="phone">Địa chỉ <span class="req">*</span></label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}" maxlength="64">
                                @include('layouts.msg_error', ['item' => 'address'])
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}" maxlength="64">
                                @include('layouts.msg_error', ['item' => 'email'])
                            </div>
                            <button type="submit" class="button_submit_agency" id="button_send_info">GỬI
                                THÔNG TIN <i
                                    class="fa fa-chevron-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div>
                    <h5>Nếu đã có tài khoản, vui lòng ấn <a class="search-link" href="{{ route('guarantee.search') }}">vào đây</a> hoặc nút bên dưới để tới trang tra cứu thông tin bảo hành.</h5>
                    <p>&nbsp;<a class="policy-link" href="{{ route('guarantee.policy') }}"> » Xem thêm chính sách bảo hành</a></p>
                    <br>
                    <div class="text-center">
                        <a class=" button-search" href="{{ route('guarantee.search') }}">Tra cứu bảo hành</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
