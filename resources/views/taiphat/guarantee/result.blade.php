@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content=" Công ty cổ phần Tập Đoàn Nesan đặt phương châm  'Khách hàng là niềm tin' làm kim chỉ nam hoạt động. Chúng tôi luôn cung cấp những sản phẩm sơn tốt nhất, uy tín nhất cho ngôi nhà của bạn ">
    <meta name="keywords"
          content="Bảo hành,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Thông tin bảo hành</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/baohanh.css">
@stop
@section('page_content')
    <div class="wrap-page-title-emty">&nbsp;</div>
    <div id="maincontent">
        <div class="container">
            <div class="row">
                <div class="page-title"><h1 class="title"><span>THÔNG TIN KHÁCH HÀNG</span></h1></div>
            </div>
            @if(Session::has('msg_error'))
                <div class="alert-danger"
                     style="padding: 10px;margin-bottom: 50px">{{ Session::get('msg_error') }}</div>
            @endif
            @if(Session::has('msg_success'))
                <div class="alert-success"
                     style="padding: 10px;margin-bottom: 50px">{{ Session::get('msg_success') }}</div>
            @endif

            @if(!empty($dataClient))
                <div class="col-md-12">
                    <div class="form-group has-feedback wrap_tc_3">
                        <div style="text-align: left">
                            <div style="font-weight: bold">
                                <p>Tên khách hàng: {{ $dataClient->name }}</p>
                                <p>Số điện thoại: {{ $dataClient->phone }}</p>
                                <p>Địa chỉ: {{ $dataClient->address }}</p>
                                <p>Email: {{ $dataClient->email }}</p>
                            </div>
                            <p class="text-center"> ------------------- </p>
                            <form class="form-register" action="{{ route('guarantee.register') }}" method="post">
                                @csrf
                                <input type="hidden" name="client_id" value="{{ $dataClient->id }}">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Nhà phân phối: </label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="agency_id" id="listAgency"
                                                style="width:100% !important;">
                                                @foreach($dataAgency as $key => $listAgency)
                                                    <optgroup label="{{ $key }}">
                                                        @if($listAgency->isNotEmpty())
                                                            @foreach($listAgency as  $agency)
                                                                <option
                                                                    value="{{$agency->id}}">{{$agency->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </optgroup>
                                                @endforeach
                                        </select>
                                        @include('layouts.msg_error', ['item' => 'agency_id'])
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Mã sản phẩm: </label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="product_id[]" id="listProduct"
                                                style="width:100% !important;">
                                            @foreach($dataProduct as $key => $listProduct)
                                                <optgroup label="{{ $key }}">
                                                    @if(!empty($listProduct))
                                                        @foreach($listProduct as $product)
                                                            <option
                                                                value="{{$product->id}}">{{$product->product_name}} - {{ $product->code }}</option>
                                                        @endforeach
                                                    @endif
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="number" name="number[]" class="form-control"
                                               placeholder="Số lượng">
                                    </div>
                                    <div class="col-sm-2">
                                        <select class="form-control" name="unit[]">
                                            @foreach(config('constant.UNIT_TEXT') as $key => $unit)
                                                <option value="{{$key}}">{{ $unit }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8">
                                        @if (count($errors->get('product_id.*')) > 0 || count($errors->get('number.*')) > 0)
                                            <div class="msg_error">Sản phẩm hoặc số lượng không được để trống.</div>
                                        @endif
                                    </div>
                                </div>
                                <div id="data-add-product"></div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8">
                                        <button class="btn btn-primary" onclick="addProduct(this)" type="button"><i class="fa fa-plus"></i> Thêm sản phẩm</button>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-6">
                                        <input type="checkbox" class="form-check-input" name="agree" id="exampleCheck1">
                                        <label class="form-check-label"  style="font-weight: 400 !important;"
                                               for="exampleCheck1">Tôi cam kết thông tin vừa nhập là chính xác.</label>
                                        @include('layouts.msg_error', ['item' => 'agree'])
                                    </div>
                                </div>
                                <div class="form-check-register">
                                    <button type="submit" class="col-sm-2 button_submit_register">Gửi thông tin</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="form-group has-feedback wrap_tc" style="margin-bottom:2.5em;width: 100%">
                        <form action="{{ route('guarantee.result') }}" method="get">
                            <h4>CÁC SẢN PHẨM ĐANG ĐƯỢC BẢO HÀNH</h4>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 5%" scope="col">STT</th>
                                    <th class="text-center" style="width: 40%">Tên sản phẩm</th>
                                    <th class="text-center" scope="col">Đại lý</th>
                                    <th class="text-center" scope="col">Số lượng</th>
                                    <th class="text-center" scope="col">Đơn vị</th>
                                    <th class="text-center" scope="col">Ngày đăng ký</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($dataBooking->isNotEmpty())
                                    @php($stt = 1)
                                    @foreach($dataBooking as $data)
                                        <tr>
                                            <th >{{ $stt }}</th>
                                            <td class="text-left">{{ $data->product_name }}</td>
                                            <td>{{ $data->agency_name }}</td>
                                            <td>{{ $data->number }}</td>
                                            <td>
                                                @if((int)$data->unit == 1)
                                                    <div>Thùng</div>
                                                @elseif((int)$data->unit == 2)
                                                    <div>Lon</div>
                                                @elseif((int)$data->unit == 3)
                                                    <div>Lít</div>
                                                @else
                                                    <div>Bao</div>
                                                @endif
                                            </td>
                                            <td>{{ date('d-m-Y', strtotime($data->created_at)) }}</td>
                                        </tr>
                                        @php($stt++)
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-danger text-center">Không có dữ liệu</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            @else
                <div class="text-center text-danger">Không tìm thấy thông tin khách hàng</div>
            @endif
        </div>
    </div>
    <div class="container">
        <div class="row row-eq-height">
            <div class="col-md-12">

            </div>
        </div>
    </div>
    <div id="data-option" style="display: none">
        @foreach($dataProduct as $key => $listProduct)
            <optgroup label="{{ $key }}">
                @if(!empty($listProduct))
                    @foreach($listProduct as $product)
                        <option
                            value="{{$product->id}}">{{$product->product_name}} - {{ $product->code }}</option>
                    @endforeach
                @endif
            </optgroup>
        @endforeach
    </div>
    <div id="data-unit" style="display: none">
        @foreach(config('constant.UNIT_TEXT') as $key => $unit)
            <option value="{{$key}}">{{ $unit }}</option>
        @endforeach
    </div>
@endsection
@section('button_footer')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#listAgency").select2();
            $("#listProduct").select2();
        });

        function addProduct() {
            let same = $("#data-option").html();
            let unit = $("#data-unit").html();
            let lastField = $("#data-add-product");
            let removeButton = "<button type=\"button\" onclick=\"$(this).closest('.form-group').remove();\" class=\"btn btn-danger\">Xóa</button>";

            let html = "<div class=\"form-group row\"><div class=\"col-sm-2\">"
            html += "</div><div class=\"col-sm-4\"><select class=\"form-control\" name=\"product_id[]\" style=\"width:100% !important;\">"+same+"</select></div>";
            html += "<div class=\"col-sm-2\"> <input type=\"number\" name=\"number[]\" class=\"form-control\" placeholder=\"Số lượng\"></div>";
            html += "<div class=\"col-sm-2\"> <select class=\"form-control\" name=\"unit[]\" style=\"width:100% !important;\">"+unit+"</select></div>";
            html += "<div class=\"col-sm-1\">" + removeButton + "</div></div>";
            lastField.append(html);
        }
    </script>
@stop

