@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content=" Công ty cổ phần Tập Đoàn Nesan đặt phương châm  'Khách hàng là niềm tin' làm kim chỉ nam hoạt động. Chúng tôi luôn cung cấp những sản phẩm sơn tốt nhất, uy tín nhất cho ngôi nhà của bạn ">
    <meta name="keywords"
          content="Bảo hành,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Chính sách bảo hành</title>
@endsection
@section('page_content')
    <div class="row row-eq-height">
        <div class="wrap-page-title-emty">&nbsp;</div>
        <div class="container">
            <div class="row">
                <div class="page-title"><h1 class="title"><span>Chính sách bảo hành</span></h1></div>
            </div>
            <div class="row">
                <b>A. Điều kiện được bảo hành.</b><br>
                - Sản phẩm phải do Công ty cổ phần Tập Đoàn Nesan sản xuất và được nhà phân phối sơn chính
                thức xác nhận (kèm hóa đơn hoặc chứng từ).<br>
                - Sử dụng đúng hướng đẫn được in trên bao bì sản phẩm.Nếu mục đích đặc biệt ngoài hướng dẫn sử
                dụng,Người sử dụng phải được nhà sản xuất xác nhận bằng văn bản, tư vấn thích hợp trước khi thi
                công.<br>
                - Trong thời gian thi công đơn vị được bảo hành phải tạo điều kiện dễ dàng cho việc kiểm tra, giám sát
                từ đại diện đơn vị bảo hành và đơn vị được bảo hành phải thực hiện sửa chữa ngay lập tức các hư
                hỏng được cảnh báo có thể gây ảnh hưởng đến chất lượng sơn hoặc làm cho phần hư hỏng càng nặng
                hơn.<br>
                - Đơn vị bảo hành sẽ cung cấp lượng sơn phù hợp để đơn vị được bảo hành tự thi công lại những phần
                sản phẩm bị hư hỏng chỉ sau khi đơn vị bảo hành có xác nhận chính thức và thông báo bằng văn bản
                nguyên nhân và giải pháp khắc phục hư hỏng. Số lượng được cung cấp để thi công lại phân bị hư
                hỏng này tối đa bằng số lượng và đúng chủng loại sản phẩm đã thực cung cấp cho công trình và được
                ghi trên phiếu bảo hành này.<br>
                - Chỉ áp dụng cho công trình xây mới.<br>
                <br>
                <b>B. Điều kiện không được bảo hành.</b><br>
                - Hư hỏng ở bất cứ dạng hoặc quy mô nào do việc sử dụng không đúng theo hướng dẫn sử dụng của hệ
                thống sơn được in trên bao bì sản phẩm.<br>
                - Hư hỏng do nguyên nhân trực tiếp hoặc gián tiếp bởi các tác động hóa học hoặc cơ lý như: Phản ứng
                hóa học từ môi trường, va đập cơ học, tường bị biến dạng, rạn nứt, ẩm ướt, rạn chân chim, bong tróc
                hư hỏng của phần xây dựng, phai màu từng phần do bị thấm ướt ngược từ nền, chân tường,... hoặc
                thấm từ tầng trên, từ phía bên kia tường...<br>
                - Hư hỏng do các yếu tố khách quan xảy ra không thể đoán trước được nằm ngoài hưỡng dẫn kỹ thuật
                in trên bao bì sản phẩm sơn.<br>
                - Hư hỏng do các bên thứ ba hoặc do đơn vị được bảo hành thiếu kiểm soát gây ra, hoặc do nhưng tiềm
                ẩn về mặt kỹ thuật, kết cấu xây dựng.<br>
                - Hư hỏng ở những nơi việc thi công sơn không đảm bảo về mặt kỹ thuật, bụi bẩn, ẩm ướt kéo dài,
                hoặc do bề mặt thi công quá kém chất lượng, quá mềm, yếu.<br>
                - Khi có hư hỏng mà đơn vị được bảo hành không thông báo kịp thời thì đơn vị bảo hành không chịu
                trách nhiệm với các hư hỏng phát sinh.<br>
                - Hư hỏng do đơn vị được bảo hành sử dụng các sản phẩm khác cùng với sản phẩm của Tài Phát sản
                xuất trên cùng một phần hoặc toàn bộ bề mặt thi công.<br>
                - Trường hợp đơn vị được bảo hành không đồng ý với số lượng, chủng loại sơn được bảo hành hoặc
                sản phẩm ghi trên phiếu bảo hành bị phát hiện không đúng số lượng, không đúng chủng loại được thi
                công thực tế cho công trình hoặc có những yêu cầu khác không đúng với điều khoản ghi trên phiếu
                bảo hành thì phiếu bảo hành này không có giá trị.
            </div>
            <br>
            <div class="row">
                <img src="{{ asset('images/tp/1.jpg') }}" alt="">
                <img src="{{ asset('images/tp/2.jpg') }}" alt="">
                <img src="{{ asset('images/tp/3.jpg') }}" alt="">
                <img src="{{ asset('images/tp/4.jpg') }}" alt="">
            </div>
        </div>
@endsection
