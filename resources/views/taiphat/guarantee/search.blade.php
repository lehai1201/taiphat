@extends('taiphat.layout')
@section('page_title')
    <meta name="description"
          content=" Công ty cổ phần Tập Đoàn Nesan đặt phương châm  'Khách hàng là niềm tin' làm kim chỉ nam hoạt động. Chúng tôi luôn cung cấp những sản phẩm sơn tốt nhất, uy tín nhất cho ngôi nhà của bạn ">
    <meta name="keywords"
          content="Bảo hành,taiphat,congtytaiphat,Sơn Tài Phát, Công ty Tài Phát,ANTINI,NESAN,MALLEND">
    <title>Tra cứu thông tin bảo hành</title>
@endsection
@section('page_css')
    <link rel="stylesheet" href="/css/baohanh.css">
@stop
@section('page_content')
    <div class="wrap-page-title-emty">&nbsp;</div>
    <div id="maincontent">
        <div class="container">
            <div class="row">
                <div class="page-title"><h1 class="title"><span>TRUNG TÂM BẢO HÀNH</span></h1></div>
            </div>
            <div class="row">
            </div>

            <div class="col-md-12">
                <h5 class="text-center">TRUNG TÂM HỖ TRỢ TRA CỨU THÔNG TIN, CHÍNH SÁCH BẢO HÀNH SẢN PHẨM CHÍNH HÃNG</h5>
                <div class="form-group has-feedback wrap_tc_2" style="margin-bottom:2.5em;width: 100%">
                   <div style="text-align: left">
                       <p>Đối với các sản phẩm của Nesan Việt Nam kinh doanh sẽ được bảo hành từ 2 đến 5 năm.</p>
                       <p> Đổi mới nếu có lỗi từ sản phẩm.</p>
                       <p>&nbsp;<a class="policy-link" href="{{ route('guarantee.policy') }}"> » Xem thêm chính sách bảo hành</a></p>
                   </div>
                </div>
                <div class="form-group has-feedback wrap_tc" style="margin-bottom:2.5em;width: 100%">
                    <form action="{{ route('guarantee.result') }}" method="get">
                        <h4>TRA CỨU THÔNG TIN BẢO HÀNH</h4>
                        <div class="input-group mb-3" style="width: 50%; margin: auto">
                            <div class="input-group-addon">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-search"></i></span>
                            </div>
                            <input id="search_input" type="text" name="phone" class="form-control" required
                                   placeholder="Nhập số điện thoại để tra cứu">
                            <div class="input-group-addon group-search">
                                <button class="btn btn-search" type="submit">Kiểm tra</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row row-eq-height">
            <div class="col-md-12">

            </div>
        </div>
    </div>
@endsection
