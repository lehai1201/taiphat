@if ($errors->has($item))
    <div class="msg_error">{{ $errors->first($item) }}</div>
@endif
