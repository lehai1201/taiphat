<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Tài Phát - Management</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Công ty Tài Phát -  Management || Nhà sản xuất và phân phối sơn uy tín"
          name="description"/>
    <meta content="" name="author"/>
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('css/public/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    @yield('page_level_plugin')
    <link href="/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/pages/css/login-4.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="/assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/toastr/toastr.min.css">
    <link href="{{ asset('css/date-picker.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/backend.css') }}">
    @yield('page_head_include')
    <link rel="shortcut icon" href="/images/logo2.png"/>
</head>
<body class="page-container-bg-solid page-header-top-fixed">
<div class="page-wrapper">
    <div class="page-wrapper-row">
        <div class="page-wrapper-top">
            <div class="page-header">
            @include('backend.includes.page_header_top')
            @include('backend.includes.menu_header_top')
            </div>
        </div>
    </div>
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            @yield('page_content')
        </div>
    </div>
    @include('backend.includes.page_footer')
</div>

<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="{{ asset('css/public/datatables.min.js') }}" type="text/javascript"></script>
@yield('begin_page_pugin_js')
<script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script src="/jquery-validate/dist/jquery.validate.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
@yield('begin_page_level_js')
<script src="/assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="/assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/select2/dist/js/select2.min.js"></script>
<script src="{{ asset('js/date-picker.js') }}"></script>
@yield('page_footer_script')
</body>
</html>
