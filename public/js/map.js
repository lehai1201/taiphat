var mapList= [];
var LatLngList =
  {
  	['hochiminh']:[],
	  ['hanoi']:[],
	  ['nhatrang']: [],
	  ['danang']: [],
	  ['binhduong']: [],
	  ['cantho']: []
	};
// var infoWindow = [];

// const colors = {
// 	'hochiminh': 'red',
// 	'hanoi': 'green',
// 	'nhatrang': 'blue',
// 	'danang' : 'orange',
// 	'binhduong' : 'black',
// 	'cantho' : 'yellow'
// }

var dest = [];
var curentPos;
var loaded = 0;


function createMap() {
	  callbackGeo();

	for (var key in mapList){

    	var storeItem = jQuery('.store-item[data-map="'+key+'"]').first();
    	var  infoWindow = new google.maps.InfoWindow({maxWidth: 350});
	    var latlng = storeItem.data("latlng");
	    var mapid = storeItem.data("map");

    	latlng = latlng.split(",");
        var lat = Number(latlng[0]);
        var lng = Number(latlng[1]);

        dest[key]={"lat":lat,"lng": lng };

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: mapList[key],
            icon: 'asset/images/icons/icon_mark.svg'
        });
        if(LatLngList[mapid]!= undefined)
        	LatLngList[mapid].push(new google.maps.LatLng(lat, lng));

        var pos = {
            lat: Number(latlng[0]),
            lng: Number(latlng[1])
        };
       	infoWindow.setPosition(pos);
        infoWindow.setContent(storeItem.data("title"));

        infoWindow.open(mapList[key]);
        mapList[key].setCenter(pos);

        // callbackGeo();


	    if(LatLngList[key]!= undefined){
	    	LatLngList[key].forEach(function(latLng){
		    	var latlngbounds = new google.maps.LatLngBounds();
		        latlngbounds.extend(latLng);
		    });
	    }

	   if(mapList[key] != undefined)
	   	mapList[key].setZoom(13);

	}
}




function callbackGeo(){
 if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {

        	for (var key in mapList){

	        	jQuery('.map_info').hide();

	        	var storeItem = jQuery('.store-item[data-map="'+key+'"]').first();

	            curentPos = {
	                lat: position.coords.latitude,
	                lng: position.coords.longitude
	            };
	            var marker = new google.maps.Marker({
	                position: curentPos,
	                map: mapList[key],
	                icon: 'asset/images/icons/icon_current.svg'
	            });
	            if(LatLngList[key]!= undefined)
	            	LatLngList[key].push(new google.maps.LatLng(curentPos.lat, curentPos.lng));


	            const directionsService = new google.maps.DirectionsService;
			    const directionsDisplay = new google.maps.DirectionsRenderer({
			        suppressMarkers: true,
			        polylineOptions: {
			            // strokeColor: colors[key]
			            strokeColor: '#ac0303'
			        }
			    });
			    directionsDisplay.setMap(mapList[key]);

		        directionsService.route({
		               origin: curentPos,
		               destination: {
		                   lat: dest[key].lat,
		                   lng: dest[key].lng
		               },
		               travelMode: 'DRIVING'
		           }, function(response, status) {
		               if (status === 'OK') {
		                   directionsDisplay.setDirections(response);
		               } else {
		               		console.warn('Directions request failed due to ' + status);
		                   //window.alert('Directions request failed due to ' + status);
		               }
		           });

		        if(loaded <= 6){
					console.log(curentPos.lat,curentPos.lng);

					var url = "https://www.google.com/maps/dir/?api=1";
					url = url + "&origin="+ curentPos.lat + "," + curentPos.lng;
					url = url + "&destination=" + dest[key].lat + "," + dest[key].lng;
					jQuery('#map_'+key+' + a.url').attr("href" , url);
					//jQuery('#map_'+key).prev().attr("href" , url);
					loaded++;

				}
	        }

        },
        function(){
        	console.log('error call back');
        }),
        {
	    enableHighAccuracy: true,
	    timeout: 5000,
	    maximumAge: 0
	  };
    }
}


function initMaps() {

   var mapOptions = {
        styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
   		streetViewControl: false
    };

    mapList['hanoi'] = new google.maps.Map(document.getElementById("map_hanoi"), mapOptions);
    mapList['hochiminh'] = new google.maps.Map(document.getElementById("map_hochiminh"), mapOptions);
    mapList['nhatrang'] = new google.maps.Map(document.getElementById("map_nhatrang"), mapOptions);
    mapList['danang'] = new google.maps.Map(document.getElementById("map_danang"), mapOptions);
    mapList['binhduong'] = new google.maps.Map(document.getElementById("map_binhduong"), mapOptions);
    mapList['cantho'] = new google.maps.Map(document.getElementById("map_cantho"), mapOptions);

	createMap();



}


