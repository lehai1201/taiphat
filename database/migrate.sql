CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(128) NOT NULL COMMENT 'Url',
  `title` VARCHAR(128) COMMENT 'Tiêu đề',
  `is_deleted` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
  `created_by` int(10) NOT NULL COMMENT 'Người tạo',
  `updated_by` int(10) DEFAULT NULL COMMENT 'Người cập nhập',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Ngày cập nhập',
  PRIMARY KEY (`id`)
);

ALTER TABLE `agency` ADD COLUMN  `area` SMALLINT(1) DEFAULT 0 AFTER `email`;


CREATE TABLE IF NOT EXISTS `rate`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `avatar`     VARCHAR(512) NOT NULL COMMENT 'Avatar',
    `name`       VARCHAR(128) NOT NULL COMMENT 'Tên',
    `address`    VARCHAR(128) NOT NULL COMMENT 'Địa chỉ',
    `comment`    VARCHAR(128) NOT NULL COMMENT 'Nhận xét',
    `sort`       integer(2)   NOT NULL COMMENT 'Sort',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

ALTER TABLE `rate`
    CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `price`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `image`     VARCHAR(512) NOT NULL COMMENT 'Bảng giá',
    `sort`       integer(2) NOT NULL COMMENT 'sort',
    `type`       integer(2) NOT NULL DEFAULT 0 COMMENT 'Type',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `meta`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `tag`     VARCHAR(512) NOT NULL COMMENT 'Tags',
    `type`       integer(2) NOT NULL DEFAULT 0 COMMENT 'Type',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `clients`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `name`     VARCHAR(512) NOT NULL COMMENT 'Name',
    `phone`       integer(2) NOT NULL COMMENT 'phone',
    `email`       VARCHAR(128) COMMENT 'email',
    `address`       VARCHAR(128) NOT NULL COMMENT 'address',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `client_guarantee`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `client_id`  integer(2) NOT NULL COMMENT 'Client id',
    `agency_id`  integer(2) NOT NULL COMMENT 'Agency id',
    `product_id`  integer(2) NOT NULL COMMENT 'product id',
    `type`       integer(2) NOT NULL DEFAULT 0 COMMENT '0: unaccept;1: accept',
    `number`       integer(20) NOT NULL DEFAULT 0 COMMENT 'number',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `catalogs`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `name`  varchar(128) NOT NULL COMMENT 'Name',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

ALTER TABLE `code_price` ADD catalog_id INTEGER ( 2 ) NOT NULL DEFAULT 1;
ALTER TABLE `client_guarantee` ADD unit INTEGER( 2 ) NOT NULL DEFAULT 1;

CREATE TABLE IF NOT EXISTS `recruits`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `content`    text NOT NULL COMMENT 'content',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `policy`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `content`    text NOT NULL COMMENT 'content',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `files`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `title`     VARCHAR(100) NOT NULL COMMENT 'content',
    `image1`     VARCHAR(512) COMMENT 'image1',
    `image2`     VARCHAR(512) COMMENT 'image2',
    `image3`     VARCHAR(512) COMMENT 'image3',
    `image4`     VARCHAR(512) COMMENT 'image4',
    `is_deleted` tinyint(2)   NOT NULL DEFAULT 0 COMMENT '0: chưa xóa, 1 đã xóa',
    `created_by` int(10)      NOT NULL COMMENT 'Người tạo',
    `updated_by` int(10)               DEFAULT NULL COMMENT 'Người cập nhập',
    `created_at` timestamp    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày tạo',
    `updated_at` timestamp    NULL     DEFAULT NULL COMMENT 'Ngày cập nhập',
    PRIMARY KEY (`id`)
);

