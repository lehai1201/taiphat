<?php

namespace App\Http\Controllers;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Meta\MetaRepository;
use Illuminate\Support\Facades\View;

class FrontendController extends Controller
{
    private $meta;
    private $category;

    public function __construct()
    {
        $this->meta = new MetaRepository();
        $this->category = new CategoryRepository();

        $metaHeader = $this->meta->getDataItem(['type' => 0], '', '', ['id' => 'desc']);
        $metaFooter = $this->meta->getDataItem(['type' => 1], '', '', ['id' => 'desc']);
        $categories = $this->category->getDataItem(['is_deleted' => NO_DELETED], '', '', ['id' => 'desc']);
        View::share('categories', $categories);
        View::share('metaHeader', $metaHeader);
        View::share('metaFooter', $metaFooter);
    }

}
