<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;

class LoginController extends Controller
{
    //

    public function login()
    {
        return view('backend.login');
    }

    public function postLogin(Request $request)
    {
        $remember=$request->get('remember');
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ],
            [
                'username.required' => ' Tên tài khoản bắt buộc phải nhập',
                'password.required' => ' Mật khẩu bắt buộc phải nhâp '
            ]);
        if ($validator->fails()) {
            return  $validator->errors()->first();
        }
        $username=$request->get('username');
        $password=$request->get('password');
        if (Auth::attempt(['username'=>$username,'password'=>$password,'is_deleted'=>NO_DELETED],$remember)) {
            return redirect(route('admin'));
        } else {
            return redirect(route('login'))->with('error', 'Sai thông tin tài khoản hoặc mật khẩu');
        }
    }

    public function userRegister(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'fullname'=>'required',
            'email'=>'required|email|unique:users',
            'address'=>'required',
            'username'=>'required|unique:users',
            'password'=>'required'
        ],
            [
                'fullname.required'=>'Tên bắt buộc phải nhập',
                'email.required'=>'Email bắt buộc phải nhập',
                'email.email'=>'Phải nhập định dạng email',
                'email.unique'=>'Email đã tồn tại',
                'username.required'=>'Tên đăng nhập không được để trống',
                'username.unique'=>'Tên đăng nhập đã tồn tại',
                'address.required'=>'Địa chỉ bắt buộc phải nhập',
                'password.required'=>'Mật khẩu bắt buộc phải nhập'
            ]);
        if ($validator->fails()) {
            return $validator->errors()->first();
        }
        $data_register = array();
        $data_register['name'] = $request->get('fullname');
        $data_register['address'] = $request->get('address');
        $data_register['email'] = $request->get('email');
        $data_register['username'] = $request->get('username');
        $data_register['password'] = bcrypt($request->get('password'));
        $user = new User();
        $user::create($data_register);
        return 'success';
    }
    public function command(){
        Artisan::call('optimize:clear');
        Artisan::call('config:clear');
        Artisan::call('config:cache');
        dd("thanh cong");
    }
}
