<?php

namespace App\Http\Controllers\Frontend\CongThuc;


use App\Http\Controllers\FrontendController;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CongthucController extends FrontendController
{
    private $product;

    public function __construct()
    {
        parent::__construct();
        $this->product = new ProductRepository();
    }

    public function congthuc()
    {
        return view('taiphat.congthuc.congthuc');
    }

    public function caculator(Request $request)
    {
        $request->validate([
            'dientich' => 'required|numeric|min:1',
            'san' => 'required|numeric|min:1',
            'tang' => 'required|numeric|min:1',
        ],
            [
                'dientich.required' => 'Diện tích bắt buộc phải nhập',
                'dientich.numeric' => 'Diện tích phải nhập kiểu số',
                'dientich.min' => 'Diện tích nhỏ nhất là 1',
                'san.required' => 'Diện tích bắt buộc phải nhập',
                'san.numeric' => 'Diện tích phải nhập kiểu số',
                'san.min' => 'Diện tích nhỏ nhất là 1',
                'tang.required' => 'Diện tích bắt buộc phải nhập',
                'tang.numeric' => 'Diện tích phải nhập kiểu số',
                'tang.min' => 'Diện tích nhỏ nhất là 1',
            ]);
        $dientich = $request->dientich;
        $san = $request->san;
        $tang = $request->tang;
        $ketqua = $dientich * $san * $tang;
        $products=$this->product->caculator($ketqua);
//        dd($products);
        return view('taiphat.congthuc.ketqua', compact('ketqua','products'));
    }
}
