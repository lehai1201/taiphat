<?php

namespace App\Http\Controllers\Frontend\DaiLi;

use App\Http\Controllers\FrontendController;
use App\Jobs\sendMailRegistration;
use App\Mail\sendMail;
use App\Repositories\Agency\AgencyRepository;
use App\Repositories\Registration\RegistrationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class DailiController extends FrontendController
{
    private $registration;
    private $agency;
    public function __construct()
    {
        parent::__construct();
        $this->registration=new RegistrationRepository();
        $this->agency=new AgencyRepository();
    }
    public function index(){
        $data_agencies=$this->agency->getDataItem(['is_deleted'=>NO_DELETED],'','',['id'=>'desc']);
        list($norland, $central, $south) = $this->agency->getArea();

        return view('taiphat.daily.daili',compact('data_agencies', 'norland', 'central', 'south'));
    }
    public function loadMap(Request $request){
        $id=$request->id;
        $agency=$this->agency->getDataById($id,'','');
        return $agency->code_map;
    }
    public function dangki(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email',
            'phone'=>'required|regex:/(0)[0-9]{9}/',
            'address'=>'required'
        ],
            [
                'name.required'=>'Tên bắt buộc phải nhập',
                'email.required'=>'Email bắt buộc phải nhập',
                'email.email'=>'Phải nhập định dạng Email',
                'phone.required'=>'Số điện thoại bắt buộc phải nhập',
                'phone.regex'=>'Số điện thoại bắt đầu bằng chữ số 0 và có 10 chữ số',
                'address.required'=>'Địa chỉ bắt buộc phải nhập'
            ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $data_create=array(
            'name'=>$request->get('name'),
            'email'=>$request->get('email'),
            'phone'=>$request->get('phone'),
            'type'=>IS_DAILI,
            'address'=>$request->get('address'),
            'created_by'=>1,
        );
        try{
            sendMailRegistration::dispatch($data_create)->delay(10);
            Log::info('MAIL_SUCCESS',['content'=>'Gui mail thanh cong']);
            $this->registration->create($data_create);
        }
        catch (\Exception $e){
            Log::warning('MAIL_ERROR',['error'=>$e->getMessage()]);
        }
        return 'success';
    }
}
