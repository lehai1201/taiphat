<?php

namespace App\Http\Controllers\Frontend\Policy;

use App\Http\Controllers\FrontendController;
use App\Repositories\PolicyRepository;

class PolicyController extends FrontendController
{
    private $policyRepository;

    public function __construct(PolicyRepository $policyRepository)
    {
        parent::__construct();
        $this->policyRepository = $policyRepository;
    }

    public function index()
    {
        $data = $this->policyRepository->getFirstData();

        return view('taiphat.policy.index', compact('data'));
    }

}
