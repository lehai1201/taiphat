<?php

namespace App\Http\Controllers\Frontend\Guarantee;

use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\GuaranteeRequest;
use App\Http\Requests\RegisterGuaranteeRequest;
use App\Jobs\sendMailGuaranteeJob;
use App\Jobs\SendMailQueue;
use App\Repositories\Agency\AgencyRepository;
use App\Repositories\Client\ClientRepository;
use App\Repositories\ClientGuarantee\ClientGuaranteeRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class GuaranteeController extends FrontendController
{
    protected $clientRepository;
    protected $productRepository;
    protected $agencyRepository;
    protected $clientGuaranteeRepository;


    public function __construct(ClientRepository $clientRepository,
                                ProductRepository $productRepository,
                                AgencyRepository $agencyRepository,
                                ClientGuaranteeRepository $clientGuaranteeRepository
    )
    {
        parent::__construct();
        $this->clientRepository = $clientRepository;
        $this->productRepository = $productRepository;
        $this->agencyRepository = $agencyRepository;
        $this->clientGuaranteeRepository = $clientGuaranteeRepository;
    }

    public function index()
    {
        return view('taiphat.guarantee.index');
    }

    public function add(GuaranteeRequest $request)
    {
        $params = $request->only(['name', 'phone', 'email', 'address']);
        $params['created_by'] = 0;

        DB::beginTransaction();
        try {
            $this->clientRepository->create($params);
            DB::commit();

            return redirect(route('guarantee.result',['phone' => $request->get('phone')]))->with('msg_success', 'Đã thêm thành công');
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
            return redirect(route('guarantee.index'))->with('msg_error', 'Đã có lỗi xảy ra');
        }
    }

    public function search()
    {
        return view('taiphat.guarantee.search');
    }

    public function result()
    {
        $phone = request()->get('phone');

        if(empty($phone)){
            return redirect(route('guarantee.index'))->with('msg_error', 'Đã có lỗi xảy ra');
        }

        $dataClient = $this->clientRepository->getFirstData(['phone' => $phone], '', '');

        if(empty($dataClient)){
            return redirect(route('guarantee.index'))->with('msg_error', 'Không tìm thấy kết quả');
        }

        $dataProduct = $this->productRepository->getListForFrontend($dataClient->id);
        $dataAgency = $this->agencyRepository->getAreaFrontend();

        $dataBooking = collect();
        if(!empty($dataClient)){
            $dataBooking = $this->clientGuaranteeRepository->getData($dataClient->id);
        }

        return view('taiphat.guarantee.result', compact('dataClient', 'dataProduct', 'dataAgency', 'dataBooking'));
    }

    public function register(RegisterGuaranteeRequest $request)
    {
        $params = $request->all();
        $clientId = data_get($params, 'client_id');
        $products = data_get($params, 'product_id');
        $numbers = data_get($params, 'number');
        $units = data_get($params, 'unit');

        if (empty($params) || empty($clientId) || empty($products) || empty($numbers)) {
            return redirect(route('guarantee.index'))->with('msg_error', 'Đã có lỗi xảy ra');
        }

        $dataClient = $this->clientRepository->getFirstData(['id' => $clientId]);

        if (empty($params) || empty($clientId)) {
            return redirect(route('guarantee.index'))->with('msg_error', 'Đã có lỗi xảy ra');
        }

        DB::beginTransaction();
        try {
            foreach ($products as $key=>$value){
                $dataCreate = [
                    'client_id' => data_get($params, 'client_id'),
                    'product_id' => $value,
                    'agency_id' => data_get($params, 'agency_id'),
                    'number' => data_get($numbers, $key),
                    'unit' => data_get($units, $key),
                    'created_by' => 0
                ];
                $this->clientGuaranteeRepository->create($dataCreate);
            }

            DB::commit();

            sendMailGuaranteeJob::dispatch($dataClient)->delay(5);

            return redirect(route('guarantee.result', ['phone' => $dataClient->phone]))->with('msg_success', 'Đăng ký thành công. Phiếu bảo hành của bạn đang chờ quản trị xác minh.');
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
            return redirect(route('guarantee.index'))->with('msg_error', 'Đã có lỗi xảy ra');
        }
    }

    public function request()
    {
        return view('taiphat.guarantee.request');
    }

    public function policy()
    {
        return view('taiphat.guarantee.policy');
    }
}
