<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Repositories\FileRepository;

class FileController extends FrontendController
{
    private $fileRepository;

    public function __construct(FileRepository $fileRepository)
    {
        parent::__construct();
        $this->fileRepository = $fileRepository;
    }

    public function index()
    {
        $data = $this->fileRepository->getFirstData();

        return view('taiphat.file.index', compact('data'));
    }

    public function view()
    {
        $file = request()->get('file');
        header("Content-Length: " . filesize($file));
        header("Content-type: application/pdf");
        header("Content-disposition: inline;filename=" . asset($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        $filepath = readfile($file);
    }
}
