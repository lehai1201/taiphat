<?php

namespace App\Http\Controllers\Frontend\Tuvan;

use App\Http\Controllers\FrontendController;
use App\Repositories\Post\PostRepository;
use App\Repositories\Registration\RegistrationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TuvanController extends FrontendController
{
    private $post;
    private $registration;
    public function __construct()
    {
        parent::__construct();
        $this->post=new PostRepository();
        $this->registration=new RegistrationRepository();
    }
    public function index(){
        $posts=$this->post->getDataItem(['is_deleted'=>NO_DELETED,'type'=>IS_ADVISOTY],'','',['id'=>'desc']);
        return view('taiphat.tuvan.chuyengia',compact('posts'));
    }
    public function detail($id){
        $post=$this->post->getDataById($id,'user','');
        return view('taiphat.tuvan.chitiettuvan',compact('post'));
    }
    public function dangky(){
        return view('taiphat.tuvan.dangky');
    }
    public function postDangky(Request $request){
        $id=Auth::id();
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email',
            'phone'=>'required|regex:/(0)[0-9]{9}/',
            'address'=>'required'
        ],
            [
                'name.required'=>'Tên bắt buộc phải nhập',
                'email.required'=>'Email bắt buộc phải nhập',
                'email.email'=>'Phải nhập định dạng Email',
                'phone.required'=>'Số điện thoại bắt buộc phải nhập',
                'phone.regex'=>'Số điện thoại bắt đầu bằng chữ số 0 và có 10 chữ số',
                'address.required'=>'Địa chỉ bắt buộc phải nhập'
            ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $data_create=array(
            'name'=>$request->get('name'),
            'email'=>$request->get('email'),
            'phone'=>$request->get('phone'),
            'type'=>IS_TUVAN,
            'address'=>$request->get('address'),
            'created_by'=>$id,
        );
        $this->registration->create($data_create);
        return 'success';
    }
}
