<?php

namespace App\Http\Controllers\Frontend\TuyenDung;

use App\Http\Controllers\FrontendController;
use App\Repositories\RecruitRepository;

class RecruitController extends FrontendController
{
    private $recruitRepository;

    public function __construct(RecruitRepository $recruitRepository)
    {
        parent::__construct();
        $this->recruitRepository = $recruitRepository;
    }

    public function index()
    {
        $data = $this->recruitRepository->getFirstData();

        return view('taiphat.recruit.index', compact('data'));
    }

}
