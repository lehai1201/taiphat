<?php

namespace App\Http\Controllers\Frontend\Post;

use App\Http\Controllers\FrontendController;
use App\Repositories\Post\PostRepository;
use App\Repositories\PostCheck\PostCheckRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends FrontendController
{
    private $post_check;
    private $post;
    public function __construct()
    {
        parent::__construct();
        $this->post=new PostRepository();
        $this->post_check=new PostCheckRepository();
    }
    public function index(){
        $post_check=$this->getPostCheck();
        $posts=$this->post->getDataItem(['is_deleted'=>NO_DELETED,'type'=>IS_POST],'user','',['id'=>'desc']);
        return view('taiphat.tintuc.tintuc',compact('posts','post_check'));
    }
    public function detail($title,$id){
        $post=$this->post->getDataById($id,'user','');
        $post_same=$this->post->getPostSame($id,$post->type);
        return view('taiphat.tintuc.chitiet',compact('post','post_same'));
    }
    public function event(){
        $post_check=$this->getPostCheck();
        $events=$this->post->getDataItem(['is_deleted'=>NO_DELETED,'type'=>IS_EVENT],'user','',['id'=>'desc']);
        return view('taiphat.tintuc.sukien',compact('events','post_check'));

    }
    public function getPostCheck(){
        $post_check=$this->post_check->getFirstData('','','');
        $post_selected=$this->post->getDataById($post_check->post_id,'','');
        return $post_selected;
    }
}
