<?php

namespace App\Http\Controllers\Frontend\Product;

use App\Http\Controllers\FrontendController;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends FrontendController
{
    private $product;
    private $category;

    public function __construct()
    {
        parent::__construct();
        $this->product = new ProductRepository();
        $this->category = new CategoryRepository();
    }

    public function index()
    {
        $categories = $this->category->getDataItem(['is_deleted' => NO_DELETED], '', '', ['id' => 'desc']);
        $products = $this->product->getDataItem(['is_deleted' => NO_DELETED], ['category', 'codeprice'], 6,
            ['id' => 'desc']);
        return view('taiphat.sanpham.sanpham', compact('categories', 'products'));
    }

    public function detail($title, $id)
    {
        $product = $this->product->getDataById($id, ['category', 'codeprice'], '');
        if ($product == null) {
            abort(404);
        }
        $product_same = $this->product->getProductSame($id, $product->category_id);
        $qrlink = route('detail.product', ['product' => $title, 'id' => $id]);
        return view('taiphat.sanpham.chitiet', compact('product', 'qrlink', 'product_same'));
    }

    public function category($post, $category_id)
    {
        $categories = $this->category->getDataItem(['is_deleted' => NO_DELETED], '', '', ['id' => 'desc']);
        $products = $this->product->getDataItem(['is_deleted' => NO_DELETED, 'category_id' => $category_id],
            ['category', 'codeprice'], 10, '');
        return view('taiphat.sanpham.danhmuc', compact('products', 'category_id', 'categories'));
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $products = $this->product->searchProduct($search);
        return view('taiphat.sanpham.list_product', compact('products'));
    }
}
