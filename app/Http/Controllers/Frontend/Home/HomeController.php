<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\FrontendController;
use App\Repositories\Construct\ConstructRepository;
use App\Repositories\FileRepository;
use App\Repositories\Post\PostRepository;
use App\Repositories\PostCheck\PostCheckRepository;
use App\Repositories\PriceRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Rate\RateRepository;
use App\Repositories\Registration\RegistrationRepository;
use App\Repositories\Video\VideoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends FrontendController
{
    private $product;
    private $contructer;
    private $post_check;
    private $post;
    private $registration;
    private $video;
    private $rate;
    private $price;
    private $file;
    public function __construct()
    {
        parent::__construct();
        $this->product=new ProductRepository();
        $this->contructer=new ConstructRepository();
        $this->post_check=new PostCheckRepository();
        $this->post=new PostRepository();
        $this->registration=new RegistrationRepository();
        $this->video = new VideoRepository();
        $this->rate = new RateRepository();
        $this->price = new PriceRepository();
        $this->file = new FileRepository();
    }

    public function index()
    {
        $post_check = $this->post_check->getFirstData('', '', '');
        $data_post_check = $this->post->getDataById($post_check->post_id, '', '');
        //Lấy ds công trình tiêu biểu
        $products_nessan = $this->product->getProductHome(IS_NESSAN);
        $chairs = $this->product->getProductHome(IS_CHAIR);
        $constructs = $this->contructer->getRecordData(['is_deleted' => NO_DELETED], '', 6, ['id' => 'desc']);
        //Lấy ds video
        $videos = $this->video->getRecordData(['is_deleted' => NO_DELETED], '', 6, ['id' => 'desc']);
        //lấy ds đánh giá
        $rates = $this->rate->getRecordData(['is_deleted' => NO_DELETED], '', 6, ['sort' => 'asc']);

        $files = $this->file->getFirstData();
        return view('taiphat.home.home', compact('products_nessan', 'constructs', 'data_post_check', 'videos','rates', 'files', 'chairs'));
    }

    public function customerRegister(Request $request){
        $user=Auth::id();
        $validator=Validator::make($request->all(),[
            'email'=>'email'
        ],[
            'email.email'=>'Phải nhập định dạng Email'
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $email=$request->email;
        $data_created=array(
            'email'=>$email,
            'type'=>IS_NHANTIN,
            'created_by'=>$user
        );
        $this->registration->create($data_created);
        return 'success';
    }

    public function priceList()
    {
        $listPrice = $this->price->getDataItem([], [], '', ['sort' => 'asc', 'id' => 'desc']);

        return view('taiphat.home.price', compact('listPrice'));
    }

    public function gioithieu(){
        return view('taiphat.gioithieu.gioithieu');
    }
}
