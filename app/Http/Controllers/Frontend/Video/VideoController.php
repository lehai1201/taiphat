<?php

namespace App\Http\Controllers\Frontend\Video;

use App\Http\Controllers\FrontendController;
use App\Repositories\Video\VideoRepository;
use App\Http\Controllers\Controller;

class VideoController extends FrontendController
{
    protected $video;

    public function __construct()
    {
        parent::__construct();
        $this->video = new VideoRepository();
    }

    public function index()
    {
        $videos = $this->video->getDataItem([], [], 10, ['id' => 'desc']);

        return view('taiphat.video.index', compact('videos'));
    }
}
