<?php

namespace App\Http\Controllers\Frontend\LienHe;

use App\Http\Controllers\FrontendController;
use App\Repositories\Agency\AgencyRepository;
use App\Repositories\Registration\RegistrationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LienHeController extends FrontendController
{
    private $registration;
    private $agency;
    public function __construct()
    {
        parent::__construct();
        $this->registration=new RegistrationRepository();
        $this->agency=new AgencyRepository();
    }
    public function index(){
        list($norland, $central, $south) = $this->agency->getArea();

        return view('taiphat.lienhe.lienhe',compact('norland', 'central', 'south'));
    }
    public function sendInfo(Request $request){
        $id=Auth::id();
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email',
            'phone'=>'required|regex:/(0)[0-9]{9}/'
        ],
            [
                'name.required'=>'Tên bắt buộc phải nhập',
                'email.required'=>'Email bắt buộc phải nhập',
                'email.email'=>'Phải nhập định dạng Email',
                'phone.required'=>'Số điện thoại bắt buộc phải nhập',
                'phone.regex'=>'Số điện thoại bắt đầu bằng chữ số 0 và có 10 chữ số',
            ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $data_create=array(
            'name'=>$request->get('name'),
            'email'=>$request->get('email'),
            'phone'=>$request->get('phone'),
            'type'=>IS_LIENHE,
            'note'=>$request->get('content'),
            'created_by'=>$id,
        );
        $this->registration->create($data_create);
        return 'success';
    }
}
