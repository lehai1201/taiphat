<?php

namespace App\Http\Controllers\Frontend\Color;

use App\Http\Controllers\FrontendController;
use App\Repositories\Color\ColorRepository;
use App\Repositories\Construct\ConstructRepository;

class ColorController extends FrontendController
{
    private $color;
    private $construct;
    public function __construct()
    {
        parent::__construct();
        $this->color=new ColorRepository();
        $this->construct=new ConstructRepository();
    }
    public function bangmau(){
        $colors=$this->color->getDataItem('','',21,['id'=>'desc']);
        return view('taiphat.mausac.bangmau',compact('colors'));
    }
    public function camhung(){
        return view('taiphat.mausac.camhung');
    }
    public function congtrinh(){
        $construct=$this->construct->getDataItem(['is_deleted'=>NO_DELETED],'','',['id'=>'desc']);
        return view('taiphat.mausac.congtrinh',compact('construct'));
    }
}
