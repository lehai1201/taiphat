<?php

namespace App\Http\Controllers;

use App\Repositories\Registration\RegistrationRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class BaseController extends Controller
{
    private $registration;
    private $user_repository;

    public function __construct()
    {
        $this->user_repository=new UserRepository();
        $this->registration=new RegistrationRepository();
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $id = $this->getAuthId();
            $data_user_login = $this->user_repository->getDataById($id, 'role', '');
            $daily=$this->registration->getDataItem(['type'=>IS_DAILI,'is_new'=>IS_NEW],'','','');
            $tuvan=$this->registration->getDataItem(['type'=>IS_TUVAN,'is_new'=>IS_NEW],'','','');
            $lienhe=$this->registration->getDataItem(['type'=>IS_LIENHE,'is_new'=>IS_NEW],'','','');
            $nhantin=$this->registration->getDataItem(['type'=>IS_NHANTIN,'is_new'=>IS_NEW],'','','');
//           dd($daily->count(),$tuvan->count(),$lienhe->count(),$nhantin->count());
            View::share('data_user_login', $data_user_login);
            View::share('sldaily', $daily->count());
            View::share('sltuvan', $tuvan->count());
            View::share('sllienhe', $lienhe->count());
            View::share('slnhantin',$nhantin->count());
            return $next($request);
        });
    }

    public function getAuthId()
    {
        return Auth::user()->id;
    }
}
