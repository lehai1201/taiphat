<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Repositories\User\UserRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Mail;
use Hash;
class UserController extends BaseController
{
    //
    protected $user;
    public function __construct()
    {
        parent::__construct();
        $this->user=new UserRepository();
    }

    public function viewProfile($id){
        $data_user_profile=$this->user->getDataById($id,'role','');
        return view('backend.profile',compact('data_user_profile'));
    }

    public function editPassword(Request $request,$id){
        $data_input=$request->all();
        $data_login=$this->user->getDataById($id,'','');
        $validator=Validator::make($request->all(),[
            'old_password'=>'required',
            'password'=>'required|confirmed',
            'password_confirmation'=>'required'
        ],
            [
                'old_password.required'=>'Mật khẩu cũ không được để trống',
                'password.required'=>'Mật khẩu mới không được để trống',
                'password.confirmed'=>'Mật khẩu mới không trùng',
            ]);
        if($validator->fails()){
            return respone();
        }
        if(Hash::check($data_input['old_password'],$data_login->password)){
            $data_input['password']=bcrypt($data_input['password']);
            $this->user->update($id,['password'=>$data_input['password']]);
            $data_user_profile=$this->user->getDataById($id,'','');
            return view('backend.includes.data_user_profile',compact('data_user_profile'));
        }
        $data_user_profile=$this->user->getDataById($id,'','');
        return 'error';
    }
    public function editProfile(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'phone'=>'required',
            'address'=>'required'
        ],
            [
                'name.required'=>'Tên bắt buộc phải nhập',
                'phone.required'=>'Số điện thoại bắt buộc phải nhập',
                'address.required'=>'Địa chỉ bắt buộc phải nhập',
            ]);
        if($validator->fails()){
            return back()->with('error', $validator->errors()->first());
        }
        $data_update=Input::all();
        $this->user->update($id,$data_update);
        $data_user_profile=$this->user->getDataById($id,'role','');
        return view('backend.includes.data_user_profile',compact('data_user_profile'));
    }

    public function editAvatar(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'avatar'=>'image|required|mimes:jpg,jpeg|max:500'
        ],[
            'avatar.required'=>'Chọn ít nhất 1 ảnh',
            'avatar.image'=>'Chỉ được chọn ảnh',
            'avatar.mimes'=>'Phải chọn đuôi jpg,png',
            'avatar.max'=>'Kích thước tối đa 500kb'
        ]);
        if($validator->fails()){
            return back()->with('error', $validator->errors()->first());
        }
        $data_login=$this->user->getDataById($id,'','');
        if(!$data_login->avatar==null){
            if(file_exists('avatar/'.$data_login->avatar)){
                unlink('avatar/'.$data_login->avatar);
            }
        }
        $avatar = time() . Input::file('avatar')->getClientOriginalName();
        Input::file('avatar')->move("avatar/", $avatar);
       $this->user->update($id,['avatar'=>$avatar]);
       return back()->with('message','Thay đổi thành công');
    }

    // subject: Tiêu đề gửi
    public function sendEmail(){
        $param=array(
            'title'=>'Tiêu đề',
            'name'=>'Lê hai'
        );
        Mail::send('backend.Mail.MailTest',$param, function($message){
            $message->to('ptit.lehai@gmail.com', 'Lehai')->subject('Hello việt nam!');
        });
        return back()->with('message','Gửi mail thành công');
    }
}
