<?php

namespace App\Http\Controllers\Color;

use App\Http\Controllers\BaseController;
use App\Repositories\Color\ColorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ColorController extends BaseController
{
    //
    protected $color;
    public function __construct()
    {
        parent::__construct();
        $this->color=new ColorRepository();
    }
    public function index(){
        $data_color=$this->color->getDataItem(['is_deleted'=>NO_DELETED],'',12,['id'=>'desc']);
        return view('backend.colors.index',compact('data_color'));
    }
    public function add(Request $request){
        $user=$this->getAuthId();
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'color_code'=>'required|min:4'
        ],[
            'name.required'=>'Tên bắt buộc phải nhập',
            'color_code.required'=>'Mã màu không được để trống',
            'color_code.min'=>'Mã màu phảiít nhất 4 kí tự',
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
       $code=$request->color_code;
        $data_created=array();
        $data_created['name']=$request->name;
        $data_created['color_code']=$code;
        $data_created['created_by']=$user;
        $data_created['updated_by']=$user;
        $this->color->create($data_created);
        return 'success';
    }
    public function deleted(Request $request){
        $id=$request->get('id');
        $color=$this->color->getDataById($id);
        $color->delete();
        return 'success';
    }
}
