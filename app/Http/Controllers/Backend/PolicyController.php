<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseController;
use App\Repositories\PolicyRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PolicyController extends BaseController
{
    public $policyRepository;

    public function __construct(PolicyRepository $policyRepository)
    {
        parent::__construct();
        $this->policyRepository = $policyRepository;
    }

    public function index()
    {
        $data = $this->policyRepository->getFirstData();

        return view('backend.policy.index', compact('data'));
    }

    public function add(Request $request)
    {
        $userId = Auth::id();
        $id = $request->get('id');
        $validator = Validator::make($request->all(), [
            'content' => 'required',
        ], [
            'content.required' => 'Nội dung bắt buộc phải nhập',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors()->first());
        }

        if (!empty($id)) {
            $dataUpdate = array(
                'content' => $request->get('content'),
            );
            $this->policyRepository->update($id, $dataUpdate);
            return redirect(route('backend.policy.index'))->with('messeger', 'Đã sửa thành công');
        }
        $data_created = array(
            'content' => $request->get('content'),
            'created_at' => Carbon::now(),
            'created_by' => $userId
        );
        $this->policyRepository->create($data_created);
        return redirect(route('backend.policy.index'))->with('messeger', 'Đã thêm thành công');
    }
}
