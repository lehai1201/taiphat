<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseController;
use App\Repositories\Post\PostRepository;
use App\Repositories\PostCheck\PostCheckRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Registration\RegistrationRepository;
use App\Repositories\User\UserRepository;

class HomeController extends BaseController
{
    private $product;
    private $post;
    private $registration;
    private $user;
    private $post_check;
    public function __construct(){
        parent::__construct();
        $this->product=new ProductRepository();
        $this->post=new PostRepository();
        $this->registration=new RegistrationRepository();
        $this->user=new UserRepository();
    }

    public function home()
    {

        $products=$this->product->getDataItem(['is_deleted'=>NO_DELETED],'','','');
        $posts=$this->post->getDataItem(['is_deleted'=>NO_DELETED],'','','');
        $registrations=$this->registration->getDataItem('','','','');
        $users=$this->user->getDataUserNotSys(['is_deleted'=>NO_DELETED],'','','');
        return view('backend.Home',compact('products','posts','registrations','users'));
    }
}
