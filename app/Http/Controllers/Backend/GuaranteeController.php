<?php

namespace App\Http\Controllers\Backend;

use App\Exports\GuaranteeExport;
use App\Exports\UsersExport;
use App\Http\Controllers\BaseController;
use App\Repositories\ClientGuarantee\ClientGuaranteeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;


class GuaranteeController extends BaseController
{
    private $clientGuarantee;

    public function __construct()
    {
        parent::__construct();
        $this->clientGuarantee = new ClientGuaranteeRepository();
    }

    public function index()
    {
        $dataGuarantee = $this->clientGuarantee->getDataForBackend();

        return view('backend.guarantee.index', compact('dataGuarantee'));
    }

    public function changeStatus(Request $request)
    {
        $params = $request->all();
        $listId = data_get($params, 'ids');
        $status = data_get($params, 'type');

        $result = ['success' => false];

        if (empty($listId) || empty($status)) {
            return json_encode($result);
        }

        DB::beginTransaction();
        try {
            $this->clientGuarantee->updateStatus($listId, $status);
            DB::commit();

            $result = ['success' => true];
            return json_encode($result);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error($exception->getMessage());
            return json_encode($result);
        }
    }

    public function delete(Request $request)
    {
        $params = $request->all();
        $listId = data_get($params, 'ids');

        $result = ['success' => false];

        if (empty($listId)) {
            return json_encode($result);
        }

        DB::beginTransaction();
        try {
            $this->clientGuarantee->deleteData($listId);
            DB::commit();

            $result = ['success' => true];
            return json_encode($result);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error($exception->getMessage());
            return json_encode($result);
        }
    }

    public function export(Request $request)
    {
        $params = $request->all();
        $output = Excel::download(new GuaranteeExport($params), 'bao-hanh.xlsx');

        return $output;
    }
}
