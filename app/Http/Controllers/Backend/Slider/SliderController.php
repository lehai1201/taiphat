<?php

namespace App\Http\Controllers\Backend\Slider;

use App\Http\Controllers\BaseController;
use App\Repositories\Slider\SliderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class SliderController extends BaseController
{
    protected $slider;
    public function __construct()
    {
        parent::__construct();
        $this->slider=new SliderRepository();
    }

    public function add(Request $request){
        $user=$this->getAuthId();
        $validator=Validator::make($request->all(),[
            'images_slider'=>'required|image|mimes:jpeg,png,jpg|max:5100'
        ],[
            'images_slider.required'=>'Ảnh bắt buộc phải chọn',
            'images_slider.image'=>'Chỉ được chọn ảnh',
            'images_slider.mimes'=>'Ảnh phải ở dạng jpeg,png hoặc jpg',
            'images_slider.max'=>'Dung lượng tối đa 5MB',
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $image=time().Input::file('images_slider')->getClientOriginalName();
        Input::file('images_slider')->move("images/slider/", $image);
        $data_created=array();
        $data_created['images']=$image;
        $data_created['created_by']=$user;
        $data_created['updated_by']=$user;
        $this->slider->create($data_created);
        return 'success';
    }
    public function deleted(Request $request){
        $id=$request->id;
        $slider=$this->slider->getDataById($id);
        if (file_exists('images/slider/' . $slider->images)) {
            unlink('images/slider/' . $slider->images);
        }
        $this->slider->deleteRecord($id);
        return "Đã xóa thành công";
    }
}
