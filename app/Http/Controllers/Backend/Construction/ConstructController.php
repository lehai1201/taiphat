<?php

namespace App\Http\Controllers\Backend\Construction;

use App\Http\Controllers\BaseController;
use App\Repositories\Construct\ConstructRepository;
use App\Repositories\Slider\SliderRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ConstructController extends BaseController
{
    //
    private $construct;
    private $slider;
    public function __construct()
    {
        parent::__construct();
        $this->construct = new ConstructRepository();
        $this->slider=new SliderRepository();
    }

    public function index()
    {
        $data_all_slider=$this->slider->getDataItem(['is_deleted'=>NO_DELETED],'','',['id'=>'desc']);
        $data_all_construct = $this->construct->getDataItem(['is_deleted' => NO_DELETED], '', '', ['id' => 'dessc']);
        return view('backend.construct.index', compact('data_all_construct','data_all_slider'));
    }

    public function add(Request $request)
    {
        $user_id = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:100',
            'images_construct' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048'
        ], [
            'title.required' => 'Tiêu đề bắt buộc phải nhập',
            'title.max' => 'Tiêu đề tối đa 100 kí tự',
            'images_construct.required' => 'Ảnh bắt buộc phải nhập',
            'images_construct.image' => 'Phải chọn kiểu ảnh',
            'images_construct.mimes' => 'Phải chọn đuôi jpeg, png, jpg, svg',
            'images_construct.max' => 'Ảnh tối đa 2MB',

        ]);
        if ($validator->fails()) {
            return $validator->errors()->first();
        }
        $title = $request->input('title');
        $images = time() . Input::file('images_construct')->getClientOriginalName();
        Input::file('images_construct')->move("images/congtrinh/", $images);
        $data_create = array();
        $data_create['title'] = $title;
        $data_create['images'] = $images;
        $data_create['created_by'] = $user_id;
        $data_create['updated_by'] = $user_id;
        $data_create['created_at'] =  Carbon::now('Asia/Ho_Chi_Minh');
        $data_create['updated_at'] = Carbon::now('Asia/Ho_Chi_Minh');
        $this->construct->create($data_create);
        return 'success';
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $construct = $this->construct->getDataById($id, '', '');
        if (file_exists('images/congtrinh/' . $construct->images)) {
            unlink('images/congtrinh/' . $construct->images);
        }
        $this->construct->deleteRecord($id);
        return "Đã xóa thành công";
    }
}
