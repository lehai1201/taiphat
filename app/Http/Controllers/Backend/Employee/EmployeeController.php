<?php

namespace App\Http\Controllers\Backend\Employee;

use App\Exports\UsersExport;
use App\Http\Controllers\BaseController;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\User;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends BaseController
{
    private $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = new UserRepository();
    }

    public function list()
    {
        if(!Gate::allows('SYS')){
            return back()->with('notPermission','Bạn không có quyền truy cập');
        }
        $data_user_all = $this->user->getDataUserNotSys(['is_deleted' => NO_DELETED], 'role', 5, ['role_id' => 'asc']);
        return view('backend.Employee.index', compact('data_user_all'));
    }

    //thêm mới user
    public function ajaxAddUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'myname' => 'required',
            'email' => 'required|email|unique:users',
            'address' => 'required',
            'phone'=>'required',
            'username' => 'required|unique:users',
            'password' => 'required|confirmed'
        ],
            [
                'myname.required' => 'Tên bắt buộc phải nhập',
                'email.required' => 'Email bắt buộc phải nhập',
                'email.email' => 'Phải nhập định dạng Email',
                'email.unique' => 'Email đã tồn tại',
                'username.required' => 'Tên đăng nhập không được để trống',
                'username.unique' => 'Tên đăng nhập đã tồn tại',
                'address.required' => 'Địa chỉ bắt buộc phải nhập',
                'phone.required' => 'Số điện thoại bắt buộc phải nhập',
                'password.required' => 'Mật khẩu bắt buộc phải nhập',
                'password.confirmed' => 'Mật khẩu không trùng',
            ]);
        if ($validator->fails()) {
            return $validator->errors()->first();
        }
        $data_register = array();
        $data_register['name'] = $request->get('myname');
        $data_register['address'] = $request->get('address');
        $data_register['phone'] = $request->get('phone');
        $data_register['email'] = $request->get('email');
        $data_register['username'] = $request->get('username');
        $data_register['password'] = bcrypt($request->get('password'));
        $data_register['role_id'] = $request->get('role_id');
        $data_register['is_deleted'] = NO_DELETED;
        $this->user->create($data_register);
        return 'success';
    }

    //xóa mềm user
    public function ajaxDeleteUser(Request $request)
    {
        $id = $request->get('id');
        $data_update = array(
            'is_deleted' => IS_DELETED
        );
        $this->user->update($id, $data_update);
        return 'success';
    }

    //load modal edit
    public function ajaxLoadEditUser(Request $request)
    {
        $id = $request->get('id');
        $data_user = $this->user->getDataById($id, '', '');
        return view('backend.Employee.ajax_edit_user', compact('data_user'));
    }

    //thực hiện edit
    public function ajaxUpdateUser(Request $request)
    {
        $data_update = array();
        $data_all = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric'
        ],
            [
                'name.required' => 'Tên bắt buộc phải nhập',
                'address.required' => 'Địa chỉ bắt buộc phải nhâp ',
                'phone.required' => 'Số điện thoại bắt buộc phải nhập',
                'phone.numeric' => 'Số điện thoại chỉ được nhập kiểu số',
            ]);
        if ($validator->fails()) {
            return $validator->errors()->first();
        }
        if (!empty($data_all['is_change_password']) && ($data_all['is_change_password'] == IS_CHECKED)) {
            $validator_password = Validator::make($request->all(), [
                'password' => 'required|min:4'
            ], [
                'password.required' => 'Mật khẩu bắt buộc phải nhập',
                'password.min' => 'Mật khẩu phải có ít nhất 4 kí tự'
            ]);
            if ($validator_password->fails()) {
                return $validator_password->errors()->first();
            }
            $data_update['password'] = bcrypt($data_all['password']);
        }
        $data_update['name'] = $data_all['name'];
        $data_update['address'] = $data_all['address'];
        $data_update['phone'] = $data_all['phone'];
        $this->user->update($data_all['id'], $data_update);
        return 'success';
    }

    //Tìm kiếm
    public function ajaxSearch(Request $request)
    {
        $search = $request->get('search');
        $data_search = array('name' => $search);
        $data_user_all = $this->user->getDataBySearch(['is_deleted' => NO_DELETED], '',5, ['role_id'=>'asc'], $data_search);
        return view('backend.Employee.data_table_user', compact('data_user_all'));
    }

    //Excel
    public function exportExcel(Request $request){
        return Excel::download(new UsersExport(), 'DSnhanvien.xlsx');
    }
}
