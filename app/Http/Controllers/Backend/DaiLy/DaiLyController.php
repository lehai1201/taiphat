<?php

namespace App\Http\Controllers\backend\DaiLy;

use App\Http\Controllers\BaseController;
use App\Repositories\Agency\AgencyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DaiLyController extends BaseController
{
    private $agency;

    public function __construct()
    {
        parent::__construct();
        $this->agency = new AgencyRepository();
    }

    public function index()
    {
        $data_agencies = $this->agency->getDataItem(['is_deleted' => NO_DELETED], '', '', ['id' => 'desc']);
        return view('backend.DaiLy.index', compact('data_agencies'));
    }

    public function add(Request $request)
    {
        $id = Auth::id();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'numeric|required|regex:/(0)[0-9]{9}/',
            'address' => 'required',
            'map' => 'required'
        ], [
            'name.required' => 'Tên bắt buộc phải nhập',
            'phone.required' => 'Số điện thoại bắt buộc phải nhập',
            'phone.numeric' => 'Số điện thoại chỉ được nhập số',
            'phone.regex' => 'Số điện thoại phải bắt đầu bằng số 0 và có 10 chữ số',
            'address.required' => 'Địa chỉ bắt buộc phải nhập',
            'map.required' => 'Code Map bắt buộc phải nhập',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->first();
        }
        $data_created = array(
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'area' => $request->get('area'),
            'code_map' => $request->get('map'),
            'created_by' => $id
        );
        $this->agency->create($data_created);
        return 'success';
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $this->agency->deleteRecord($id);
        return 'success';
    }

    public function loadModal(Request $request)
    {
        $id = $request->id;
        $agency = $this->agency->getDataById($id, '', '');
        return view('backend.DaiLy.data_edit_agency', compact('agency'));
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'numeric|required|regex:/(0)[0-9]{9}/',
            'address' => 'required',
            'map' => 'required'
        ], [
            'name.required' => 'Tên bắt buộc phải nhập',
            'phone.required' => 'Số điện thoại bắt buộc phải nhập',
            'phone.numeric' => 'Số điện thoại chỉ được nhập số',
            'phone.regex' => 'Số điện thoại phải bắt đầu bằng số 0 và có 10 chữ số',
            'address.required' => 'Địa chỉ bắt buộc phải nhập',
            'map.required' => 'Code Map bắt buộc phải nhập',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->first();
        }
        $agency = $this->agency->getDataById($id, '', '');
        $data_update = array(
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'code_map' => $request->get('map'),
            'area' => $request->get('area')
        );
        $agency->update($data_update);
        return 'success';
    }
    public function search(Request $request){
        $search=$request->search;
        $data_agencies=$this->agency->getDataBySearch(['is_deleted'=>NO_DELETED],'','','',['name'=>$search]);
        return view('backend.DaiLy.list_data',compact('data_agencies'));
    }
}
