<?php

namespace App\Http\Controllers\Backend\Meta;

use App\Http\Controllers\BaseController;
use App\Repositories\Meta\MetaRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class MetaController extends BaseController
{
    private $metaRepository;

    public function __construct()
    {
        parent::__construct();
        $this->metaRepository = new MetaRepository();
    }

    public function index()
    {
        $listMeta = $this->metaRepository->getDataItem([], [], 10, ['id' => 'desc']);

        return view('backend.meta.index', compact('listMeta'));
    }

    public function create()
    {
        return view('backend.meta.create');
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        $validator = Validator::make($request->all(), [
            'tag' => 'required|check_tag',
            'type' => 'required',
        ], [
            'tag.required' => 'Thẻ không được để trống',
            'tag.check_tag' => 'Định dạng thẻ không chính xác',
            'type.required' => 'Vị trí không được để trống'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors()->all());
        }

        $data_created = array(
            'tag' => $request->get('tag'),
            'type' => $request->get('type'),
            'created_at' => Carbon::now(),
            'created_by' => $id
        );

        DB::beginTransaction();
        try {
            $this->metaRepository->create($data_created);
            DB::commit();

            return redirect(route('meta.index'))->with('success', 'Đã thêm thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('ERROR', ['CONTENT' => $exception->getMessage()]);

            return redirect(route('meta.index'))->with('errors', 'Lỗi hệ thống');
        }
    }

    public function edit($id)
    {
        $meta = $this->metaRepository->getDataById($id);
        if (empty($meta)) {
            return redirect(route('meta.index'))->with('errors', 'Dữ liệu đã bị xóa');

        }

        return view('backend.meta.edit', compact('meta'));
    }

    public function update($id)
    {
        $price = $this->metaRepository->getDataById($id);
        if (empty($price)) {
            return redirect(route('meta.index'))->with('error', 'Dữ liệu đã bị xóa');

        }

        $validator = Validator::make(request()->all(), [
            'tag' => 'required|check_tag',
            'type' => 'required',
        ], [
            'tag.required' => 'Thẻ không được để trống',
            'tag.check_tag' => 'Định dạng thẻ không chính xác',
            'type.required' => 'Vị trí không được để trống'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors()->all());
        }

        $dataUpdate = array(
            'tag' => request()->get('tag'),
            'type' => request()->get('type'),
            'created_at' => Carbon::now(),
            'created_by' => $id
        );

        DB::beginTransaction();
        try {
            $this->metaRepository->update($id, $dataUpdate);
            DB::commit();

            return redirect(route('meta.index'))->with('success', 'Đã sửa thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('ERROR', ['CONTENT' => $exception->getMessage()]);

            return redirect(route('meta.index'))->with('errors', 'Lỗi hệ thống');
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $price = $this->metaRepository->getDataById($id);

        if (empty($price)) {
            return 'Đã xóa thất bại';
        }

        $this->metaRepository->deleteRecord($id);
        return 'Đã xóa thành công';
    }
}
