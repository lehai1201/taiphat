<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Controllers\BaseController;
use App\Repositories\Catalog\CatalogRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\CodePrice\CodePriceRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends BaseController
{
    //
    private $category;
    private $code_price;
    private $catalogRepository;

    public function __construct()
    {
        parent::__construct();
        $this->category=new CategoryRepository();
        $this->code_price=new CodePriceRepository();
        $this->catalogRepository =  new CatalogRepository();
    }

    public function index(){
        $data_categories=$this->code_price->getDataItem('','category',10,['category_id'=>'desc','id'=>'desc']);
        $data_species=$this->category->getDataItem(['is_deleted'=>NO_DELETED],'products','',['id'=>'desc']);
        $dataCatalog =$this->catalogRepository->getDataItem(['is_deleted'=>NO_DELETED],'','',['id'=>'desc']);
        $id_dont_delete=array(IS_NESSAN,IS_ATINI,IS_MALLEND);


        return view('backend.category.index',compact('data_species','data_categories','id_dont_delete', 'dataCatalog'));
    }

    public function newSpecies(Request $request){
        $user_id=Auth::user()->id;
        $validator=Validator::make($request->all(),[
            'name'=>'required'
        ],[
            'name.required'=>'Tên không được để trống'
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $data_created=array();
        $data_created['name']=$request->get('name');
        $data_created['is_deleted']=NO_DELETED;
        $data_created['created_by']=$user_id;
        $data_created['updated_by']=$user_id;
        $this->category->create($data_created);
        return 'success';
    }

    public function addCatalog(Request $request){
        $user_id=Auth::user()->id;
        $validator=Validator::make($request->all(),[
            'name'=>'required'
        ],[
            'name.required'=>'Tên không được để trống'
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $data_created=array();
        $data_created['name']=$request->get('name');
        $data_created['is_deleted']=NO_DELETED;
        $data_created['created_by']=$user_id;
        $data_created['updated_by']=$user_id;
        $this->catalogRepository->create($data_created);
        return 'success';
    }

    public function newCategory(Request $request){
        $user_id=Auth::user()->id;
        $time=Carbon::now();
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'unit'=>'required',
            'kl'=>'required|numeric',
            'code'=>'required|unique:code_price',
            'price'=>'required|numeric'
        ],[
            'name.required'=>'Tên không được để trống',
            'kl.required'=>'Khối lượng không được để trống',
            'kl.numeric'=>'Khối lượng chỉ được nhập số',
            'unit.required'=>'Đơn vị không được để trống',
            'code.required'=>'Mã sản phẩm không được để trống',
            'code.unique'=>'Mã sản phẩm đã tồn tại',
            'price.required'=>'Giá không được để trống',
            'price.numeric'=>'Giá chỉ được nhập số'
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $data_created=array();
        $data_created['description']=$request->get('name');
        $data_created['code']=strtoupper($request->get('code'));
        $data_created['category_id']=$request->get('species');
        $data_created['price']=$request->get('price');
        $data_created['catalog_id']=$request->get('catalog_id');
        $data_created['unit']=$request->get('unit');
        $data_created['kl']=$request->get('kl');
        $data_created['created_by']=$user_id;
        $data_created['created_at']=$time;
        $this->code_price->create($data_created);
        return 'success';
    }

    public function deletedCategoryTag(Request $request){
        $id=$request->get('category_id');
        $category=$this->code_price->getDataById($id,'products','');
        if(!($category->products)->isEmpty()){
            $category->products()->delete();
        }
        $this->code_price->deleteRecord($id);
        return 'success';
    }
    public function editCategory(Request $request){
        $id=$request->id;
        $category=$this->code_price->getDataById($id,'','');
        $data_species=$this->category->getDataItem(['is_deleted'=>NO_DELETED],'products','',['id'=>'desc']);
        $dataCatalog =$this->catalogRepository->getDataItem(['is_deleted'=>NO_DELETED],'','',['id'=>'desc']);

        return view('backend.category.dataEditModal',compact('category','data_species', 'dataCatalog'));
    }

    public function postEdit(Request $request){
        $user_id=Auth::id();
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'unit'=>'required',
            'kl'=>'required|numeric',
            'price'=>'required|numeric'
        ],[
            'name.required'=>'Tên không được để trống',
            'kl.required'=>'Khối lượng không được để trống',
            'kl.numeric'=>'Khối lượng chỉ được nhập số',
            'unit.required'=>'Đơn vị không được để trống',
            'price.required'=>'Giá không được để trống',
            'price.numeric'=>'Giá chỉ được nhập số'
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $id=$request->get('id');
        $data_updated=array();
        $code_new=$request->get('code');
        $category=$this->code_price->getDataById($id,'','');
        if($code_new!=$category->code){
            $validator_code=Validator::make($request->all(),[
                'code'=>'required|unique:code_price',
            ],[
                'code.required'=>'Mã sản phẩm không được để trống',
                'code.unique'=>'Mã sản phẩm đã tồn tại',
            ]);
            if($validator_code->fails()){
                return $validator_code->errors()->first();
            }
            $data_updated['code']=strtoupper($request->get('code'));
        }

        $data_updated['description']=$request->get('name');
        $data_updated['category_id']=$request->get('species');
        $data_updated['price']=$request->get('price');
        $data_updated['catalog_id']=$request->get('catalog_id');
        $data_updated['unit']=$request->get('unit');
        $data_updated['kl']=$request->get('kl');
        $data_updated['updated_by']=$user_id;
        $this->code_price->update($id,$data_updated);
        return 'success';
    }

    public function search(Request $request){
        $search=$request->search;
        $data_categories=$this->code_price->searchCategory($search);
        return view('backend.category.dataTableCategory',compact('data_categories'));
    }
    public function showModalSpecies(Request $request){
        $id=$request->id;
        $species=$this->category->getDataById($id,'','');
        return json_encode($species);
    }

    public function showModalCatalog(Request $request){
        $id=$request->id;
        $catalog=$this->catalogRepository->getDataById($id,'','');
        return json_encode($catalog);
    }

    public function editCatalog(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required'
        ],[
            'name.required'=>'Tên không được để trống'
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $id=$request->id;
        $data_update=array(
            'name'=>$request->name
        );
        $this->catalogRepository->update($id,$data_update);
        return 'success';
    }

    public function editSpecies(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required'
        ],[
            'name.required'=>'Tên không được để trống'
        ]);
        if($validator->fails()){
            return $validator->errors()->first();
        }
        $id=$request->id;
        $data_update=array(
            'name'=>$request->name
        );
        $this->category->update($id,$data_update);
        return 'success';
    }
    public function deleteSpecies(Request $request){
        $id=$request->id;
        $species=$this->category->getDataById($id,['products','codeprices'],'');
        if(!$species->products->isEmpty()){
            $species->products()->delete();
        }
        if(!$species->codeprices->isEmpty()){
            $species->codeprices()->delete();
        };
        $species->delete();
        return 'success';
    }

    public function deleteCatalog(Request $request){
        $id=$request->id;
        $species=$this->catalogRepository->getDataById($id,'','');
        $species->delete();
        return 'success';
    }
}
