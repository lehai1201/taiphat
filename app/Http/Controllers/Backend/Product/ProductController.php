<?php

namespace App\Http\Controllers\Backend\product;

use App\Http\Controllers\BaseController;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\CodePrice\CodePriceRepository;
use App\Repositories\Product\ProductRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends BaseController
{
    //
    private $product;
    private $category;
    private $code_price;

    public function __construct()
    {
        parent::__construct();
        $this->product = new ProductRepository();
        $this->category = new CategoryRepository();
        $this->code_price = new CodePriceRepository();
    }

    public function list()
    {
        $data_all_product = $this->product->getDataItem(['is_deleted' => NO_DELETED], ['category', 'codeprice'], 10,
            ['id' => 'desc']);
        return view('backend.product.product', compact('data_all_product'));
    }

    public function addProduct()
    {
        $data_species = $this->category->getDataItem(['is_deleted' => NO_DELETED], 'codeprices', '', ['id' => 'desc']);
        return view('backend.product.FormProduct', compact('data_species'));
    }

    public function postAddProduct(Request $request)
    {
        $id = Auth::id();
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:50',
            'description' => 'required',
            'content' => 'required',
            'images' => 'required|image|mimes:jpg,png,jpeg'
        ], [
            'title.required' => 'Tiêu đề bắt buộc phải nhập',
            'title.max' => 'Tiêu đề có tối đa 50 kí tự',
            'description.required' => 'Mô tả bắt buộc phải nhập',
            'content.required' => 'Nội dung bắt buộc phải nhập',
            'images.required' => 'Ảnh bắt buộc phải nhập',
            'images.image' => 'Chỉ được chọn file ảnh',
            'images.mimes' => 'Ảnh phải có đuôi png, jpg, jpge'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors()->first());
        }
        $file = time() . Input::file('images')->getClientOriginalName();
        Input::file('images')->move('images/product', $file);
        $data_create = array(
            'category_id' => $request->get('species'),
            'code_price_id' => $request->get('category'),
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'content' => $request->get('content'),
            'images' => $file,
            'created_by' => $id
        );
        $this->product->create($data_create);
        return redirect(route('product.list'))->with('success', 'Đã thêm thành công');
    }

    public function ajaxLoadCategory(Request $request)
    {
        $id = $request->id;
        $data_category = $this->code_price->getDataItem(['category_id' => $id], '', '', '');
        return view('backend.product.category_list', compact('data_category'));
    }

    public function ajaxLoadEditCategory(Request $request)
    {
        $id_product = $request->get('id');
        $id_category = $request->get('id_category');
        $data_category = $this->code_price->getDataItem(['category_id' => $id_category], '', '', '');
        $product = $this->product->getDataById($id_product, '', '');
        return view('backend.product.category_list', compact('data_category', 'product'));
    }


    public function deletedProduct(Request $request)
    {
        $id = $request->get('id');
        $product = $this->product->getDataById($id);
        if (file_exists("images/product/" . $product->images)) {
            unlink("images/product/" . $product->images);
        }
        $this->product->deleteRecord($id);
        return 'Đã xóa thành công';
    }

    public function searchProduct(Request $request)
    {
        $search = $request->search;
        $data_all_product = $this->product->searchProduct($search);
        return view('backend.product.table_product', compact('data_all_product'));
    }

    public function editProduct($id)
    {
        $product = $this->product->getDataById($id, ['category', 'codeprice'], '');
        $data_species = $this->category->getDataItem(['is_deleted' => NO_DELETED], 'codeprices', '', ['id' => 'desc']);
        return view('backend.product.editProduct', compact('product', 'data_species', 'id'));
    }

    public function postEditProduct(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:50',
            'description' => 'required',
            'content' => 'required'
        ], [
            'title.required' => 'Tiêu đề bắt buộc phải nhập',
            'title.max' => 'Tiêu đề có tối đa 50 kí tự',
            'description.required' => 'Mô tả bắt buộc phải nhập',
            'content.required' => 'Nội dung bắt buộc phải nhập'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors()->first());
        }
        $data_update=array();
        $product=$this->product->getDataById($id,'','');
        if ($request->has('images')) {
            $validator_img = Validator::make($request->all(), [
                'images' => 'required|image|mimes:jpg,png,jpeg'
            ],
                [
                    'images.required' => 'Ảnh bắt buộc phải nhập',
                    'images.image' => 'Chỉ được chọn file ảnh',
                    'images.mimes' => 'Ảnh phải có đuôi png, jpg, jpge'
                ]);
            if($validator_img->fails()){
                return redirect()->back()->with('errors',$validator_img->errors()->first());
            }
            $file=time().Input::file('images')->getClientOriginalName();
            if (file_exists('images/product/' . $product->images)) {
                unlink('images/product/' . $product->images);
            }
            Input::file('images')->move('images/product',$file);
            $data_update['images']=$file;
        }
        $data_update['title']=$request->get('title');
        $data_update['description']=$request->get('description');
        $data_update['content']=$request->get('content');
        $data_update['species']=$request->get('species');
        $data_update['category']=$request->get('category');
        $this->product->update($id,$data_update);
        return redirect(route('product.list'))->with('success','Sửa thành công');
    }
}
