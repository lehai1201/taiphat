<?php

namespace App\Http\Controllers\Backend\Video;

use App\Http\Controllers\BaseController;
use App\Repositories\Video\VideoRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class VideoController extends BaseController
{
    protected $video;

    public function __construct()
    {
        parent::__construct();
        $this->video = new VideoRepository();
    }

    public function index()
    {
        $listVideo = $this->video->getDataItem([], [], 10, ['id' => 'desc']);

        return view('backend.video.index', compact('listVideo'));
    }

    public function add()
    {
        return view('backend.video.create');
    }

    public function postAdd(Request $request)
    {
        $id = Auth::id();
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'url' => 'required|url'
        ], [
            'title.required' => 'Tên không được để trống',
            'url.required' => 'Url không được để trống',
            'url.url' => 'Định dạng url không hợp lệ',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors()->all());
        }

        $data_created = array(
            'title' => $request->get('title'),
            'url' => $request->get('url'),
            'created_at' => Carbon::now(),
            'created_by' => $id
        );

        DB::beginTransaction();
        try {
            $this->video->create($data_created);
            DB::commit();

            return redirect(route('video.index'))->with('messeger', 'Đã thêm thành công');
        } catch (\Exception $exception) {
            DB::rollBack();

            return redirect(route('video.index'))->with('messeger', 'Lỗi hệ thống');
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $video = $this->video->getDataById($id);

        if (file_exists("videos/" . $video->url)) {
            unlink("videos/" . $video->url);
        }

        $this->video->deleteRecord($id);
        return 'Đã xóa thành công';
    }

    public function edit($id)
    {
        $video = $this->video->getDataById($id);
        if (empty($video)) {
            return redirect(route('video.index'))->with('messeger', 'Dữ liệu đã bị xóa');

        }

        return view('backend.video.edit', compact('video'));
    }

    public function postEdit(Request $request, $id)
    {
        $video = $this->video->getDataById($id);
        if (empty($video)) {
            return redirect(route('video.index'))->with('messeger', 'Dữ liệu đã bị xóa');

        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'url' => 'nullable|url'
        ], [
            'title.required' => 'Tên không được để trống',
            'url.required' => 'Url không được để trống',
            'url.url' => 'Định dạng url không hợp lệ',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors()->all());
        }

        $data_update = array(
            'title' => $request->get('title'),
            'url' => $request->get('url'),
            'updated_at' => Carbon::now(),
            'updated_by' => Auth::id()
        );

        DB::beginTransaction();
        try {
            $this->video->update($id, $data_update);
            DB::commit();

            return redirect(route('video.index'))->with('messeger', 'Đã sửa thành công');
        } catch (\Exception $exception) {
            DB::rollBack();

            return redirect(route('video.index'))->with('messeger', 'Lỗi hệ thống');
        }
    }
}
