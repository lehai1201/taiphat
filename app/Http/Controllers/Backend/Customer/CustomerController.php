<?php

namespace App\Http\Controllers\Backend\Customer;

use App\Http\Controllers\BaseController;
use App\Repositories\Customer\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends BaseController
{
    protected $customer;
    public function  __construct()
    {
        parent::__construct();
        $this->customer=new CustomerRepository();
    }

    public function index()
    {
        $data_customer=$this->customer->getDataItem(['is_deleted'=>NO_DELETED],'','',['id'=>'desc']);
        return view('backend.customer.index',compact('data_customer'));
    }
}
