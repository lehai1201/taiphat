<?php

namespace App\Http\Controllers\Backend\post;

use App\Http\Controllers\BaseController;
use App\Repositories\Post\PostRepository;
use App\Repositories\PostCheck\PostCheckRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use Intervention\Image\ImageManagerStatic as Image;


class PostController extends BaseController
{

    private $post;
    private $post_check;

    public function __construct()
    {
        parent::__construct();
        $this->post = new PostRepository();
        $this->post_check = new PostCheckRepository();
    }

    public function list()
    {
        $post_selected=$this->post_check->getFirstData('','','');
        $postCheck = $this->post->getListPost();
        $posts = $this->post->getDataItem(['is_deleted' => NO_DELETED], 'user', 10, ['id' => 'desc']);
        return view('backend.post.list', compact('posts', 'postCheck','post_selected'));
    }

    public function checkPost(Request $request)
    {
        $user = Auth::id();
        $id = $request->id;
        $check_data = $this->post_check->getDataItem('', '', '', '');
        if ($check_data->isEmpty()) {
            $data_create = array('post_id' => $id, 'created_by' => $user);
            $this->post_check->create($data_create);
        } else {
            $data_update = array('post_id' => $id);
            $post = $this->post_check->getFirstData('', '', '');
            $this->post_check->update($post->id, $data_update);
        }
        return 'success';
    }

    public function edit($id)
    {
        $post = $this->post->getDataById($id, '', '');
        return view('backend.post.edit', compact('post'));
    }

    public function postEdit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
        ], [
            'title.required' => 'Tiêu đề bắt buộc phải nhập',
            'description.required' => 'Mô tả bắt buộc phải nhập',
            'content.required' => 'Nội dung bắt buộc phải nhập',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors()->first());
        }
        $data_updated = array();
        $data_updated['title'] = $request->get('title');
        $data_updated['description'] = $request->get('description');
        $data_updated['content'] = $request->get('content');
        $data_updated['type'] = $request->get('type');
        $old_post = $this->post->getDataById($id, '', '');
        if ($request->has('images')) {
            if (file_exists('images/post/' . $old_post->images)) {
                unlink('images/post/' . $old_post->images);
            }

           $file=time().Input::file('images')->getClientOriginalName();
           Input::file('images')->move('images/post',$file);
           $data_updated['images']=$file;
        }
        $this->post->update($id,$data_updated);
        return redirect(route('post.list'))->with('message','Cập nhập thành công');
    }

    public function addPost()
    {
        return view('backend.post.add');
    }

    public function postAddNews(Request $request)
    {
        $id = Auth::id();
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
            'images' => 'required|image|mimes:jpg,jpeg,png'
        ], [
            'title.required' => 'Tiêu đề bắt buộc phải nhập',
            'description.required' => 'Mô tả bắt buộc phải nhập',
            'content.required' => 'Nội dung bắt buộc phải nhập',
            'images.required' => 'Avatar bắt buộc phải nhập',
            'images.image' => 'Chỉ được chọn file ảnh',
            'images.mimes' => 'Chỉ được chọn ảnh có đuôi jpg,png,jpeg',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors()->first());
        }
        $file = time() . Input::file('images')->getClientOriginalName();
        Input::file('images')->move('images/post', $file);
        $data_created = array(
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'content' => $request->get('content'),
            'images' => $file,
            'type' => $request->get('type'),
            'created_at' => Carbon::now(),
            'created_by' => $id
        );
        $this->post->create($data_created);
        return redirect(route('post.list'))->with('messeger', 'Đã thêm thành công');
    }

    public function deletedPost(Request $request)
    {
        $id = $request->get('id');
        $post = $this->post->getDataById($id, '', '');
        if (file_exists('images/post/' . $post->images)) {
            unlink('images/post/' . $post->images);
        }
        $this->post->deleteRecord($id);
        return 'success';
    }


}
