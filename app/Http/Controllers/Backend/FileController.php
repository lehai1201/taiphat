<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseController;
use App\Repositories\FileRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class FileController extends BaseController
{
    public $fileRepository;

    public function __construct()
    {
        parent::__construct();
        $this->fileRepository = new FileRepository();
    }

    public function index()
    {
        $data = $this->fileRepository->getFirstData();

        return view('backend.file.index',  compact('data'));
    }

    public function add(Request $request)
    {
        $userId = Auth::id();
        $id = $request->get('id');
        $rules = [
            'title' => 'required',
            'image1' => 'required|image|mimes:jpg,png,jpeg',
            'image2' => 'nullable|file|mimes:pdf|max:10240',

        ];

        if (!empty($id)) {
            $rules['image1'] = 'nullable|image|mimes:jpg,png,jpeg';
        }

        $validator = Validator::make($request->all(),$rules, [
            'image2.mimes' => 'Phải chọn file pdf',
            'image2.max' => 'Kích thước tối đa là 10MB'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors()->first());
        }

        if (!empty($id)) {
            $product=$this->fileRepository->getDataById($id,'','');
            $dataUpdate = array(
                'title' => $request->get('title')
            );
            if ($request->has('image1')) {
                if (file_exists('images/file/' . $product->image1)) {
                    unlink('images/file/' . $product->image1);
                }
                $file1 = time() . Input::file('image1')->getClientOriginalName();
                Input::file('image1')->move('images/file', $file1);
                $dataUpdate['image1'] = $file1;
            }

            if ($request->has('image2')) {
                if (file_exists('images/file/' . $product->image2)) {
                    unlink('images/file/' . $product->image2);
                }
                $file2 = time() . Input::file('image2')->getClientOriginalName();
                Input::file('image2')->move('images/file', $file2);
                $dataUpdate['image2'] = $file2;
            }

            $this->fileRepository->update($id, $dataUpdate);
            return redirect(route('backend.file.index'))->with('messeger', 'Đã sửa thành công');
        }
        $file1 = $file2 = $file3 = $file4 = '';

        if( $request->has('image1')){
            $file1 = time() . Input::file('image1')->getClientOriginalName();
            Input::file('image1')->move('images/file', $file1);
        }

        $data_created = array(
            'title' => $request->get('title'),
            'image1' => $file1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by' => $userId
        );

        if($request->has('image2')){
            $file2 = time() . Input::file('image2')->getClientOriginalName();
            Input::file('image2')->move('images/file', $file2);
            $data_created['image2'] = $file2;
        }

        $this->fileRepository->create($data_created);
        return redirect(route('backend.file.index'))->with('messeger', 'Đã thêm thành công');
    }
}
