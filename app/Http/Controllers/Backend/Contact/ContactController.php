<?php

namespace App\Http\Controllers\Backend\Contact;

use App\Http\Controllers\BaseController;
use App\Jobs\SendMailQueue;
use App\Jobs\SendReminderEmail;
use App\Repositories\Registration\RegistrationRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

class ContactController extends BaseController
{
    private $registration;

    public function __construct()
    {
        parent::__construct();
        $this->registration = new RegistrationRepository();
    }

    public function index()
    {
        //lấy danh sách liên hệ
        $data_contact = $this->registration->getDataItem(['type' => IS_LIENHE], '', 9, ['id' => 'desc']);
        return view('backend.contact.index', compact('data_contact'));
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $data_contact = $this->registration->getDataBySearch(['type' => IS_LIENHE], '', 9, ['id' => 'desc'], ['name' => $search]);
        return view('backend.contact.data_lienhe', compact('data_contact'));
    }

    public function tuvan()
    {
        //Lấy danh sách tư vấn
        $data_tuvan = $this->registration->getDataItem(['type' => IS_TUVAN], '', '', ['id' => 'desc']);
        return view('backend.contact.tuvan', compact('data_tuvan'));
    }

    public function daily()
    {
        //lấy danh sách đại lí
        $data_daily = $this->registration->getDataItem(['type' => IS_DAILI], '', 6, ['id' => 'desc']);
        return view('backend.contact.daily', compact('data_daily'));
    }

    public function replyContact(Request $request)
    {
        $contact_id = $request->get('contact_id');
        $data_contact = $this->registration->getDataById($contact_id, '', '');
        return json_encode(['id' => $data_contact->id, 'email' => $data_contact->email]);
    }

    public function sendEmailContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email_title' => 'required',
            'content' => 'required'
        ], [
            'email_title.required' => 'Tiêu đề bắt buộc phải nhập',
            'content.required' => 'Nội dung bắt buộc phải nhập'
        ]);
        if ($validator->fails()) {
            return $validator->errors()->first();
        }
//        //thực hiện gửi mail
        try {
            $email_title = $request->get('email_title');
            $content = $request->get('content');
            $email_to = $request->get('email_to');
            $param = array(
                'title' => $email_title,
                'content' => $content
            );
            SendMailQueue::dispatch($email_to,$email_title,$content)->delay(10);
            return 'success';
        } catch (Exception $e) {
            Log::error('SEND MAIL ERROR',['CONTENT'=> $e->getMessage()]);
            return 'error';
        }
    }
}
