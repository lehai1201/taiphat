<?php

namespace App\Http\Controllers\Backend\Rate;

use App\Http\Controllers\BaseController;
use App\Repositories\Rate\RateRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class RateController extends BaseController
{
    protected $rateRepository;

    public function __construct(RateRepository $rateRepository)
    {
        parent::__construct();
        $this->rateRepository = $rateRepository;
    }

    public function index()
    {
        $listRate = $this->rateRepository->getDataItem([], [], 10, ['id' => 'desc']);

        return view('backend.rate.index', compact('listRate'));
    }


    public function add()
    {
        return view('backend.rate.create');
    }

    public function edit($id)
    {
        $rate = $this->rateRepository->getDataById($id);
        if (empty($rate)) {
            return redirect(route('rate.index'))->with('errors', 'Dữ liệu đã bị xóa');

        }

        return view('backend.rate.edit', compact('rate'));
    }

    public function postEdit(Request $request, $id)
    {
        $video = $this->rateRepository->getDataById($id);
        if (empty($video)) {
            return redirect(route('rate.index'))->with('error', 'Dữ liệu đã bị xóa');

        }

        $validator = Validator::make($request->all(), [
            'comment' => 'required|max:128',
            'name' => 'required|max:128',
            'address' => 'required|max:128',
            'avatar' => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048'
        ], [
            'comment.required' => 'Nhận xét không được để trống',
            'name.required' => 'Tên không được để trống',
            'name.max' => 'Tên không được quá 128 kí tự',
            'address.required' => 'Địa chỉ không được để trống',
            'address.max' => 'Địa chỉ không được quá 128 kí tự',
            'avatar.image' => 'Định dạng không hợp lệ',
            'avatar.mimes' => 'Phải chọn đuôi jpeg, png, jpg, svg',
            'avatar.max' => 'Ảnh tối đa 2MB',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors()->all());
        }

        $data_update = array(
            'comment' => $request->get('comment'),
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'sort' => $request->get('sort'),
            'updated_at' => Carbon::now(),
            'updated_by' => Auth::id()
        );

        if ($request->has('avatar')) {
            $file = time() . Input::file('avatar')->getClientOriginalName();

            if (file_exists('images/rate/' . $video->avatar)) {
                unlink('images/rate/' . $video->avatar);
            }

            Input::file('avatar')->move('images/rate/', $file);

            $data_update['avatar'] = $file;
        }

        DB::beginTransaction();
        try {
            $this->rateRepository->update($id, $data_update);
            DB::commit();

            return redirect(route('rate.index'))->with('success', 'Đã sửa thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('ERROR', ['CONTENT' => $exception->getMessage()]);

            return redirect(route('rate.index'))->with('errors', 'Lỗi hệ thống');
        }
    }

    public function postAdd(Request $request)
    {
        $id = Auth::id();
        $validator = Validator::make($request->all(), [
            'comment' => 'required|max:128',
            'name' => 'required|max:128',
            'address' => 'required|max:128',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048'
        ], [
            'comment.required' => 'Nhận xét không được để trống',
            'name.required' => 'Tên không được để trống',
            'name.max' => 'Tên không được quá 128 kí tự',
            'address.required' => 'Địa chỉ không được để trống',
            'address.max' => 'Địa chỉ không được quá 128 kí tự',
            'avatar.image' => 'Định dạng không hợp lệ',
            'avatar.mimes' => 'Phải chọn đuôi jpeg, png, jpg, svg',
            'avatar.max' => 'Ảnh tối đa 2MB',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors()->all());
        }

        $file = time() . Input::file('avatar')->getClientOriginalName();

        Input::file('avatar')->move('images/rate', $file);

        $data_created = array(
            'comment' => $request->get('comment'),
            'sort' => $request->get('sort'),
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'avatar' => $file,
            'created_at' => Carbon::now(),
            'created_by' => $id
        );

        DB::beginTransaction();
        try {
            $this->rateRepository->create($data_created);
            DB::commit();

            return redirect(route('rate.index'))->with('success', 'Đã thêm thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('ERROR', ['CONTENT' => $exception->getMessage()]);

            return redirect(route('rate.index'))->with('errors', 'Lỗi hệ thống');
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $rate = $this->rateRepository->getDataById($id);

        if (file_exists("images/rate/" . $rate->avatar)) {
            unlink("images/rate/" . $rate->avatar);
        }

        $this->rateRepository->deleteRecord($id);
        return 'Đã xóa thành công';
    }

}
