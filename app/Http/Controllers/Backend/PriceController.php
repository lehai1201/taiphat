<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseController;
use App\Repositories\PriceRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class PriceController extends BaseController
{
    private $price;

    public function __construct()
    {
        parent::__construct();
        $this->price = new PriceRepository();
    }

    public function list()
    {
        $listPrice = $this->price->getDataItem([], [], 10, ['sort' => 'asc', 'id' => 'desc']);

        return view('backend.price.index', compact('listPrice'));
    }

    public function add()
    {
        return view('backend.price.create');
    }

    public function postAdd(Request $request)
    {
        $id = Auth::id();
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048'
        ], [
            'image.required' => 'Ảnh không được để trống',
            'image.image' => 'Định dạng không hợp lệ',
            'image.mimes' => 'Phải chọn đuôi jpeg, png, jpg, svg',
            'image.max' => 'Ảnh tối đa 2MB',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors()->all());
        }

        $file = time() . Input::file('image')->getClientOriginalName();

        Input::file('image')->move('images/price', $file);

        $data_created = array(
            'sort' => $request->get('sort'),
            'image' => $file,
            'created_at' => Carbon::now(),
            'created_by' => $id
        );

        DB::beginTransaction();
        try {
            $this->price->create($data_created);
            DB::commit();

            return redirect(route('price.index'))->with('success', 'Đã thêm thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('ERROR', ['CONTENT' => $exception->getMessage()]);

            return redirect(route('price.index'))->with('errors', 'Lỗi hệ thống');
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $price = $this->price->getDataById($id);

        if (file_exists("images/price/" . $price->image)) {
            unlink("images/price/" . $price->image);
        }

        $this->price->deleteRecord($id);
        return 'Đã xóa thành công';
    }

    public function edit($id)
    {
        $price = $this->price->getDataById($id);
        if (empty($price)) {
            return redirect(route('price.index'))->with('errors', 'Dữ liệu đã bị xóa');

        }

        return view('backend.price.edit', compact('price'));
    }

    public function postEdit(Request $request, $id)
    {
        $price = $this->price->getDataById($id);
        if (empty($price)) {
            return redirect(route('price.index'))->with('error', 'Dữ liệu đã bị xóa');

        }

        $validator = Validator::make($request->all(), [
            'image' => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048'
        ], [
            'image.image' => 'Định dạng không hợp lệ',
            'image.mimes' => 'Phải chọn đuôi jpeg, png, jpg, svg',
            'image.max' => 'Ảnh tối đa 2MB',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors()->all());
        }

        $dataUpdate = array(
            'sort' => $request->get('sort'),
            'updated_at' => Carbon::now(),
            'updated_by' => Auth::id()
        );

        if ($request->has('image')) {
            $file = time() . Input::file('image')->getClientOriginalName();

            if (file_exists('images/price/' . $price->image)) {
                unlink('images/price/' . $price->image);
            }

            Input::file('image')->move('images/price/', $file);

            $dataUpdate['image'] = $file;
        }

        DB::beginTransaction();
        try {
            $this->price->update($id, $dataUpdate);
            DB::commit();

            return redirect(route('price.index'))->with('success', 'Đã sửa thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('ERROR', ['CONTENT' => $exception->getMessage()]);

            return redirect(route('price.index'))->with('errors', 'Lỗi hệ thống');
        }
    }
}
