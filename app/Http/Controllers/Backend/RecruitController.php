<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseController;
use App\Repositories\RecruitRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RecruitController extends BaseController
{
    public $recruitRepository;

    public function __construct(RecruitRepository $recruitRepository)
    {
        parent::__construct();
        $this->recruitRepository = $recruitRepository;
    }

    public function index()
    {
        $data = $this->recruitRepository->getFirstData();

        return view('backend.recruit.index', compact('data'));
    }

    public function add(Request $request)
    {
        $userId = Auth::id();
        $id = $request->get('id');
        $validator = Validator::make($request->all(), [
            'content' => 'required',
        ], [
            'content.required' => 'Nội dung bắt buộc phải nhập',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors()->first());
        }

        if (!empty($id)) {
            $dataUpdate = array(
                'content' => $request->get('content'),
            );
            $this->recruitRepository->update($id, $dataUpdate);
            return redirect(route('backend.recruit.index'))->with('messeger', 'Đã sửa thành công');
        }
        $data_created = array(
            'content' => $request->get('content'),
            'created_at' => Carbon::now(),
            'created_by' => $userId
        );
        $this->recruitRepository->create($data_created);
        return redirect(route('backend.recruit.index'))->with('messeger', 'Đã thêm thành công');
    }
}
