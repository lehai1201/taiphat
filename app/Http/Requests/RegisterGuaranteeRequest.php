<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterGuaranteeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id.*' => 'required',
            'agency_id' => 'required',
            'number.*' => 'required|numeric|max:100',
            'agree' => 'accepted',
        ];
    }

    public function attributes()
    {
        return [
            'product_id.*' => 'Sản phẩm',
            'number.*' => 'Số lượng',
            'agree' => 'Bạn',
        ];
    }
}
