<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class GuaranteeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:64',
            'phone' => 'required|unique:clients|regex:/^((0)[0-9\s\-\+\(\)]{9,10})$/',
            'address' => 'required|max:64',
            'email' => 'nullable|email|max:64',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Tên khách hàng',
            'address' => 'Địa chỉ',
            'phone' => 'Số điện thoại',
            'email' => 'Email',
        ];
    }

    public function messages()
    {
        return [
            'phone.regex' => trans('validation.invalid_phone')

        ];
    }
}
