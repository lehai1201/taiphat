<?php

namespace App;

use App\Models\Post;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Role;
use App\Notifications\ResetPassword as ResetPasswordNotification;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'username',
        'avatar',
        'role_id',
        'phone',
        'is_deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    //relationship
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function post(){
        return $this->hasMany(Post::class,'created_by');
    }
    // Hàm lấy avata
    public function getAvatarAtributte()
    {
        if ($this->avatar == null) {
            return '/avatar/noimage.jpg';
        }
        return '/avatar/'.$this->avatar;

    }
    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }
}
