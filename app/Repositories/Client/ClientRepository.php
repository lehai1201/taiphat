<?php
namespace App\Repositories\Client;

use App\Repositories\BaseRepository;

class ClientRepository extends BaseRepository{
    public function __construct()
    {
        parent::__construct(\App\Models\Client::class);
    }
}
