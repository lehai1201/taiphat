<?php
namespace App\Repositories\Construct;

use App\Models\Construct;
use App\Repositories\BaseRepository;

class ConstructRepository extends BaseRepository{
    public function __construct()
    {
        parent::__construct(Construct::class);
    }
}