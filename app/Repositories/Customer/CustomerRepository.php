<?php
namespace App\Repositories\Customer;

use App\Models\OpinionCustomer;
use App\Repositories\BaseRepository;

class CustomerRepository extends BaseRepository{
    public function __construct()
    {
        parent::__construct(OpinionCustomer::class);
    }
}