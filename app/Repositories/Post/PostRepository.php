<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Repositories\BaseRepository;
use App\User;

class PostRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Post::class);
    }

    public function getPostSame($id,$type)
    {
        $data_post=Post::where('id', '!=', $id)->where('type',$type)->offset(0)->limit(4)->get();
        return $data_post;
    }
    public function getListPost(){
        $data=Post::where('type','!=',IS_TUVAN)->where('is_deleted',NO_DELETED)->get();
        return $data;
    }
}