<?php
namespace App\Repositories\Agency;
use App\Models\Agency;
use App\Repositories\BaseRepository;

class AgencyRepository extends BaseRepository{
    public function __construct()
    {
        parent::__construct(Agency::class);
    }

    public function getArea()
    {
        $norland = $this->getDataItem(['is_deleted'=>NO_DELETED, 'area' => config('constant.AGENCY.NORLAND')],'','',['id'=>'desc']);
        $central = $this->getDataItem(['is_deleted'=>NO_DELETED, 'area' => config('constant.AGENCY.CENTRAL')],'','',['id'=>'desc']);
        $south = $this->getDataItem(['is_deleted'=>NO_DELETED, 'area' => config('constant.AGENCY.SOUTH')],'','',['id'=>'desc']);

        return [$norland, $central, $south];
    }

    public function getAreaFrontend()
    {
        $norland = $this->getDataItem(['is_deleted'=>NO_DELETED, 'area' => config('constant.AGENCY.NORLAND')],'','',['id'=>'desc']);
        $central = $this->getDataItem(['is_deleted'=>NO_DELETED, 'area' => config('constant.AGENCY.CENTRAL')],'','',['id'=>'desc']);
        $south = $this->getDataItem(['is_deleted'=>NO_DELETED, 'area' => config('constant.AGENCY.SOUTH')],'','',['id'=>'desc']);

        return  [
            config('constant.BRANCH.0') => $norland,
            config('constant.BRANCH.1') => $central,
            config('constant.BRANCH.2') => $south
        ];
    }
}
