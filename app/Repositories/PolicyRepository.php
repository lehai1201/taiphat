<?php

namespace App\Repositories;

use App\Models\Policy;


class PolicyRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Policy::class);
    }
}
