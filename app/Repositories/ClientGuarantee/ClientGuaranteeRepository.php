<?php

namespace App\Repositories\ClientGuarantee;

use App\Models\ClientGuarantee;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class ClientGuaranteeRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(\App\Models\ClientGuarantee::class);
    }

    public function getData($clientId)
    {
        return DB::table('client_guarantee')
            ->select(['products.title AS product_name', 'agency.name as agency_name', 'client_guarantee.created_at', 'client_guarantee.number', 'client_guarantee.unit'])
            ->join('products', 'client_guarantee.product_id', 'products.id')
            ->join('agency', 'client_guarantee.agency_id', 'agency.id')
            ->where('client_guarantee.client_id','=', $clientId)
            ->where('client_guarantee.type', config('constant.GUARANTEE_TYPE.ACCEPT'))
            ->orderBy('client_guarantee.created_at', 'desc')
            ->get();
    }

    public function getDataForBackend()
    {
        $params = request()->all();

        $builder = DB::table('client_guarantee')
            ->select(['client_guarantee.id','client_guarantee.unit', 'clients.name as client_name', 'clients.phone', 'products.title AS product_name', 'code_price.code', 'agency.name as agency_name', 'client_guarantee.created_at', 'client_guarantee.number', 'client_guarantee.type'])
            ->join('clients', 'client_guarantee.client_id', 'clients.id')
            ->join('products', 'client_guarantee.product_id', 'products.id')
            ->join('agency', 'client_guarantee.agency_id', 'agency.id')
            ->leftJoin('code_price', 'products.code_price_id', 'code_price.id');

        if (!empty(data_get($params, 'start_date'))) {
            $start = date('Y-m-d', strtotime( data_get($params, 'start_date')));
            $builder->whereDate('client_guarantee.created_at','>=', $start);
        }
        if (!empty(data_get($params, 'end_date'))) {
            $end = date('Y-m-d', strtotime( data_get($params, 'end_date')));
            $builder->whereDate('client_guarantee.created_at','<=', $end);
        }

        if (!empty(data_get($params, 'phone'))) {
            $phone = str_replace('+84', '', data_get($params, 'phone'));
            $builder->where('clients.phone', 'LIKE', '%' . $phone . '%');
        }

        return $builder->orderBy('client_guarantee.id', 'desc')->get();
    }

    public function updateStatus($listId, $status)
    {
        return DB::table('client_guarantee')->whereIn('id', $listId)->update(['type' => $status]);
    }

    public function deleteData($listId)
    {
        return DB::table('client_guarantee')->whereIn('id', $listId)->delete();
    }

    public function getDataForExport($listId)
    {
        return
            ClientGuarantee::select([
                'clients.name as client_name',
                'clients.email',
                'clients.address',
                'clients.phone',
                'code_price.code',
                'products.title AS product_name',
                'agency.name as agency_name',
                'client_guarantee.type',
                DB::raw('(CASE
                WHEN client_guarantee.unit = "1"  THEN "Thùng"
                WHEN client_guarantee.unit = "2"  THEN "Lon"
                WHEN client_guarantee.unit = "3"  THEN "Lit"
                ELSE "Bao" END) AS type'),
                'client_guarantee.number',
                'client_guarantee.created_at'
            ])
                ->join('clients', 'client_guarantee.client_id', 'clients.id')
                ->join('products', 'client_guarantee.product_id', 'products.id')
                ->join('agency', 'client_guarantee.agency_id', 'agency.id')
                ->leftJoin('code_price', 'products.code_price_id', 'code_price.id')
                ->whereIn('client_guarantee.id', $listId)
                ->orderBy('client_guarantee.id', 'desc')
                ->get();
    }
}
