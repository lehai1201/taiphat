<?php
namespace App\Repositories\PostCheck;
use App\Models\PostCheck;
use App\Repositories\BaseRepository;

class PostCheckRepository extends BaseRepository{
    public function __construct()
    {
        parent::__construct(PostCheck::class);
    }
}