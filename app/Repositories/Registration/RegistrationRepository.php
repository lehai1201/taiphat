<?php

namespace App\Repositories\Registration;


use App\Models\RegistrationType;
use App\Repositories\BaseRepository;
use App\User;

class RegistrationRepository extends  BaseRepository
{
    public function __construct()
    {
        parent::__construct(RegistrationType::class);
    }

}