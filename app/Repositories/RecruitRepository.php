<?php

namespace App\Repositories;

use App\Models\Recruit;


class RecruitRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Recruit::class);
    }
}
