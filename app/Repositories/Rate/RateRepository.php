<?php
namespace App\Repositories\Rate;
use App\Models\Rate;
use App\Repositories\BaseRepository;

class RateRepository extends BaseRepository{
    public function __construct()
    {
        parent::__construct(Rate::class);
    }
}
