<?php

namespace App\Repositories\Video;

use App\Models\Video;
use App\Repositories\BaseRepository;

class VideoRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Video::class);
    }
}
