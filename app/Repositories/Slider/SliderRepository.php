<?php
namespace App\Repositories\Slider;
use App\Models\Slider;
use App\Repositories\BaseRepository;

class SliderRepository extends BaseRepository{
    public function __construct()
    {
        parent::__construct(Slider::class);
    }
}