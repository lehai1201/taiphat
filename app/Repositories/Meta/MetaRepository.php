<?php

namespace App\Repositories\Meta;

use App\Models\Meta;
use App\Repositories\BaseRepository;


class MetaRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Meta::class);
    }
}
