<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Product::class);
    }

    public function getProductSame($id, $category_id)
    {
        $data_product_same = Product::where('id', '!=', $id)->where('category_id',
            $category_id)->with('category')->offset(0)->limit(10)->get();
        return $data_product_same;
    }

    public function searchProduct($search)
    {
        $data_search = Product::where('is_deleted', NO_DELETED)->where('title', 'LIKE',
            '%' . $search . '%')->with('category')->with('codeprice')->paginate(10);
        return $data_search;
    }

    public function getProductHome($id)
    {
        $data = Product::where('category_id', $id)->where('is_deleted', NO_DELETED)->offset(0)->limit(15)->orderBy('id',
            'desc')->get();
        return $data;
    }

    public function caculator($dientich)
    {
        $data=DB::table('products')->select(
            'products.id',
            'products.title',
            'products.images',
            'products.code_price_id',
            'code_price.kl',
            'code_price.price',
            'code_price.unit',
            'code_price.description'
        )->leftJoin('code_price','products.code_price_id','code_price.id')
            ->where('code_price.kl','<=',$dientich)
            ->offset(0)
            ->limit(3)
            ->get();
        return $data;
    }

    public function getListForFrontend()
    {
        $result = [];
        $products = DB::table('products')->select(
            'products.id',
            'products.title as product_name',
            'code_price.code',
            'catalogs.id as catalog_id',
            'catalogs.name as catalog_name'
        )->leftJoin('code_price', 'products.code_price_id', 'code_price.id')
            ->leftJoin('catalogs', 'code_price.catalog_id', 'catalogs.id')
            ->orderBy('catalogs.name', 'desc')
            ->orderBy('products.title', 'desc')
            ->get();

        if ($products->isNotEmpty()) {
            foreach ($products as $product) {
                $result[$product->catalog_name][] = $product;
            }
        }

        return $result;
    }
}
