<?php

namespace App\Repositories\Catalog;

use App\Models\Catalog;
use App\Repositories\BaseRepository;

class CatalogRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Catalog::class);
    }

}

