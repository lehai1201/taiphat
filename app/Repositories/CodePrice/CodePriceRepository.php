<?php
namespace App\Repositories\CodePrice;
use App\Models\CodePrice;
use App\Repositories\BaseRepository;

class CodePriceRepository extends BaseRepository{
    public function __construct()
    {
        parent::__construct(\App\Models\CodePrice::class);
    }
    public function searchCategory($search){
        $data=CodePrice::with('category')->where('description','LIKE','%'.$search.'%')->orwhere('code',$search)->paginate(10);
        return $data;
    }
}