<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class BaseRepository
{
    protected $model_class;

    public function __construct($_model_class)
    {
        $this->model_class = $_model_class;
    }

    //Lấy tất cả bản ghi
    public function getDataItem($parameters = array(), $with_fields = null, $paging = null, $order_by_fields = null)
    {
        $data_query = $this->generateQueryBuilder($parameters, $with_fields, $order_by_fields);
        if (!empty($paging)) {
            return $data_query->paginate($paging);
        }
        return $data_query->get();
    }

    //Lấy tất cả bản ghi theo từ khóa tìm kiếm
    public function getDataBySearch(
        $parameters = array(),
        $with_fields = null,
        $paging = null,
        $order_by_fields = null,
        $search = array()
    ) {
        $data_query = $this->generateQueryBuilder($parameters, $with_fields, $order_by_fields);
        foreach ($search as $key => $data_search) {
            $data_query->where($key, 'LIKE', '%' . $data_search . '%');
        }
        if (!empty($paging)) {
            return $data_query->paginate($paging);
        }
        return $data_query->get();
    }

    //Lấy theo số lượng
    public function getRecordData($parameters = array(), $with_fields = null, $record = null, $order_by_fields = null)
    {
        $data_query = $this->generateQueryBuilder($parameters, $with_fields, $order_by_fields);
        $data_query->offset(0)->limit($record);
        return $data_query->get();
    }

    //Lấy tất cả bản ghi trừ sys
    public function getDataUserNotSys(
        $parameters = array(),
        $with_fields = null,
        $paging = false,
        $order_by_fields = null
    ) {
        $data_query = $this->generateQueryBuilder($parameters, $with_fields, $order_by_fields);
        $data_query = $data_query->where('id', '!=', 1);
        if ($paging) {
            return $data_query->paginate($paging);
        }
        return $data_query->get();
    }

    //lấy 1 bản ghi
    public function getFirstData($parameters = array(), $with_fields = null, $order_by_fields = null)
    {
        $data_query = $this->generateQueryBuilder($parameters, $with_fields, $order_by_fields);
        return $data_query->first();
    }

    //lấy 1 bản ghi theo id
    public function getDataById($id, $with_fields = null, $order_by_fields = null)
    {
        if (empty($id)) {
            return null;
        }
        $parameters = array(
            'id' => $id
        );
        return $this->getFirstData($parameters, $with_fields, $order_by_fields);

    }

    // thực hiện truy vấn
    public function generateQueryBuilder($parameters, $with_fields, $order_by_fields)
    {
//        if($ignore_param_deleted){
//            //nếu có truyền vào
//        }else {
//            //Nếu không truyền vào đối tượng delete thì mặc định lấy nodelete
//            $data_query = $this->model_class::where('is_deleted', NO_DELETED);
//        }
        $data_query = $this->model_class::whereRaw(DB::raw("1=1"));
        // điều kiện
        if (!empty($parameters)) {
            foreach ($parameters as $parameter_key => $parameter_value) {
                if (is_null($parameter_value)) {
                    continue;
                }
                if (!is_null($parameter_value)) {
                    $data_query = $data_query->where($parameter_key, $parameter_value);
                }
            }
        }

        //relationship
        if (!empty($with_fields)) {
            $data_query->with($with_fields);
        }

        //orderby
        if (!empty($order_by_fields)) {
            if (is_array($order_by_fields)) {
                foreach ($order_by_fields as $order_by_column => $order_by_type) {
                    $data_query = $data_query->orderBy($order_by_column, $order_by_type);
                }
            } else {
                $data_query = $data_query->orderBy($order_by_fields);
            }
        }
        return $data_query;
    }

    //  update theo id
    public function update($id, $data_update)
    {
        $item_info = $this->getDataById($id);
        if (empty($item_info)) {
            return false;
        }
        $item_info->update($data_update);
        return $item_info;
    }

    // Created
    public function create($input_data)
    {
        return $this->model_class::create($input_data);
    }

    //delete cứng
    public function deleteRecord($id)
    {
        $item_info = $this->getDataById($id);
        $item_info->delete();
        return true;
    }
}