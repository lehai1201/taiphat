<?php

namespace App\Repositories\Color;

use App\Models\Color;
use App\Repositories\BaseRepository;
use App\User;

class ColorRepository extends  BaseRepository
{
    public function __construct()
    {
        parent::__construct(Color::class);
    }

}