<?php

namespace App\Console;

use App\Console\Commands\sendMailQueue;
use App\Jobs\UpdateRegistration;
use App\Repositories\Registration\RegistrationRepository;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        sendMailQueue::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('queue:work --stop-when-empty --tries=2')->everyMinute()->withoutOverlapping();

//        $schedule->command('emails:work')->everyMinute()->withoutOverlapping();
//        $schedule->job(new UpdateRegistration())->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
