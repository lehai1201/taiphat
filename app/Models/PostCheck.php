<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCheck extends Model
{
    protected $table = 'post_check';
    protected $fillable = ['post_id', 'created_at', 'updated_at', 'created_by', 'updated_by'];
}
