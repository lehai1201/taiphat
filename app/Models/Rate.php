<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table="rate";
    protected $fillable = [
        'avatar',
        'name',
        'address',
        'comment',
        'is_deleted',
        'sort',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}
