<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = "videos";
    protected $fillable=[
        'url','title','created_by','updated_by'
    ];

    public function getTimeCreated()
    {
        $date = Carbon::parse($this->created_at)->format('d-m-Y');
        $time = Carbon::parse($this->created_at)->format('H:i');
        return $date . ' vào lúc ' . $time;
    }

    function genUrl()
    {
        return  str_replace('watch?v=', 'embed/', $this->url);
    }

    function getVideoId(){
        $url = $this->url;
        $video_id = '';
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i', $url, $match)) {
            $video_id = $match[1];
        }

        return $video_id;
    }
}

