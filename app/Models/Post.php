<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = 'posts';
    protected $fillable = [
        'title',
        'description',
        'content',
        'type',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
        'status',
        'is_deleted',
        'images',
    ];
    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function getTimeCreated(){
        $date=Carbon::parse($this->created_at)->format('d-m-Y');
        $time=Carbon::parse($this->created_at)->format('H:i');
        return $date.' vào lúc '.$time;
    }
    public function getTitlePost(){
        switch ($this->type){
            case IS_POST:
                $type='Bài viết';
                break;
            case IS_EVENT:
                $type='Sự kiện';
                break;
            case IS_TUVAN:
                $type='Tư vấn từ chuyên gia';
                break;
        }
        return $this->title.' - '.$type;
    }
    function convert(){
        $title=$this->title;
        $str=strtolower($title);
        $str = preg_replace("/á|à|ạ|ả|ã|â|ă|ấ|ầ|ẫ|ậ|ắ|ằ|ẳ|ẵ|ặ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ặ|Ẵ|Ẳ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ/","a", $str);
        $str=preg_replace("/é|è|ẹ|ẻ|ẽ|ê|ế|ề|ệ|ể|ễ|Ê|Ế|Ề|Ể|Ệ|Ễ/",'e',$str);
        $str=preg_replace("/ú|ù|ủ|ụ|ũ|ứ|ừ|ử|ữ|ự|ư|Ư|Ứ|Ừ|Ử|Ữ|Ự/",'u',$str);
        $str=preg_replace("/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ộ|ổ|ỗ|ơ|ớ|ờ|ở|ỡ|ợ|Ô|Ố|Ồ|Ộ|Ổ|Ỗ|Ơ|Ớ|Ờ|Ợ|Ỡ|Ở/",'o',$str);
        $str=preg_replace("/đ|Đ/",'d',$str);
        $str=preg_replace("/í|ì|ị|ỉ|ĩ|Í|Ì|Ị|Ỉ|Ĩ/",'i',$str);
        $str=preg_replace("/ý|ỳ|ỵ|ỷ|ỹ|Ý|Ỳ|Ỵ|Ỷ|Ỹ/",'i',$str);
        $str=preg_replace("/( )/",'-',$str);
        return $str;
    }
}
