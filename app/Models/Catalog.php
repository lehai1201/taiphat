<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    //
    protected $table = 'catalogs';
    protected $fillable = [
        'name',
        'is_deleted',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function getTimeCreated()
    {
        $date = Carbon::parse($this->created_at)->format('d-m-Y');
        $time = Carbon::parse($this->created_at)->format('H:i');
        return $date . ' vào lúc ' . $time;
    }
}
