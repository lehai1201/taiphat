<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodePrice extends Model
{
    //
    protected $table="code_price";
    protected $fillable=[
        'category_id',
        'description',
        'code',
        'price',
        'kl',
        'catalog_id',
        'unit',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];
    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
    Public function products(){
        return $this->hasMany(Product::class,'code_price_id');
    }
    public function info(){
        return $this->description." - ".$this->code;
    }
}
