<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientGuarantee extends Model
{
    protected $table = 'client_guarantee';
    protected $fillable = [
        'client_id',
        'agency_id',
        'product_id',
        'type',
        'unit',
        'number',
        'is_deleted',
        'created_by',
        'updated_by',
        'created_at',
    ];

    public function client()
    {
        return $this->hasOne(Client::class, 'client_id', 'id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'product_id', 'id');
    }

    public function agency()
    {
        return $this->hasOne(Agency::class, 'agency_id', 'id');
    }
}
