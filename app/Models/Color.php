<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    //
    protected $table='colors';
    protected $fillable=[
        'name',
        'color_code',
        'created_by',
        'updated_by',
        'is_deleted',
        'status',
        'created_at',
        'updated_at',
    ];
}
