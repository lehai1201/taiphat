<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //
    protected $table='files';
    protected $fillable=[
        'title',
        'image1',
        'image2',
        'image3',
        'image4',
        'created_by',
        'updated_by',
        'is_deleted',
        'created_at',
        'updated_at',
    ];
}
