<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    //
    protected $table = 'agency';
    protected $fillable = [
        'name',
        'address',
        'email',
        'area',
        'phone',
        'code_map',
        'is_deleted',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function getAreaAtributte(){
        $area = $this->area;
        if($area == config('constant.AGENCY.NORLAND')){
            return 'Miền Bắc';
        }
        if($area == config('constant.AGENCY.CENTRAL')){
            return 'Miền Trung';
        }
        if($area == config('constant.AGENCY.SOUTH')){
            return 'Miền Nam';
        }
    }
}
