<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Construct extends Model
{
    protected $table = 'constructs';
    protected $fillable = [
        'id',
        'title',
        'images',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    public function getTimeCreated()
    {
        $date=Carbon::parse($this->created_at)->format('d-m-Y');
        $time=Carbon::parse($this->created_at)->format('H:i');
        return $date." vào lúc ".$time;
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
