<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $fillable = [
        'name',
        'phone',
        'address',
        'email',
        'is_deleted',
        'status',
        'created_by',
        'updated_by'
    ];

}
