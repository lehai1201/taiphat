<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //
    protected $table='slider';
    protected $fillable=[
        'images','is_deleted','created_by','updated_by'
    ];
    public function getTimeCreated()
    {
        $date=Carbon::parse($this->created_at)->format('d-m-Y');
        $time=Carbon::parse($this->created_at)->format('H:i');
        return $date." vào lúc ".$time;
    }
}
