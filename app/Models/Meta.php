<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    //
    protected $table='meta';
    protected $fillable=[
        'tag',
        'type',
        'created_by',
        'updated_by',
        'is_deleted',
        'created_at',
        'updated_at',
    ];

    public function getType(){
        $type = $this->type;
        if((int)$type == 0){
            return 'Header';
        }
        if((int)$type == 1){
            return 'Footer';
        }

        return '';
    }
}
