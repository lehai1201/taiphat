<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpinionCustomer extends Model
{
    //
    protected $table="opinion_customer";
    protected $fillable=[
        'content','address','status','is_deleted','created_by','updated_by'
    ];
}
