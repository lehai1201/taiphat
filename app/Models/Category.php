<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category_tag';
    protected $fillable = [
        'name',
        'type',
        'is_deleted',
        'status',
        'created_by',
        'updated_by'
    ];

    public function getTimeCreated()
    {
        $date = Carbon::parse($this->created_at)->format('d-m-Y');
        $time = Carbon::parse($this->created_at)->format('H:i');
        return $date . ' vào lúc ' . $time;
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function codeprices()
    {
        return $this->hasMany(CodePrice::class, 'category_id');
    }
    function convert(){
        $title=$this->name;
        $str=strtolower($title);
        $str = preg_replace("/á|à|ạ|ả|ã|â|ă|ấ|ầ|ẫ|ậ|ắ|ằ|ẳ|ẵ|ặ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ặ|Ẵ|Ẳ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ/","a", $str);
        $str=preg_replace("/é|è|ẹ|ẻ|ẽ|ê|ế|ề|ệ|ể|ễ|Ê|Ế|Ề|Ể|Ệ|Ễ/",'e',$str);
        $str=preg_replace("/ú|ù|ủ|ụ|ũ|ứ|ừ|ử|ữ|ự|ư|Ư|Ứ|Ừ|Ử|Ữ|Ự/",'u',$str);
        $str=preg_replace("/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ộ|ổ|ỗ|ơ|ớ|ờ|ở|ỡ|ợ|Ô|Ố|Ồ|Ộ|Ổ|Ỗ|Ơ|Ớ|Ờ|Ợ|Ỡ|Ở/",'o',$str);
        $str=preg_replace("/đ|Đ/",'d',$str);
        $str=preg_replace("/í|ì|ị|ỉ|ĩ|Í|Ì|Ị|Ỉ|Ĩ/",'i',$str);
        $str=preg_replace("/ý|ỳ|ỵ|ỷ|ỹ|Ý|Ỳ|Ỵ|Ỷ|Ỹ/",'i',$str);
        $str=preg_replace("/( )/",'-',$str);
        return $str;
    }
}
