<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegistrationType extends Model
{
    //
    protected $table = "registration_type";
    protected $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'images',
        'note',
        'type',
        'is_reply',
        'is_new',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];
}
