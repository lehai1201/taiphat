<?php

namespace App\Jobs;

use App\Repositories\Registration\RegistrationRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateRegistration implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $registration;
    public function __construct()
    {
        $this->registration=new RegistrationRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       $data_all=$this->registration->getDataItem(['is_new'=>IS_NEW],'','','');
       $data_update=array('is_new'=>IS_OLD);
       foreach ($data_all as $item){
           $this->registration->update($item->id,$data_update);
       }
    }
}
