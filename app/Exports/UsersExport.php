<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UsersExport implements FromCollection,WithHeadings,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        return User::select('name','email','address','phone')->where('id','>',1)->get();
    }

    public function headings(): array
    {
        return [
            'Tên',
            'Email',
            'Địa chỉ',
            'Số điện thoại'
        ];
    }
}
