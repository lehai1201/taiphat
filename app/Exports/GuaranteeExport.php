<?php

namespace App\Exports;

use App\Repositories\ClientGuarantee\ClientGuaranteeRepository;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class GuaranteeExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $listId;

    public function __construct($listId)
    {
        $this->listId = $listId;
    }

    public function collection()
    {
        $clientGuarantee = app(ClientGuaranteeRepository::class);
        return $clientGuarantee->getDataForExport($this->listId);
    }

    public function headings(): array
    {
        return [
            'Tên',
            'Email',
            'Địa chỉ',
            'Số điện thoại',
            'Mã sản phẩm',
            'Tên sản phẩm',
            'Đại lý',
            'Đơn vị',
            'Số lượng',
            'Ngày đăng ký',
        ];
    }
}
